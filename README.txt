# Tesis

Proyecto de LaTeX. Tesis Doctoral.

# NOTAS sobre edicion de la tesis

1. Reemplazar matrices por scaffolds en varias partes asi se intercalan los terminos
(scaffold se introduce entre parentesis en una parte, de ahi en mas es valido usarla)
2. Reemplazar enrulamiento por tortuosidad en varias partes asi se intercalan los terminos
3. Reemplazar haz por fascículo en varias partes...

# APORTES FUNDAMENTALES (2 señalados por Santi, despues ire agregando yo)

1. Santi, Cap2, resultados, comparacion: En el modelo aquí presentado, la adopción de una distribución estadística para la tortuosidad de las fibras permite reproducir exitosamente la respuesta constitutiva macroscópica no lineal, aún contemplando un material mecánicamente lineal para las nanofibras individuales.
2. Santi, Cap2, resultados, injerto: Como se discutió previamente, una gran ventaja del modelado multiescala es la posibilidad de vincular propiedades microscópicas con la respuesta mecánica macroscópica, y
por ende brindar una herramienta para el diseño de microestructuras de materiales biomiméticos. 
3. El uso de un modelo simplificado de tipo resorte bilineal para las fibras, que permite obtener una respuesta realista sin tener que implementar las complejas ecuaciones de teoria de vigas, con el incremento en tiempo de computacion que conllevaria.
