! = = = = = = = = = =
MODULE MOD_Bundle

    ! - - - - - - - - -----
    IMPLICIT NONE
    PRIVATE ! NOTE_avoid_public_variables_if_possible
    ! - - - - - - - - -----
    PUBLIC :: Get_CurvaConstitutivaHaz
    PUBLIC :: Get_BundlesVectors
    ! - - - - - - - - -----

! = = = = = = = = = =
CONTAINS
! = = = = = = = = = =

    ! = = = = = = = = = =
    SUBROUTINE Get_CurvaConstitutivaHaz &
        (CCh_nPuntos, lambda_min, lambda_max, &
        Et, Eb, lamr0, lamrS, lamr_min, lamr_max, &
        nPuntos_cdnt, &
        CCh_lambdas, CCh_tensiones)
        ! - - - - - - - - -
        ! Calcula la curva constitutiva de un haz de fibras en funcion de de los
        ! parametros constitutivos de una fibra y de los parametros de distribu-
        ! cion normal.
        ! Primero calcula la curva de distribucion normal truncada y normalizada
        ! discreta, luego calcula el valor de tension realizando la integral dis-
        ! creta para cada valor de deformacion del haz segun:
        !
        ! s_haz = int_-lam_min^+lam_max tf(lam, lamr)*n(lamr)*dlamr
        !
        ! donde:    s_haz es la tension desarrollada por el haz
        !           tf es la tension de la fibra
        !           lam es la elongacion del haz
        !           lamr es el valor de enrulamiento de cada fibra
        !           n(lamr) es la densidad de probabilidad normal
        !
        !
        !
        ! notar:    1) que se suponen infinitas fibras en el haz
        !           2) que E es distinto a la traccion que a la flexion, por lo
        ! que hay que desdoblar la integral en dos.
        !
        ! Ver apunte "haz_curvaconstitutiva_corregido"
        ! Daniel Enrique Caballero - danielcaballero88@gmail.com
        ! - - - - - - - - -
        USE MOD_Auxiliar, ONLY : Get_CurvaDistribucionNormalTruncada, IntegCumTrapz, ComputeFromCurve
        ! - - - - - - - - -
        IMPLICIT NONE
        ! - - - - - - - - -
        INTEGER, INTENT(IN) :: CCh_nPuntos! cantidad de puntos de la curva constitutiva
        REAL(8), INTENT(IN) :: lambda_min, lambda_max
        REAL(8), INTENT(IN) :: Et, Eb, lamr0, lamrS, lamr_min, lamr_max
        INTEGER, INTENT(IN) :: nPuntos_cdnt ! cantidad de puntos de la curva distribucion normal truncada
        ! - - - - - - - - -
        REAL(8), ALLOCATABLE, INTENT(OUT) :: CCh_lambdas(:)
        REAL(8), ALLOCATABLE, INTENT(OUT) :: CCh_tensiones(:)
        ! - - - - - - - - -
        ! curva distribucion normal truncada y normalizada
        REAL(8) :: lamr(nPuntos_cdnt), pdf1(nPuntos_cdnt), cdf1(nPuntos_cdnt)
        ! integrandos e integrados de la curva de distribucion:
        REAL(8) :: pdf2(nPuntos_cdnt), cdf2(nPuntos_cdnt), pdf3(nPuntos_cdnt), cdf3(nPuntos_cdnt)
        ! auxiliares
        REAL(8) :: dlambda
        INTEGER :: ii
        REAL(8) :: lambda_i, C1, C2, C3, tension_i
        ! - - - - - - - - -

        ! - - - - - - - - -
        ALLOCATE( CCh_lambdas(CCh_nPuntos) )
        ALLOCATE( CCh_tensiones(CCh_nPuntos) )
        ! - - - - - - - - -
        dlambda = (lambda_max-lambda_min)/DFLOAT(CCh_nPuntos-1)
        ! - - - - - - - - -
        CALL Get_CurvaDistribucionNormalTruncada(lamr0, lamrS, nPuntos_cdnt, lamr_min, lamr_max, lamr, pdf1, cdf1)
        ! - - - - - - - - -
        pdf2 = pdf1 * lamr
        pdf3 = pdf1 / lamr
        cdf2 = IntegCumTrapz(nPuntos_cdnt, lamr, pdf2)
        cdf3 = IntegCumTrapz(nPuntos_cdnt, lamr, pdf3)
        DO ii=1,CCh_nPuntos
            lambda_i = lambda_min + DFLOAT(ii-1)*dlambda
            C1 = ComputeFromCurve(lambda_i, nPuntos_cdnt, lamr, cdf1, .TRUE.)
            C2 = ComputeFromCurve(lambda_i, nPuntos_cdnt, lamr, cdf2, .TRUE.)
            C3 = ComputeFromCurve(lambda_i, nPuntos_cdnt, lamr, cdf3, .TRUE.)
            tension_i = Eb*(lambda_i-1.0d0)*(1.0d0 - C1) + Eb*(C2-C1) + Et*(lambda_i*C3-C1)
            CCh_lambdas(ii) = lambda_i
            CCh_tensiones(ii) = tension_i
        END DO
        ! - - - - - - - - -

        ! - - - - - - - - -
    END SUBROUTINE Get_CurvaConstitutivaHaz
    ! = = = = = = = = = =

    ! = = = = = = = = = =
    SUBROUTINE Get_BundlesVectors(nBundles, angulo_gra, b0)
        ! - - - - - - - - -
        ! Given:   the number of bundles in the RVE
        !          and the orientation angle in degrees
        ! Returns: the unit vectors of the bundles of the RVE
        ! - - - - - - - - -
        IMPLICIT NONE
        ! - - - - - - - - -
        REAL(8), PARAMETER :: PI = 4.0d0*ATAN(1.0d0)
        ! - - - - - - - - -
        INTEGER, INTENT(IN)  :: nBundles
        REAL(8), INTENT(IN)  :: angulo_gra
        REAL(8), INTENT(OUT) :: b0(2,nBundles)
        ! - - - - - - - - -
        REAL(8) :: angulo
        REAL(8) :: dang
        INTEGER :: ii
        REAL(8) :: angulo_ii
        ! - - - - - - - - -

        ! - - - - - - - - -
        angulo = angulo_gra * PI / 180.0d0
        dang = PI/DFLOAT(nBundles)
        ! - - - - - - - - -
        DO ii=1,nBundles
            angulo_ii = angulo + DFLOAT(ii-1)*dang
            b0(1,ii) = DCOS(angulo_ii)
            b0(2,ii) = DSIN(angulo_ii)
        END DO
        ! - - - - - - - - -

        ! - - - - - - - - -
    END SUBROUTINE
    ! = = = = = = = = = =


END MODULE MOD_Bundle
! = = = = = = = = = =
