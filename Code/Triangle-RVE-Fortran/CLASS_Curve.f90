! = = = = = = = = = =
MODULE CLASS_Curve
! = = = = = = = = = =

    ! Modulo para tener variables globales accesibles
    ! Por ej: curva de deformacion-tension del material

    IMPLICIT NONE
    PRIVATE

    PUBLIC :: Curva

    TYPE Curva
        INTEGER :: n
        REAL(8), ALLOCATABLE :: x(:)
        REAL(8), ALLOCATABLE :: y(:)
    CONTAINS
        PROCEDURE :: iniciar => IniciarCurva
        PROCEDURE :: asignar => AsignarPunto
        PROCEDURE :: leer => LeerCurva
        PROCEDURE :: calcular => CalcularValor
    END TYPE

! = = = = = = = = = =
CONTAINS
! = = = = = = = = = =

    ! = = = = = = = = = =
    SUBROUTINE IniciarCurva(self, nPoints)
        ! - - - - - - - - - -
        IMPLICIT NONE
        CLASS(Curva), INTENT(INOUT) :: self
        INTEGER, INTENT(IN) :: nPoints
        ! - - - - - - - - - -
        self%n = nPoints
        ALLOCATE( self%x(nPoints) )
        ALLOCATE( self%y(nPoints) )
        ! - - - - - - - - - -
    END SUBROUTINE
    ! = = = = = = = = = =

    ! = = = = = = = = = =
    SUBROUTINE AsignarCurva(self, xdata, ydata)
        ! - - - - - - - - - -
        IMPLICIT NONE
        CLASS(Curva), INTENT(INOUT) :: self
        REAL(8), INTENT(IN) :: xdata(self%n)
        REAL(8), INTENT(IN) :: ydata(self%n)
        ! - - - - - - - - - -
        self%x = xdata
        self%y = ydata
        ! - - - - - - - - - -
    END SUBROUTINE
    ! = = = = = = = = = =

    ! = = = = = = = = = =
    SUBROUTINE AsignarPunto(self, i, xval, yval)
        ! - - - - - - - - - -
        IMPLICIT NONE
        CLASS(Curva), INTENT(INOUT) :: self
        INTEGER, INTENT(IN) :: i
        REAL(8), INTENT(IN) :: xval
        REAL(8), INTENT(IN) :: yval
        ! - - - - - - - - - -
        self%x(i) = xval
        self%y(i) = yval
        ! - - - - - - - - - -
    END SUBROUTINE
    ! = = = = = = = = = =

    ! = = = = = = = = = =
    SUBROUTINE LeerCurva(self, archivo)
        ! - - - - - - - - - -
        IMPLICIT NONE
        CLASS(Curva), INTENT(INOUT) :: self
        CHARACTER(LEN=260), INTENT(IN) :: archivo
        INTEGER :: fid, nn, ii
        ! - - - - - - - - - -
        fid = 99
        OPEN(UNIT=fid, FILE=TRIM(archivo), STATUS='OLD')
        READ(fid,*) nn
        CALL IniciarCurva(self,nn)
        DO ii=1,nn
            READ(fid,*) self%x(ii), self%y(ii)
        END DO
        CLOSE(fid)
        ! - - - - - - - - - -
    END SUBROUTINE LeerCurva
    ! = = = = = = = = = =


    ! = = = = = = = = = =
    FUNCTION CalcularValor(self, xval, extrapolar_IN) RESULT(yval)
        ! - - - - - - - - - -
        IMPLICIT NONE
        ! - - - - - - - - - -
        CLASS(Curva), INTENT(IN)        :: self
        REAL(8), INTENT(IN)             :: xval
        LOGICAL, OPTIONAL, INTENT(IN)   :: extrapolar_IN
        ! - - - - - - - - - -
        REAL(8) :: yval
        ! - - - - - - - - - -
        LOGICAL :: extrapolar
        LOGICAL :: fdi1, fdi2 ! logicals "fuera de intervalo"
        INTEGER :: iSI ! index of sub-interval where x lands in self%x (array of x values of discrete curve)
        REAL(8) :: slope
        ! - - - - - - - - - -

        ! - - - - - - - - - -
        extrapolar = .FALSE.
        IF ( PRESENT(extrapolar_IN) ) THEN
            extrapolar = extrapolar_IN
        END IF
        ! - - - - - - - - - -
        fdi1 = ( xval < self%x(1) )
        fdi2 = ( xval > self%x(self%n) )
        IF ( (fdi1) .OR. (fdi2) ) THEN
            IF ( extrapolar) THEN
                IF (fdi1) THEN
                    yval = self%y(1)
                ELSE
                    yval = self%y(self%n)
                END IF
            ELSE
                WRITE(*,*) 'Error, fuera de rango al CalcularValor() de curva'
                STOP
            END IF
        END IF
        iSI = MINLOC(self%x, DIM=1, MASK=(self%x>=xval)) - 1
        ! linear interpolation within the sub-interval
        slope = ( self%y(iSI+1) - self%y(iSI) ) / ( self%x(iSI+1) - self%x(iSI) )
        yval = self%y(iSI) + slope * (xval - self%x(iSI))
        ! - - - - - - - - - -

        ! - - - - - - - - - -
    END FUNCTION CalcularValor
    ! = = = = = = = = = =

! = = = = = = = = = =
END MODULE CLASS_Curve
! = = = = = = = = = =
