! = = = = = = = = = =
MODULE MOD_Programs

    ! - - - - - - - - - -
    IMPLICIT NONE
    PRIVATE ! NOTE_avoid_public_variables_if_possible
    ! - - - - - - - - - -
    PUBLIC :: RVE
    PUBLIC :: Haz_Curva
    PUBLIC :: RVE_Optimizar
    ! - - - - - - - - - -

! = = = = = = = = = =
CONTAINS
! = = = = = = = = = =


    ! = = = = = = = = = =
    SUBROUTINE Haz_Curva(configurationFile, outputFile)
        ! - - - - - - - - - -
        USE MOD_Bundle, ONLY : Get_CurvaConstitutivaHaz
        USE MOD_Auxiliar, ONLY : FindStringInFile
        IMPLICIT NONE
        ! - - - - - - - - - -
        INTEGER, PARAMETER :: nDim = 2
        ! - - - - - - - - - -
        CHARACTER(LEN=*), INTENT(IN) :: configurationFile
        CHARACTER(LEN=*), INTENT(IN) :: outputFile
        ! - - - - - - - - - -
        ! Elastic parameters
        REAL(8) :: Et
        REAL(8) :: Eb
        ! Bundle parameters
        REAL(8) :: lamr0, lamrS, lamr_min, lamr_max
        ! Parametros haz - discretizacion curva constitutiva
        INTEGER :: CC_nPuntos, CC_precision
        REAL(8) :: CC_lam_min, CC_lam_max
        ! Curva constitutiva
        REAL(8), ALLOCATABLE :: CC_lam(:), CC_ten(:)
        ! RVE parameters
        INTEGER :: nBundlesRVE
        REAL(8), ALLOCATABLE :: bundles_v0(:,:)
        REAL(8) :: angleRVE
        ! Mat parameters
        REAL(8) :: volFraction
        ! - - - - - - - - - -
        ! Counter
        INTEGER :: last, i
        ! Reading status
        INTEGER :: iStatus
        ! File ID
        INTEGER, PARAMETER :: fid1 = 11
        ! Current working directory
        CHARACTER(LEN=255) :: cwd
        ! - - - - - - - - - -
        ! Arrays for optimization
        INTEGER :: numParFix
        REAL(8), ALLOCATABLE :: ParFix(:)
        INTEGER :: numParFit
        REAL(8), ALLOCATABLE :: ParFit(:)
        ! Fitting
        REAL(8) :: MEA, TSS, SSR, R2
        ! - - - - - - - - - -

        ! - - - - - - - - -
        WRITE(*,*) "Configuracion en: ", TRIM(configurationFile)
        WRITE(*,*) "Salida en: ", TRIM(outputFile)
        ! - - - - - - - - -
        ! Leer parametros
        OPEN(UNIT=fid1, FILE=TRIM(configurationFile), STATUS="OLD")
        iStatus = FindStringInFile("*FIXPAR", fid1, .TRUE.)
        READ(fid1,*) numParFix
        ALLOCATE( ParFix(numParFix) )
        READ(fid1,*) ParFix
        iStatus = FindStringInFile("*FITPAR", fid1, .TRUE.)
        READ(fid1,*) numParFit
        ALLOCATE( ParFit(numParFit) )
        READ(fid1,*) ParFit
        CLOSE(UNIT=fid1)
        ! - - - - - - - - -
        ! Parametros
        nBundlesRVE = NINT(ParFix(1))
        ALLOCATE( bundles_v0(nDim, nBundlesRVE) )
        bundles_v0 = RESHAPE( ParFix(2:1+nDim*nBundlesRVE), SHAPE(bundles_v0) )
        last = 1 + nDim*nBundlesRVE
        !
        CC_nPuntos = ParFix(last+1)
        CC_lam_min = ParFix(last+2)
        CC_lam_max = ParFix(last+3)
        CC_precision = ParFix(last+4)
        !
        Et = ParFit(1)
        Eb = ParFit(2)
        lamr0 = ParFit(4)
        lamrS = ParFit(5)
        lamr_min = ParFit(6)
        lamr_max = ParFit(7)
        ! - - - - - - - - -
        ! Calcular curva constitutiva de un haz
        CALL Get_CurvaConstitutivaHaz &
        (CC_nPuntos, CC_lam_min, CC_lam_max, &
        Et, Eb, lamr0, lamrS, lamr_min, lamr_max, &
        CC_precision, &
        CC_lam, CC_ten)
        ! - - - - - - - - -
        OPEN(UNIT=fid1,FILE=outputFile,STATUS="replace")
        WRITE(fid1, *) CC_nPuntos
        DO i=1,CC_nPuntos
            WRITE(fid1,'(2E20.8E3)') CC_lam(i), CC_ten(i)
        END DO
        CLOSE(fid1)
        ! - - - - - - - - -


        ! - - - - - - - - - -
    END SUBROUTINE Haz_Curva
    ! = = = = = = = = = =


    ! = = = = = = = = = =
    SUBROUTINE RVE(ExtFun_Curva, configurationFile, outputFile)
        ! - - - - - - - - - -
        ! Esta subrutina computa y escribe en archivo la curva constitutiva de
        ! un RVE, el tipo de deformacion aplicado viene dado por la subrutina
        ! externa ExtFun_Curva. Puede ser uniaxial, biaxial, simple. (Ver ejem-
        ! plos en el modulo MOD_RVE)
        ! - - - - - - - - - -
        USE MOD_Auxiliar, ONLY : FindStringInFile, Compute_SSR
        IMPLICIT NONE
        ! - - - - - - - - - -
        INTEGER, PARAMETER :: nDim = 2
        ! - - - - - - - - - -
        EXTERNAL :: ExtFun_Curva
        INTERFACE
            SUBROUTINE ExtFun_Curva(nPuntos, lam_array, nPar1, Par1, nPar2, Par2, P)
                INTEGER, PARAMETER   :: nDim = 2
                INTEGER, INTENT(IN)  :: nPuntos
                REAL(8), INTENT(IN)  :: lam_array(nPuntos)
                INTEGER, INTENT(IN)  :: nPar1
                REAL(8), INTENT(IN)  :: Par1(nPar1)
                INTEGER, INTENT(IN)  :: nPar2
                REAL(8), INTENT(IN)  :: Par2(nPar2)
                REAL(8), INTENT(OUT) :: P(nDim,nDim,nPuntos)
            END SUBROUTINE
        END INTERFACE
        CHARACTER(LEN=*), INTENT(IN) :: configurationFile
        CHARACTER(LEN=*), INTENT(IN) :: outputFile
        ! - - - - - - - - - -
        ! Counter
        INTEGER :: last, i
        ! Reading status
        INTEGER :: iStatus
        ! Data arrays
        INTEGER :: numExpDataPoints, lambdas_n
        REAL(8) :: lambdas_ini, lambdas_fin, lambdas_delta
        REAL(8), ALLOCATABLE :: exp_lam(:), exp_ten(:)
        !
        REAL(8), ALLOCATABLE :: sim_stress(:,:,:)
        ! File ID
        INTEGER, PARAMETER :: fid1 = 11, fid2=12
        ! Current working directory
        CHARACTER(LEN=255) :: cwd
        ! - - - - - - - - - -
        ! Arrays for optimization
        INTEGER :: numParFix
        REAL(8), ALLOCATABLE :: ParFix(:)
        INTEGER :: numParFit
        REAL(8), ALLOCATABLE :: ParFit(:)
        ! Fitting
        REAL(8) :: MEA, TSS, SSR, Residuo
        ! - - - - - - - - - -

        ! - - - - - - - - -
        WRITE(*,*) "Configuracion en: ", TRIM(configurationFile)
        WRITE(*,*) "Salida en: ", TRIM(outputFile)
        ! - - - - - - - - -
        ! LEER PARAMETROS
        OPEN(UNIT=fid1, FILE=TRIM(configurationFile), STATUS="OLD")
        ! parametros fijos
        iStatus = FindStringInFile("*FIXPAR", fid1, .TRUE.)
        READ(fid1,*) numParFix
        ALLOCATE( ParFix(numParFix) )
        READ(fid1,*) ParFix
        ! parametros a estimar
        iStatus = FindStringInFile("*FITPAR", fid1, .TRUE.)
        READ(fid1,*) numParFit
        ALLOCATE( ParFit(numParFit) )
        READ(fid1,*) ParFit
        ! - - - - - - - - -
        ! LEER DEFORMACIONES Y TENSIONES
        ! chequear si los lambdas se dan en 0d.config
        iStatus = FindStringInFile("*LAMBDAS", fid1, .FALSE.)
        IF (iStatus == 0) THEN
            ! si se dan ahi se ensambla el array de lambdas a partir de unos pocos parametros
            ! y a la tension se asigna un array de ceros
            READ(fid1,*) lambdas_ini, lambdas_fin, lambdas_n
            ! Ensamblar array de deformaciones para computar tensiones
            ALLOCATE( exp_lam(lambdas_n) )
            ALLOCATE( exp_ten(lambdas_n) )
            exp_ten = 0.0d0
            exp_lam(1) = lambdas_ini
            lambdas_delta = (lambdas_fin-lambdas_ini)/DFLOAT(lambdas_n-1)
            DO i=2,lambdas_n
                exp_lam(i) = exp_lam(i-1)+lambdas_delta
            END DO
        ELSE IF (iStatus < 0) THEN
            ! si no se dan en 0d.config entonces se lee de una curva experimental
            OPEN( UNIT=fid2, FILE="curva_experimental.txt", STATUS="old")
            READ(fid2,*) numExpDataPoints
            lambdas_n = numExpDataPoints
            ALLOCATE( exp_lam(lambdas_n) )
            ALLOCATE( exp_ten(lambdas_n) )
            DO i=1,lambdas_n
                READ(fid2, *) exp_lam(i), exp_ten(i)
            END DO
            CLOSE(UNIT=fid2)
            ! Mean average and total sum of squares
            MEA = SUM(exp_ten) / lambdas_n
            TSS = SUM( (exp_lam-MEA)*(exp_lam-MEA) )
        ELSE
            WRITE(*,*) "Error leyendo archivo, iStatus= ", iStatus
            STOP
        END IF

        CLOSE(UNIT=fid1)
        ! - - - - - - - - -

        ! - - - - - - - - -
        ! COMPUTAR CURVA CONSTITUTIVA TENSION-DEFORMACION
        ALLOCATE( sim_stress(nDim, nDim, lambdas_n), STAT=iStatus)
        CALL ExtFun_Curva &
        (lambdas_n, exp_lam, &
        numParFix, ParFix, &
        numParFit, ParFit, &
        sim_stress)
        ! - - - - - - - - -
        ! ESCRIBIR CURVA CONSTITUTIVA
        OPEN(UNIT=fid1,FILE=outputFile,STATUS="replace")
        IF (iStatus<0) THEN
            ! Residuo
            SSR = Compute_SSR(exp_ten, sim_stress(1,1,:), lambdas_n)
            Residuo = SSR/TSS
            WRITE(*,*) "Residuo: ", Residuo
        END IF
        WRITE(fid1,"(6A20)") "exp_lambda", "exp_stress", "P11", "P21", "P12", "P22"
        DO i=1,lambdas_n
            WRITE(fid1,'(6E20.8E3)') exp_lam(i), exp_ten(i), sim_stress(:,:,i)
        END DO
        CLOSE(fid1)
        ! - - - - - - - - -


        ! - - - - - - - - - -
    END SUBROUTINE RVE
    ! = = = = = = = = = =


    ! = = = = = = = = = =
    SUBROUTINE RVE_Optimizar(ExtFun_Curva, configurationFile, outputFile)
        ! - - - - - - - - - -
        USE MOD_Optimization, ONLY : BruteForceOptimization
        USE MOD_Auxiliar, ONLY : FindStringInFile
        IMPLICIT NONE
        ! - - - - - - - - - -
        INTEGER, PARAMETER :: nDim = 2
        ! - - - - - - - - - -
        EXTERNAL :: ExtFun_Curva
        INTERFACE
            SUBROUTINE ExtFun_Curva(nPuntos, lam_array, nPar1, Par1, nPar2, Par2, P)
                INTEGER, PARAMETER   :: nDim = 2
                INTEGER, INTENT(IN)  :: nPuntos
                REAL(8), INTENT(IN)  :: lam_array(nPuntos)
                INTEGER, INTENT(IN)  :: nPar1
                REAL(8), INTENT(IN)  :: Par1(nPar1)
                INTEGER, INTENT(IN)  :: nPar2
                REAL(8), INTENT(IN)  :: Par2(nPar2)
                REAL(8), INTENT(OUT) :: P(nDim,nDim,nPuntos)
            END SUBROUTINE
        END INTERFACE
        CHARACTER(LEN=*), INTENT(IN) :: configurationFile
        CHARACTER(LEN=*), INTENT(IN) :: outputFile
        ! - - - - - - - - - -
        ! Counter
        INTEGER :: i
        ! Reading Status
        INTEGER :: iStatus
        ! Data arrays
        INTEGER :: numExpDataPoints
        REAL(8), ALLOCATABLE :: exp_lambdas(:)
        REAL(8), ALLOCATABLE :: exp_stress(:)
        !real(8) :: sim_stress(2,2,60)
        REAL(8), ALLOCATABLE :: sim_P(:,:,:)
        REAL(8), ALLOCATABLE :: sim_P_old(:,:,:)
        REAL(8), ALLOCATABLE :: sim_F22(:)
        ! File ID
        INTEGER, PARAMETER :: fid1 = 11
        ! Current working directory
        CHARACTER(LEN=255) :: cwd
        ! - - - - - - - - - -
        ! Arrays for optimization
        INTEGER :: numParFix
        REAL(8), ALLOCATABLE :: ParFix(:)
        INTEGER :: numParFit
        REAL(8), ALLOCATABLE :: ParFit(:)
        REAL(8), ALLOCATABLE :: ParFitUpd(:)
        REAL(8), ALLOCATABLE :: FitDeltas(:)
        LOGICAL, ALLOCATABLE :: FitMask(:)
        INTEGER              :: maxiter_opt
        REAL(8)              :: tolerance_opt
        REAL(8)              :: Residuo
        ! - - - - - - - - - -


        ! - - - - - - - - - -
        ! Leer parametros
        OPEN(UNIT=fid1, FILE=TRIM(configurationFile), STATUS="OLD")

        iStatus = FindStringInFile("*FIXPAR", fid1, .TRUE.)
        READ(fid1,*) numParFix
        ALLOCATE( ParFix(numParFix) )
        READ(fid1,*) ParFix

        iStatus = FindStringInFile("*FITPAR", fid1, .TRUE.)
        READ(fid1,*) numParFit
        ALLOCATE( ParFit(numParFit) )
        READ(fid1,*) ParFit

        iStatus = FindStringInFile('*OPTPAR', fid1, .TRUE.)
        ALLOCATE( FitDeltas(numParFit) )
        ALLOCATE( FitMask(numParFit) )
        READ(fid1,*) maxiter_opt, tolerance_opt
        READ(fid1,*) FitDeltas
        READ(fid1,*) FitMask
        CLOSE(UNIT=fid1)
        ! - - - - - - - - - -
        OPEN(UNIT=fid1, FILE='FitParUpd.txt', STATUS='old')
        READ(fid1,*) ParFit
        CLOSE(fid1)
        ! - - - - - - - - - -

        ! - - - - - - - - - -
        ! Read experimental data
        OPEN( UNIT=fid1, FILE='curva_experimental.txt', STATUS='old')
        READ(fid1,*) numExpDataPoints
        ALLOCATE( exp_lambdas(numExpDataPoints) )
        ALLOCATE( exp_stress(numExpDataPoints) )
        DO i=1,numExpDataPoints
            READ(fid1, *) exp_lambdas(i), exp_stress(i)
        END DO
        exp_lambdas = exp_lambdas
        exp_stress = exp_stress
        CLOSE(fid1)
        ! - - - - - - - - - -

        ! - - - - - - - - -
        ALLOCATE( ParFitUpd(numParFit) )
        ! - - - - - - - - -
        CALL BruteForceOptimization(ExtFun_Curva, &
        numExpDataPoints, exp_lambdas, exp_stress, numParFix, ParFix, numParFit, ParFit, &
        FitDeltas, FitMask, maxiter_opt, tolerance_opt, .TRUE., 99, &
        ParFitUpd, Residuo)
        ! - - - - - - - - -
        OPEN(UNIT=fid1, FILE='FitParUpd.txt', STATUS='replace')
        WRITE(fid1,'(F20.8, E20.8, 4F20.8)') ParFitUpd
        WRITE(fid1,*) Residuo
        CLOSE(fid1)
        ! - - - - - - - - -
        ALLOCATE( sim_P_old(nDim, nDim, numExpDataPoints) )
        CALL ExtFun_Curva(numExpDataPoints, exp_lambdas, numParFix, ParFix, numParFit, ParFit, sim_P_old)
        ALLOCATE( sim_P(nDim, nDim, numExpDataPoints) )
        CALL ExtFun_Curva(numExpDataPoints, exp_lambdas, numParFix, ParFix, numParFit, ParFitUpd, sim_P)
        ! - - - - - - - - -
        OPEN(UNIT=fid1,FILE=outputFile,STATUS='replace')
        WRITE(fid1,'(10A20)') 'exp_lambda', 'exp_stress', 'P11', 'P21', 'P12', 'P22', 'Pold11', 'Pold21', 'Pold12', 'Pold22'
        DO i=1,numExpDataPoints
            WRITE(fid1,'(10E20.8E3)') exp_lambdas(i), exp_stress(i), sim_P(:,:,i), sim_P_old(:,:,i)
        END DO
        CLOSE(UNIT=fid1)
        ! - - - - - - - - -


        ! - - - - - - - - - -
    END SUBROUTINE RVE_Optimizar
    ! = = = = = = = = = =



END MODULE MOD_Programs
