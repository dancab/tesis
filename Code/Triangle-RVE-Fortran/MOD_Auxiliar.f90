! = = = = = = = = = =
MODULE MOD_Auxiliar

    ! - - - - - - - - -
    IMPLICIT NONE
    PRIVATE ! NOTE_avoid_public_variables_if_possible
    ! - - - - - - - - -
    PUBLIC :: ComputeFromCurve
    PUBLIC :: OuterProduct_2x2
    PUBLIC :: FindStringInFile
    PUBLIC :: DeltaArray
    PUBLIC :: TxV_rV
    PUBLIC :: VxTxV_rR
    PUBLIC :: VxV_rR
    PUBLIC :: Compute_SSR
    PUBLIC :: Get_CurvaDistribucionNormalTruncada
    PUBLIC :: IntegCumTrapz
    ! - - - - - - - - -

! = = = = = = = = = =
CONTAINS
! = = = = = = = = = =


    ! = = = = = = = = = =
    FUNCTION ComputeFromCurve(x, nPoints, xArray, yArray, extrapolar_IN) RESULT(y)
        ! - - - - - - - - -
        ! Esta funcion calcula el valor y=f(x) de una curva f(x) discreta
        ! INPUT:
        !       x= valor de variable independiente
        !       nPoints= numero de puntos en la curva discreta
        !       xArray= valores de x de la curva discreta
        !       yArray= valores de y de la curva discreta
        !       extrapolar_IN= opcional, indica si se extrapola en caso de que
        !                      x caiga fuera de xArray. Se interpola con los
        !                      valores de los extremos
        ! - - - - - - - - -
        IMPLICIT NONE
        ! - - - - - - - - -
        REAL(8), INTENT(IN) :: x
        INTEGER, INTENT(IN) :: nPoints
        REAL(8), INTENT(IN) :: xArray(nPoints)
        REAL(8), INTENT(IN) :: yArray(nPoints)
        LOGICAL, OPTIONAL, INTENT(IN) :: extrapolar_IN
        ! - - - - - - - - -
        REAL(8) :: y
        ! - - - - - - - - -
        LOGICAL :: extrapolar
        LOGICAL :: fdi1, fdi2 ! logicals "fuera de intervalo"
        INTEGER :: i_pre ! indice del valor previo a x en xArray
        REAL(8) :: slope
        ! - - - - - - - - -

        ! - - - - - - - - -
        extrapolar = .FALSE.
        IF ( PRESENT(extrapolar_IN) ) THEN
            extrapolar = extrapolar_IN
        END IF
        ! - - - - - - - - -
        fdi1 = ( x < xArray(1) )
        fdi2 = ( x > xArray(nPoints) )
        IF ( (fdi1) .OR. (fdi2) ) THEN
            IF ( extrapolar) THEN
                IF (fdi1) THEN
                    y = yArray(1)
                ELSE
                    y = yArray(nPoints)
                END IF
            ELSE
                WRITE(*,*) 'Error, trying to comput out of bounds - ComputeFromCurve()'
                WRITE(*,*) 'xmin, xmax, x: ', xArray(1), xArray(nPoints), x
                STOP
            END IF
        ELSE
            DO i_pre=2,nPoints
                IF (x < xArray(i_pre)) THEN
                    EXIT
                END IF
            END DO
            i_pre = i_pre - 1
            ! linear interpolation within the sub-interval
            slope = ( yArray(i_pre+1) - yArray(i_pre) ) / ( xArray(i_pre+1) - xArray(i_pre) )
            y = yArray(i_pre) + slope * (x - xArray(i_pre))
        END IF
        ! - - - - - - - - -

        ! - - - - - - - - -
    END FUNCTION ComputeFromCurve
    ! = = = = = = = = = =


    ! = = = = = = = = = =
    FUNCTION OuterProduct_2x2(x, y) RESULT(outer) ! Producto tensorial entre 2 vectores
        ! - - - - - - - - -
        IMPLICIT NONE
        ! - - - - - - - - -
        ! Parameters
        INTEGER, PARAMETER :: nDim = 2
        ! - - - - - - - - -
        ! Arguments
        REAL(8), INTENT(IN) :: x(nDim), y(nDim) !vectors of size nDim
        ! - - - - - - - - -
        ! Result
        REAL(8) :: outer(nDim,nDim)
        ! - - - - - - - - -
        ! Locals
        INTEGER :: i,j
        ! - - - - - - - - -

        ! - - - - - - - - -
        DO i=1,nDim
            DO j=1,nDim
                outer(i,j) = x(i)*y(j)
            END DO
        END DO
        ! - - - - - - - - -

        ! - - - - - - - - -
    END FUNCTION
    ! =========================================================================


    ! = = = = = = = = = ===
    ! = = = = = = = = = ===
    FUNCTION FindStringInFile(Str, ioUnit, Mandatory) RESULT (iError)
        ! = = = = = = = = = ===
        ! Busca un String en un archivo (STR), sino lo encuentra rebovina el archivo
        ! y pone iError < 0 como indicador de no hallazgo
        ! Str: String to find, ioUnit: Unit assigned to Input File; iError: Error Status variable
        ! = = = = = = = = = ===
        IMPLICIT NONE
        ! = = = = = = = = = ===
        ! Parameters
        LOGICAL,PARAMETER  :: Segui=.True.
        ! = = = = = = = = = ===
        ! Arguments
        CHARACTER(*), INTENT(IN) :: Str
        INTEGER, INTENT (IN) :: ioUnit
        LOGICAL, OPTIONAL, INTENT(IN) :: Mandatory
        ! = = = = = = = = = ===
        ! Locals
        LOGICAL :: MandatoryL
        CHARACTER(LEN=120) :: DummyString
        CHARACTER(LEN=120) :: upStr
        INTEGER :: iError
        INTEGER :: Leng
        ! = = = = = = = = = ===

        ! = = = = = = = = = ===
        IF ( PRESENT(Mandatory) ) THEN
            MandatoryL = Mandatory
        ELSE
            MandatoryL = .FALSE.
        END IF
        ! = = = = = = = = = ===
        iError=0
        Leng = LEN_TRIM(Str)
        upStr = Upper_Case(Str)       ! Line added by NB. Converts Str to Upper Case string
        ! = = = = = = = = = ===
        REWIND(ioUnit)
        Search_Loop: DO WHILE (segui)
            READ(ioUnit,'(1A120)',IOSTAT=iError) DummyString
            DummyString = Upper_Case(DummyString)   ! line added by NB
            !       if (iError==59) backspace(ioUnit)
            IF (iError.lt.0) THEN
                REWIND (ioUnit)
                EXIT Search_Loop
            END IF
            IF ( DummyString(1:1)    /=    '*'      ) CYCLE Search_Loop
            IF ( DummyString(1:Leng) == upStr(1:Leng) ) EXIT Search_Loop
        END DO Search_Loop
        ! = = = = = = = = = ===
        IF (MandatoryL) THEN
            IF ( .NOT. ( iError == 0 ) ) THEN
                WRITE(*,*) upStr, 'NOT FOUND'
                STOP
            END IF
        END IF
        ! = = = = = = = = = ===

    END FUNCTION FindStringInFile
    ! = = = = = = = = = ===
    ! = = = = = = = = = ===


    ! = = = = = = = = = ===
    ! = = = = = = = = = ===
    FUNCTION Upper_Case(string)

        !-------------------------------------------------------------------------------------------
        !! The Upper_Case function converts the lower case characters of a string to upper case one.
        !! Use this function in order to achieve case-insensitive: all character variables used
        !! are pre-processed by Uppper_Case function before these variables are used. So the users
        !! can call functions without pey attention of case of the keywords passed to the functions.
        !-------------------------------------------------------------------------------------------

        IMPLICIT NONE

        !-------------------------------------------------------------------------------------------
        CHARACTER(LEN=*), INTENT(IN):: string     ! string to be converted
        CHARACTER(LEN=LEN(string))::   Upper_Case ! converted string
        INTEGER::                      n1         ! characters counter
        !-------------------------------------------------------------------------------------------

        Upper_Case = string
        DO n1=1,LEN(string)
            SELECT CASE(ICHAR(string(n1:n1)))
                CASE(97:122)
                    Upper_Case(n1:n1)=CHAR(ICHAR(string(n1:n1))-32) ! Upper case conversion
            END SELECT
        ENDDO
        RETURN

        !-----------------------------------------------------------------------------------------------
    END FUNCTION Upper_Case
    ! = = = = = = = = = ===
    ! = = = = = = = = = ===


    ! = = = = = = = = = =
    FUNCTION DeltaArray(n,i) RESULT(d)
        ! - - - - - - - - -
        IMPLICIT NONE
        ! - - - - - - - - -
        INTEGER, INTENT(IN) :: n,i
        REAL(8) :: d(n)
        ! - - - - - - - - -

        ! - - - - - - - - -
        d=0.0d0
        d(i) = 1.0d0
        ! - - - - - - - - -

        ! - - - - - - - - -
    END FUNCTION
    ! = = = = = = = = = =


    ! = = = = = = = = = =
    FUNCTION VxV_rR(v1, v2, n) RESULT(r)
        IMPLICIT NONE
        REAL(8), INTENT(IN) :: v1(n), v2(n)
        INTEGER, INTENT(IN) :: n
        REAL(8) :: r
        INTEGER :: i

        r = 0.0d0
        DO i=1,n
            r = r + v1(i)*v2(i)
        END DO

    END FUNCTION
    ! = = = = = = = = = =


    ! = = = = = = = = = =
    FUNCTION TxV_rV(T, v, n) RESULT(w)
        IMPLICIT NONE
        REAL(8), INTENT(IN) :: T(n,n), v(n)
        INTEGER, INTENT(IN) :: n
        REAL(8) :: w(n)
        INTEGER :: i, j

        w = 0.0d0
        DO i=1,n
            DO j=1,n
                w(i) = w(i) + T(i,j)*v(j)
            END DO
        END DO

    END FUNCTION
    ! = = = = = = = = = =


    ! = = = = = = = = = =
    FUNCTION VxTxV_rR(v1, T, v2, n) RESULT(r)
        IMPLICIT NONE
        REAL(8), INTENT(IN) :: v1(n), T(n,n), v2(n)
        INTEGER, INTENT(IN) :: n
        REAL(8) :: r
        REAL(8) :: w(n)
        INTEGER :: i,j

        w = TxV_rV(T,v2,n)
        r = VxV_rR(v1,w,n)

    END FUNCTION
    ! = = = = = = = = = =


    ! = = = = = = = = = =
    FUNCTION Compute_SSR(expArray, simArray, n) RESULT(SSR)
        ! - - - - - - - - -
        ! Calculo de residuo global como suma de residuos cuadrados
        ! (sum of squared residuals: SSR)
        ! - - - - - - - - -
        IMPLICIT NONE
        REAL(8), INTENT(IN) :: expArray(n), simArray(n)
        INTEGER, INTENT(IN) :: n
        REAL(8) :: SSR

        SSR = SUM((simArray- expArray)*(simArray- expArray))
        !SSR = SSR + 100.d0*simArray(1)*simArray(1)
        !SSR = SSR + 1000.d0*SUM(simArray(1:5)*simArray(1:5))

    END FUNCTION
    ! = = = = = = = = = =


    ! = = = = = = = = = =
    SUBROUTINE Get_CurvaDistribucionNormal(mu, sigma, nPuntos, xx, pdf, cdf)
        ! - - - - - - - - -
        ! Devuelve una curva de distribucion normal
        ! mu = valor medio
        ! sigma = desviacion estandar
        ! npuntos = numero de puntos de los arrays de la curva
        ! xx = array de valores de abscisas
        ! pdf = array de distribucion normal
        ! cdf = array de distribucion normal acumulada
        !
        ! NOTA: la curva esta truncada en "mu-10*sigma" y "mu+10*sigma"
        ! - - - - - - - - -
        IMPLICIT NONE
        ! - - - - - - - - -
        REAL(8), PARAMETER :: PI = 4.0d0*DATAN(1.0d0)
        ! - - - - - - - - -
        REAL(8), INTENT(IN)  :: mu, sigma
        INTEGER, INTENT(IN)  :: nPuntos
        REAL(8), INTENT(OUT) :: xx(nPuntos), pdf(nPuntos), cdf(nPuntos)
        ! - - - - - - - - -
        REAL(8) :: x_ini, x_fin, dx, x
        INTEGER :: ii
        ! - - - - - - - - -

        ! - - - - - - - - -
        x_ini = mu - 10.0d0*sigma
        x_fin = mu + 10.0d0*sigma
        dx = (x_fin - x_ini ) / DFLOAT(nPuntos-1)
        ! - - - - - - - - -
        DO ii=1,nPuntos
            x = x_ini + DFLOAT(ii-1)*dx
            xx(ii)  = x
            pdf(ii) = (1.0d0 / DSQRT(2.0d0*PI*sigma**2)) * DEXP( - (x-mu)**2 / (2.0d0*sigma**2) )
            cdf(ii) = 0.5d0 * ( 1.0d0 + ERF( (x-mu) / (DSQRT(2.0d0)*sigma) ) )
        END DO
        ! - - - - - - - - -

        ! - - - - - - - - -
    END SUBROUTINE Get_CurvaDistribucionNormal
    ! = = = = = = = = = =


    ! = = = = = = = = = =
    SUBROUTINE Get_CurvaDistribucionNormalTruncada(mu, sigma, nPuntos, x_min, x_max, xx, pdf, cdf)
        ! - - - - - - - - -
        ! Devuelve una curva de distribucion normal
        ! la curva esta truncada entre "x_min" y "x_max"
        ! y renormalizada para que el area bajo la curva sea 1
        !
        ! mu = valor medio
        ! sigma = desviacion estandar
        ! npuntos = numero de puntos de los arrays de la curva
        ! xx = array de valores de abscisas
        ! pdf = array de distribucion normal
        ! cdf = array de distribucion normal acumulada
        ! - - - - - - - - -
        IMPLICIT NONE
        ! - - - - - - - - -
        REAL(8), PARAMETER :: PI = 4.0d0*DATAN(1.0d0)
        ! - - - - - - - - -
        REAL(8), INTENT(IN)  :: mu, sigma
        INTEGER, INTENT(IN)  :: nPuntos
        REAL(8), INTENT(IN)  :: x_min, x_max ! limites donde se trunca la distribucion normal
        REAL(8), INTENT(OUT) :: xx(nPuntos), pdf(nPuntos), cdf(nPuntos)
        ! - - - - - - - - -
        REAL(8) :: dx, x
        INTEGER :: ii
        REAL(8) :: aux
        ! - - - - - - - - -

        ! - - - - - - - - -
        ! calculo el valor de espaciado entre puntos
        dx = (x_max - x_min ) / DFLOAT(nPuntos-1)
        ! - - - - - - - - -
        ! ensamblo el array de abscisas (xx) y el de ordenadas (pdf)
        DO ii=1,nPuntos
            x = x_min + DFLOAT(ii-1)*dx
            xx(ii)  = x
            pdf(ii) = (1.0d0 / DSQRT(2.0d0*PI*sigma**2)) * DEXP( - (x-mu)**2 / (2.0d0*sigma**2) )
        END DO
        ! - - - - - - - - -
        ! computo el array de distribucion acumulada sin normalizar (cdf)
        cdf = IntegCumTrapz(nPuntos, xx, pdf)
        ! normalizo las curvas
        aux = cdf(nPuntos)
        DO ii=1,nPuntos
            pdf(ii) = pdf(ii) / aux
            cdf(ii) = cdf(ii) / aux
        END DO
        ! - - - - - - - - -

        ! - - - - - - - - -
    END SUBROUTINE Get_CurvaDistribucionNormalTruncada
    ! = = = = = = = = = =


    ! = = = = = = = = = =
    FUNCTION IntegCumTrapz(nn, xx, yy) RESULT(yy_acum)
        ! - - - - - - - - -
        ! Integral Cumulative Trapezoidal
        ! Regla de integracion trapezoidal para una curva discreta
        !
        ! nn: numero de puntos de la curva de entrada
        ! xx: valores de abscisas
        ! yy: valores de ordenadas
        ! yy_acum: valores de la integral a medida que se acumula
        ! - - - - - - - - -
        IMPLICIT NONE
        ! - - - - - - - - -
        INTEGER, INTENT(IN)  :: nn
        REAL(8), INTENT(IN)  :: xx(nn), yy(nn)
        ! - - - - - - - - -
        REAL(8) :: yy_acum(nn)
        ! - - - - - - - - -
        INTEGER :: ii
        REAL(8) :: y_medio, delta_x
        ! - - - - - - - - -

        ! - - - - - - - - -
        yy_acum(1) = 0.0d0
        DO ii=2,nn
            y_medio = 0.5d0 * ( yy(ii-1) + yy(ii) )
            delta_x = xx(ii) - xx(ii-1)
            yy_acum(ii) = yy_acum(ii-1) + y_medio*delta_x
        END DO
        ! - - - - - - - - -

        ! - - - - - - - - -
    END FUNCTION IntegCumTrapz
    ! = = = = = = = = = =


END MODULE MOD_Auxiliar
! = = = = = = = = = =
