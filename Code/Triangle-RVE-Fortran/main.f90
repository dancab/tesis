
PROGRAM TrianglesRVE
    USE MOD_Programs, ONLY : Haz_Curva, RVE, RVE_Optimizar
    USE MOD_RVE, ONLY : RVE_Curva_Simple, RVE_Curva_Biaxial
    USE MOD_Auxiliar, ONLY : FindStringInFile
    ! = = = = = = = = = =
    IMPLICIT NONE
    ! = = = = = = = = = =
    CHARACTER(LEN=120) :: configFile
    CHARACTER(LEN=120) :: outFile
    ! = = = = = = = = = =
    ! Current working directory
    CHARACTER(LEN=255) :: cwd
    ! = = = = = = = = = =
    INTEGER :: fid
    INTEGER :: iStatus
    CHARACTER(LEN=10) :: instruccion
    ! = = = = = = = = = =


    CALL GETCWD(cwd)

    configFile = '0d.config'
    fid = 11
    OPEN(UNIT=fid, FILE=TRIM(configFile), STATUS="OLD")
    iStatus = FindStringInFile("*CONTROL", fid, .TRUE.)
    READ(fid,*) instruccion
    READ(fid,*) outFile
    CLOSE(UNIT=fid)


    IF (TRIM(instruccion) == "computar") THEN
        CALL RVE(RVE_Curva_Simple, configFile, outFile)
    ELSEIF (TRIM(instruccion) == "optimizar") THEN
        CALL RVE_Optimizar(RVE_Curva_Simple, configFile, outFile)
    ELSEIF (TRIM(instruccion) == "curva_haz") THEN
        CALL Haz_Curva(configFile, outFile)
    ELSE
        WRITE(*,*) "instruccion erronea, no se hace nada"
    END IF

    STOP

END PROGRAM TrianglesRVE

