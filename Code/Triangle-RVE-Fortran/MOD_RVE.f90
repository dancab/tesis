! = = = = = = = = = =
MODULE MOD_RVE

    ! - - - - - - - - - -
    IMPLICIT NONE
    PRIVATE ! NOTE_avoid_public_variables_if_possible
    ! - - - - - - - - - -
    PUBLIC :: RVE_Curva_Biaxial
    PUBLIC :: RVE_Curva_Simple
    ! - - - - - - - - - -

! = = = = = = = = = =
CONTAINS
! = = = = = = = = = =



    ! = = = = = = = = = =
    ! ROUTINE:
    ! RVE_Fibers_Initialization
    !
    ! DESCRIPTION:
    ! Given the number of fibers that compose the RVE
    ! returns the fibers initial unity vectors as the columns of an array of size=(2, nFibers)
    ! Also takes in an optional argument to rotate the entire RVE counter-clock-wise (in degrees)
    ! = = = = = = = = = =
    FUNCTION RVE_Deformation(FF, nMembers, members0) RESULT(lambdas)
        ! - - - - - - - - - -
        IMPLICIT NONE
        ! - - - - - - - - - -
        REAL(8), INTENT(IN)    :: FF(2,2)              ! Deformation gradient
        INTEGER, INTENT(IN)    :: nMembers              ! Number of fibers that compose the RVE
        REAL(8), INTENT(IN)    :: members0(2,nMembers)   ! Array with the fibers initial unity vectors as columns
        ! - - - - - - - - - -
        REAL(8) :: lambdas(nMembers) ! array with the stretch of each fiber of the RVE
        ! - - - - - - - - - -
        INTEGER :: i ! counter
        REAL(8) :: member(2) ! member deformed vector
        ! - - - - - - - - - -

        ! - - - - - - - - - -
        DO i=1,nMembers
            member(1) = FF(1,1)*members0(i,1) + FF(1,2)*members0(i,2)
            member(2) = FF(2,1)*members0(i,1) + FF(2,2)*members0(i,2)
            lambdas(i) = DSQRT(SUM(member*member))
        END DO
        ! - - - - - - - - - -

        ! - - - - - - - - - -
    END FUNCTION RVE_Deformation
    ! = = = = = = = = = =


    ! = = = = = = = = = =
    ! ROUTINE:
    ! TODO_function_or_subroutine_name
    !
    ! DESCRIPTION:
    ! TODO_describe_routine_purpose.
    ! = = = = = = = = = =
    FUNCTION RVE_Respuesta(FF, nBundlesRVE, bundles0, CC_n, CC_l, CC_t, volumeFraction, lr0) RESULT(fpkStress)
        ! - - - - - - - - - -
        USE CLASS_Curve, ONLY : Curva
        USE MOD_Auxiliar, ONLY : OuterProduct_2x2, ComputeFromCurve
        IMPLICIT NONE
        ! - - - - - - - - - -
        INTEGER, PARAMETER      :: nDim = 2
        ! - - - - - - - - - -
        REAL(8), INTENT(IN)     :: FF(nDim,nDim)                ! Deformation gradient
        INTEGER, INTENT(IN)     :: nBundlesRVE                  ! Number of members that compose the RVE
        REAL(8), INTENT(IN)     :: bundles0(nDim,nBundlesRVE)   ! Array with the initial unity vectors of the fibers
        INTEGER, INTENT(IN)     :: CC_n                         ! numero de puntos de la curva constitutiva del haz
        REAL(8), INTENT(IN)     :: CC_l(CC_n), CC_t(CC_n)       ! arrays de la curva constitutiva del haz (lambdas y tensiones)
        REAL(8), INTENT(IN)     :: volumeFraction               ! Volume fraction of the macroscopic material
        REAL(8), INTENT(IN)     :: lr0                          ! valor medio de la distribucion de reclutamiento
        ! - - - - - - - - - -
        REAL(8)                 :: fpkStress(nDim,nDim)                ! First Piola Kirchhoff Stress Tensor (average of the fibers)
        ! - - - - - - - - - -
        REAL(8) :: bundle(nDim) ! member deformed vector
        REAL(8) :: b0xb0(nDim,nDim) ! outer product of a bundle direction with itself
        REAL(8) :: lambda
        INTEGER :: i ! counter
        REAL(8) :: bundleStress
        ! - - - - - - - - - -

        ! - - - - - - - - - -
        ! RVE Stress as a sum over the RVE members
        fpkStress = 0.0d0
        DO i=1,nBundlesRVE
            ! Affine deformation of each member
            bundle(1) = FF(1,1)*bundles0(1,i) + FF(1,2)*bundles0(2,i)
            bundle(2) = FF(2,1)*bundles0(1,i) + FF(2,2)*bundles0(2,i)
            lambda = DSQRT(SUM(bundle*bundle))
            ! Stress excerted by the member
            bundleStress = ComputeFromCurve(lambda, CC_n, CC_l, CC_t)
            ! Sum bundle stress to the RVE stress
            b0xb0 = OuterProduct_2x2(bundles0(1,i), bundles0(1,i))
            fpkStress = fpkStress + bundleStress/lambda * b0xb0
        END DO
        ! Now complete the computation of RVE stress (products common to all members)
!        WRITE(*,*) fpkStress
        fpkStress = ( (volumeFraction/lr0) / nBundlesRVE ) * MATMUL(FF,fpkStress)
!        WRITE(*,*) fpkStress
        ! - - - - - - - - - -

        ! - - - - - - - - - -
    END FUNCTION RVE_Respuesta
    ! = = = = = = = = = =


    ! = = = = = = = = = =
    SUBROUTINE RVE_Curva_Simple &
        (nPoints, lambdaArray, &
        nParam1, Param1, &
        nParam2, Param2, &
        fpkStress_array)
        ! - - - - - - - - - -
        USE CLASS_Curve, ONLY : Curva
        USE MOD_Bundle, ONLY : Get_BundlesVectors, Get_CurvaConstitutivaHaz
        ! - - - - - - - - - -
        IMPLICIT NONE
        ! - - - - - - - - - -
        INTEGER, PARAMETER :: nDim = 2
        ! - - - - - - - - - -
        INTEGER, INTENT(IN) :: nPoints                      ! Number of points in the curve
        REAL(8), INTENT(IN) :: lambdaArray(nPoints)         ! Array with the lambda values for the nPoints
        INTEGER, INTENT(IN) :: nParam1
        REAL(8), INTENT(IN) :: Param1(nParam1)
        INTEGER, INTENT(IN) :: nParam2
        REAL(8), INTENT(IN) :: Param2(nParam2)
        ! - - - - - - - - - -
        REAL(8), INTENT(OUT) :: fpkStress_array(nDim,nDim,nPoints)
        ! - - - - - - - - - -
        ! Parametros RVE
        INTEGER :: nBundlesRVE
        REAL(8) :: thetaRVE
        REAL(8), ALLOCATABLE :: bundles_v0(:,:)
        REAL(8) :: volFraction
        ! Parametros haz - elasticos
        REAL(8) :: Et, Eb
        ! Parametros haz - distribucion
        REAL(8) :: lamr0, lamrS, lamr_min, lamr_max
        ! Parametros haz - discretizacion curva constitutiva
        INTEGER :: CC_nPuntos ! numero de puntos con los que se discretiza la curva constitutiva
        INTEGER :: CC_precision ! numero de puntos de la curva de distribucion con los que se computa la curva constitutiva
        REAL(8) :: CC_lam_min, CC_lam_max
        ! Curva constitutiva
        REAL(8), ALLOCATABLE :: CC_lam(:), CC_ten(:)
        ! Auxiliares
        INTEGER :: last, i
        REAL(8) :: F11
        REAL(8) :: FF(nDim,nDim)
        ! - - - - - - - - - -

        ! - - - - - - - - - -
        ! Parametros
        nBundlesRVE = NINT(Param1(1))
        ALLOCATE( bundles_v0(nDim, nBundlesRVE) )
        !bundles_v0 = RESHAPE( Param1(2:1+nDim*nBundlesRVE), SHAPE(bundles_v0) )
        !last = 1 + nDim*nBundlesRVE
        thetaRVE = Param1(2)
        CALL Get_BundlesVectors(nBundlesRVE, thetaRVE, bundles_v0)
        last = 2
        !
        CC_nPuntos = Param1(last+1)
        CC_lam_min = Param1(last+2)
        CC_lam_max = Param1(last+3)
        CC_precision = Param1(last+4)
        !
        volFraction = Param2(1)
        Et = Param2(2)
        Eb = Param2(3)
        lamr0 = Param2(4)
        lamrS = Param2(5)
        lamr_min = Param2(6)
        lamr_max = Param2(7)
        ! Hago una correccion: la fraccion de volumen la divido por lamr0 para no modificar la funcion RVE_Respuesta
        volFraction = volFraction

        ! - - - - - - - - - -
        ! Calcular curva constitutiva de un haz
        CALL Get_CurvaConstitutivaHaz(CC_nPuntos, CC_lam_min, CC_lam_max, &
        Et, Eb, lamr0, lamrS, lamr_min, lamr_max, &
        CC_precision, &
        CC_lam, CC_ten)
        ! - - - - - - - - - -
        ! Computing
!        WRITE(*,*) 'Computando curva en RVE_Curva_Simple()'
!        WRITE(*,*) 'Numero de puntos: ', nPoints
        DO i=1,nPoints
!            WRITE(*,'(I4)',ADVANCE='NO') i
            FF = RESHAPE([lambdaArray(i), 0.0d0, 0.0d0, 1.0d0], SHAPE(FF))
            fpkStress_array(:,:,i) = RVE_Respuesta(FF, nBundlesRVE, bundles_v0, CC_nPuntos, CC_lam, CC_ten, volFraction, lamr0)
        END DO
!        WRITE(*,*)
        ! - - - - - - - - - -

        ! - - - - - - - - - -
    END SUBROUTINE RVE_Curva_Simple
    ! = = = = = = = = = =

    ! = = = = = = = = = =
    SUBROUTINE RVE_Curva_Biaxial &
        (nPoints, lambdaArray, &
        nParam1, Param1, &
        nParam2, Param2, &
        fpkStress_array)
        ! - - - - - - - - - -
        USE CLASS_Curve, ONLY : Curva
        USE MOD_Bundle, ONLY : Get_CurvaConstitutivaHaz
        ! - - - - - - - - - -
        IMPLICIT NONE
        ! - - - - - - - - - -
        INTEGER, PARAMETER :: nDim = 2
        ! - - - - - - - - - -
        INTEGER, INTENT(IN) :: nPoints                      ! Number of points in the curve
        REAL(8), INTENT(IN) :: lambdaArray(nPoints)         ! Array with the lambda values for the nPoints
        INTEGER, INTENT(IN) :: nParam1
        REAL(8), INTENT(IN) :: Param1(nParam1)
        INTEGER, INTENT(IN) :: nParam2
        REAL(8), INTENT(IN) :: Param2(nParam2)
        ! - - - - - - - - - -
        REAL(8), INTENT(OUT) :: fpkStress_array(nDim,nDim,nPoints)
        ! - - - - - - - - - -
        ! Parametros RVE
        INTEGER :: nBundlesRVE
        REAL(8), ALLOCATABLE :: bundles_v0(:,:)
        REAL(8) :: volFraction
        ! Parametros haz - elasticos
        REAL(8) :: Et, Eb
        ! Parametros haz - distribucion
        REAL(8) :: lamr0, lamrS, lamr_min, lamr_max
        ! Parametros haz - discretizacion curva constitutiva
        INTEGER :: CC_nPuntos, CC_precision
        REAL(8) :: CC_lam_min, CC_lam_max
        ! Curva constitutiva
        REAL(8), ALLOCATABLE :: CC_lam(:), CC_ten(:)
        ! Auxiliares
        INTEGER :: last, i
        REAL(8) :: F11
        REAL(8) :: FF(nDim,nDim)
        ! - - - - - - - - - -

        ! - - - - - - - - - -
        ! Parametros
        nBundlesRVE = NINT(Param1(1))
        ALLOCATE( bundles_v0(nDim, nBundlesRVE) )
        bundles_v0 = RESHAPE( Param1(2:1+nDim*nBundlesRVE), SHAPE(bundles_v0) )
        last = 1 + nDim*nBundlesRVE
        !
        CC_nPuntos = Param1(last+1)
        CC_lam_min = Param1(last+2)
        CC_lam_max = Param1(last+3)
        CC_precision = Param1(last+4)
        !
        volFraction = Param2(1)
        Et = Param2(2)
        Eb = Param2(3)
        lamr0 = Param2(4)
        lamrS = Param2(5)
        lamr_min = Param2(6)
        lamr_max = Param2(7)
        ! - - - - - - - - - -
        ! Calcular curva constitutiva de un haz
        CALL Get_CurvaConstitutivaHaz(CC_nPuntos, CC_lam_min, CC_lam_max, &
        Et, Eb, lamr0, lamrS, lamr_min, lamr_max, &
        CC_precision, &
        CC_lam, CC_ten)
        ! - - - - - - - - - -
        ! Computing
!        WRITE(*,*) 'Computando curva en RVE_Curva_Biaxial()'
!        WRITE(*,*) 'Numero de puntos: ', nPoints
        DO i=1,nPoints
!            WRITE(*,'(I4)',ADVANCE='NO') i
            FF = RESHAPE([lambdaArray(i), 0.0d0, 0.0d0, lambdaArray(i)], SHAPE(FF))
            fpkStress_array(:,:,i) = RVE_Respuesta(FF, nBundlesRVE, bundles_v0, CC_nPuntos, CC_lam, CC_ten, volFraction, lamr0)
        END DO
!        WRITE(*,*)
        ! - - - - - - - - - -

        ! - - - - - - - - - -
    END SUBROUTINE RVE_Curva_Biaxial
    ! = = = = = = = = = =

END MODULE MOD_RVE
! = = = = = = = = = =
