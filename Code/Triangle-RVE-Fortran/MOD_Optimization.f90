! = = = = = = = = = =
MODULE MOD_Optimization

    ! - - - - - - - - - -
    IMPLICIT NONE
    PRIVATE ! NOTE_avoid_public_variables_if_possible
    ! - - - - - - - - - -
    PUBLIC :: BruteForceOptimization
    ! - - - - - - - - - -

! = = = = = = = = = =
CONTAINS
! = = = = = = = = = =

    ! = = = = = = = = = =
    SUBROUTINE BruteForceOptimization &
        (FITFUN, nExpData, expData_x, expData_y, &
        nFixPar, FIXPAR, nFitPar, FITPAR, FITDEL, FITMASK, &
        maxiter, Tolerancia, &
        verbose, logfileID, &
        FitParUpd, Residuo)
        ! - - - - - - - - - -
        ! DESCRIPTION:
        ! Optimization of parameters to minimize sum of squared deviations
        ! between FITFUN and experimentalData
        ! - - - - - - - - - -
        USE MOD_Auxiliar, ONLY : DeltaArray, Compute_SSR
        ! - - - - - - - - - -
        IMPLICIT NONE
        ! - - - - - - - - - -
        EXTERNAL :: FITFUN
        INTERFACE
            SUBROUTINE FITFUN(nPoints, xValues, nFixParam, FixParam, nFitParam, FitParam, yValues)
                INTEGER, INTENT(IN)     :: nPoints
                REAL(8), INTENT(IN)     :: xValues(nPoints)
                INTEGER, INTENT(IN)     :: nFixParam
                REAL(8), INTENT(IN)     :: FixParam(nFixParam)
                INTEGER, INTENT(IN)     :: nFitParam
                REAL(8), INTENT(IN)     :: FitParam(nFitParam)
                REAL(8), INTENT(OUT)    :: yValues(2,2,nPoints)
            END SUBROUTINE
        END INTERFACE
        INTEGER, INTENT(IN) :: nExpData
        REAL(8), INTENT(IN) :: expData_x(nExpData)
        REAL(8), INTENT(IN) :: expData_y(nExpData)
        INTEGER, INTENT(IN) :: nFixPar
        REAL(8), INTENT(IN) :: FIXPAR(nFixPar)
        INTEGER, INTENT(IN) :: nFitPar
        REAL(8), INTENT(IN) :: FITPAR(nFitPar)
        REAL(8), INTENT(IN) :: FITDEL(nFitPar)
        LOGICAL, INTENT(IN) :: FITMASK(nFitPar)
        INTEGER, INTENT(IN) :: maxiter
        REAL(8), INTENT(IN) :: Tolerancia
        LOGICAL, INTENT(IN) :: verbose
        INTEGER, INTENT(IN) :: logfileID
        ! - - - - - - - - - -
        REAL(8), INTENT(OUT) :: FitParUpd(nFitPar) ! Fitting parameters updated
        REAL(8), INTENT(OUT) :: Residuo
        ! - - - - - - - - - -
        REAL(8) :: MEA ! Mean of experimental array
        REAL(8) :: TSS ! Total sum of squares
        REAL(8) :: SSR ! Sum of squared deviations
        REAL(8) :: SSR_plus
        REAL(8) :: SSR_minus
        INTEGER :: i, j
        REAL(8) :: simData_y_full(2,2,nExpData)
        REAL(8) :: simData_y(nExpData)
        REAL(8) :: FitParVar(nFitPar) ! Fitting parameters variated
        INTEGER :: min_index
        INTEGER :: FitParChange(nFitPar) ! Change in fitting parameters per iteration
        LOGICAL :: FitSuccess    ! Dictates if optimization was succesfull
        ! - - - - - - - - - -

        ! - - - - - - - - - -
        FitSuccess = .FALSE.
        FitParUpd = FITPAR
        ! - - - - - - - - - -
        MEA = SUM(expData_y) / nExpData
        TSS = SUM((expData_y - MEA)*(expData_y - MEA))
        ! Centered value of SSR
        CALL FITFUN(nExpData, expData_x, nFixPar, FIXPAR, nFitPar, FitParUpd, simData_y_full)
        simData_y = simData_y_full(1,1,:)
        SSR = Compute_SSR(expData_y, simData_y, nExpData)
        Residuo = SSR/TSS
        WRITE(*,'(A30)') 'Starting with parameters: '
        WRITE(*,*) FitParUpd
        WRITE(*,*) 'Residuo: ', Residuo
        ! - - - - - - - - - -
        DO i=1,maxiter
            ! Check if tolerance is met
            IF ( Residuo < Tolerancia ) THEN
                WRITE(*,*) 'Achieved Residuo tolerance'
                FitSuccess = .TRUE.
                EXIT
            END IF
            DO j=1,nFitPar
                WRITE(*,'(A14, I4, A3)', ADVANCE='NO') 'Parameter: ', j, '-> '
                ! Check if the parameter is to be optimized
                IF ( .NOT. FITMASK(j) ) THEN
                    WRITE(*,'(A15)') '-----------.'
                    FitParChange(j) = 0
                    CYCLE
                END IF
                ! Plus value of SSR
                FitParVar = FitParUpd + FITDEL(j)*DeltaArray(nFitPar,j)
                CALL FITFUN(nExpData, expData_x, nFixPar, FIXPAR, nFitPar, FitParVar, simData_y_full)
                simData_y = simData_y_full(1,1,:)
                SSR_plus = Compute_SSR(expData_y, simData_y, nExpData)
                ! Minus value of SSR
                FitParVar = FitParUpd - FITDEL(j)*DeltaArray(nFitPar,j)
                CALL FITFUN(nExpData, expData_x, nFixPar, FIXPAR, nFitPar, FitParVar, simData_y_full)
                simData_y = simData_y_full(1,1,:)
                SSR_minus = Compute_SSR(expData_y, simData_y, nExpData)
                ! Move along the direction which SSR is minimum
                min_index = MINLOC( [SSR, SSR_plus, SSR_minus], DIM=1 )
                SELECT CASE (min_index)
                CASE (1)
                    WRITE(*,'(A15)') 'Unchanged.'
                    FitParChange(j) = 0
                CASE (2)
                    WRITE(*,'(A15)') 'Incremented.'
                    FitParChange(j) = 1
                CASE (3)
                    WRITE(*,'(A15)') 'Decremented.'
                    FitParChange(j) = -1
                CASE DEFAULT
                    WRITE(*,'(A50)') 'Error in searching of a minimum for parameter.'
                    STOP
                END SELECT
            END DO
            ! Updated fitting parameters
            FitParUpd = FitParUpd + FitParChange*FITDEL
            ! Centered value of SSR
            CALL FITFUN(nExpData, expData_x, nFixPar, FIXPAR, nFitPar, FitParUpd, simData_y_full)
            simData_y = simData_y_full(1,1,:)
            SSR = Compute_SSR(expData_y, simData_y, nExpData)
            Residuo = SSR/TSS
            ! Check if there is some change
            WRITE(*,'(A12, I7, A30)') 'Iteration ', i, 'complete with parameters:'
            WRITE(*,*) FitParUpd
            WRITE(*,*) 'Residuo: ', Residuo
            IF ( ALL(FitParChange == 0) ) THEN
                WRITE(*,*) 'No change in parameters, local minimum found.'
                FitSuccess = .TRUE.
                EXIT
            END IF
        END DO
        ! - - - - - - - - - -
        WRITE(*,*) 'Optimization ended with succes status: ', FitSuccess
        ! - - - - - - - - - -

        ! - - - - - - - - - -
    END SUBROUTINE BruteForceOptimization
    ! = = = = = = = = = =


    ! = = = = = = = = = =
    SUBROUTINE ConjugateGradientOptimization &
        (FITFUN, nExpData, expData_x, expData_y, &
        nFixPar, FIXPAR, nFitPar, FITPAR, FITDEL, FITMASK, &
        maxiter, R2tolerance, &
        verbose, logfileID, &
        FitParUpd, R2)
        ! - - - - - - - - - -
        ! DESCRIPTION:
        ! Optimization of parameters to minimize sum of squared deviations
        ! between FITFUN and experimentalData
        ! - - - - - - - - - -
        USE MOD_Auxiliar, ONLY : DeltaArray, VxV_rR, TxV_rV, VxTxV_rR, Compute_SSR
        ! - - - - - - - - - -
        IMPLICIT NONE
        ! - - - - - - - - - -
        EXTERNAL :: FITFUN
        INTERFACE
            FUNCTION FITFUN(nPoints, xValues, nFixParam, FixParam, nFitParam, FitParam) RESULT(yValues)
                INTEGER, INTENT(IN)     :: nPoints
                REAL(8), INTENT(IN)     :: xValues(nPoints)
                INTEGER, INTENT(IN)     :: nFixParam
                REAL(8), INTENT(IN)     :: FixParam(nFixParam)
                INTEGER, INTENT(IN)     :: nFitParam
                REAL(8), INTENT(IN)     :: FitParam(nFitParam)
                REAL(8)                 :: yValues(nPoints)
            END FUNCTION
        END INTERFACE
        INTEGER, INTENT(IN) :: nExpData
        REAL(8), INTENT(IN) :: expData_x(nExpData)
        REAL(8), INTENT(IN) :: expData_y(nExpData)
        INTEGER, INTENT(IN) :: nFixPar
        REAL(8), INTENT(IN) :: FIXPAR(nFixPar)
        INTEGER, INTENT(IN) :: nFitPar
        REAL(8), INTENT(IN) :: FITPAR(nFitPar)
        REAL(8), INTENT(IN) :: FITDEL(nFitPar)
        LOGICAL, INTENT(IN) :: FITMASK(nFitPar)
        INTEGER, INTENT(IN) :: maxiter
        REAL(8), INTENT(IN) :: R2tolerance
        LOGICAL, INTENT(IN) :: verbose
        INTEGER, INTENT(IN) :: logfileID
        ! - - - - - - - - - -
        REAL(8), INTENT(OUT) :: FitParUpd(nFitPar) ! Fitting parameters updated
        REAL(8), INTENT(OUT) :: R2
        ! - - - - - - - - - -
        REAL(8) :: MEA ! Mean of experimental array
        REAL(8) :: TSS ! Total sum of squares
        REAL(8) :: SSR ! Sum of squared deviations
        REAL(8) :: SSR_p(nFitPar)
        REAL(8) :: SSR_m(nFitPar)
        REAL(8) :: SSR_pp
        REAL(8) :: SSR_mm
        INTEGER :: i, j, k
        REAL(8) :: simData_y(nExpData)
        REAL(8) :: FitParVar(nFitPar) ! Fitting parameters variated
        INTEGER :: min_index
        INTEGER :: FitParChange(nFitPar) ! Change in fitting parameters per iteration
        LOGICAL :: FitSuccess    ! Dictates if optimization was succesfull
        ! - - - - - - - - - -
        REAL(8) :: Jacobian(nFitPar)
        REAL(8) :: Hessian(nFitPar, nFitPar)
        REAL(8) :: alpha
        REAL(8) :: aux1, aux2
        ! - - - - - - - - - -

        ! - - - - - - - - - -
        FitSuccess = .FALSE.
        FitParUpd = FITPAR
        ! - - - - - - - - - -
        MEA = SUM(expData_y) / nExpData
        TSS = SUM((expData_y - MEA)*(expData_y - MEA))
        ! Centered value of SSR
        simData_y = FITFUN(nExpData, expData_x, nFixPar, FIXPAR, nFitPar, FitParUpd)
        SSR = Compute_SSR(expData_y, simData_y, nExpData)
        R2 = 1 - SSR/TSS
        WRITE(*,'(A30)') 'Starting with parameters: '
        WRITE(*,*) FitParUpd
        WRITE(*,*) 'R2: ', R2
        ! - - - - - - - - - -
        DO i=1,maxiter
            ! Check if tolerance is met
            IF ( R2 > R2tolerance ) THEN
                WRITE(*,*) 'Achieved R2 tolerance'
                FitSuccess = .TRUE.
                EXIT
            END IF

            DO j=1,nFitPar
                ! Plus value of SSR
                FitParVar = FitParUpd + FITDEL(j)*DeltaArray(nFitPar,j)
                simData_y = FITFUN(nExpData, expData_x, nFixPar, FIXPAR, nFitPar, FitParVar)
                SSR_p(j) = Compute_SSR(expData_y, simData_y, nExpData)
                ! Minus value of SSR
                FitParVar = FitParUpd - FITDEL(j)*DeltaArray(nFitPar,j)
                simData_y = FITFUN(nExpData, expData_x, nFixPar, FIXPAR, nFitPar, FitParVar)
                SSR_m(j) = Compute_SSR(expData_y, simData_y, nExpData)
            END DO

            DO j=1,nFitPar
                ! Jacobian values
                Jacobian(j) = (SSR_p(j) - SSR_m(j)) / ( 2.0d0 * FITDEL(j) )
                ! Hessian diagonal values
                Hessian(j,j) = ( SSR_p(j) - 2*SSR + SSR_m(j) ) / ( FITDEL(j)**2 )
                DO k=j+1,nFitPar
                    ! Plus value of SSR
                    FitParVar = FitParUpd + FITDEL(j)*DeltaArray(nFitPar,j) + FITDEL(k)*DeltaArray(nFitPar,k)
                    simData_y = FITFUN(nExpData, expData_x, nFixPar, FIXPAR, nFitPar, FitParVar)
                    SSR_pp = Compute_SSR(expData_y, simData_y, nExpData)
                    ! Minus value of SSR
                    FitParVar = FitParUpd - FITDEL(j)*DeltaArray(nFitPar,j) - FITDEL(k)*DeltaArray(nFitPar,k)
                    simData_y = FITFUN(nExpData, expData_x, nFixPar, FIXPAR, nFitPar, FitParVar)
                    SSR_mm = Compute_SSR(expData_y, simData_y, nExpData)
                    ! Hessian non-diagonal values
                    Hessian(j,k) = ( SSR_pp - SSR_p(j) - SSR_p(k) + 2*SSR - SSR_m(j) - SSR_m(k) + SSR_mm ) / ( 2.0d0*FITDEL(j)*FITDEL(k) )
                    Hessian(k,j) = Hessian(j,k)
                END DO
            END DO


            aux1 = VxV_rR(Jacobian,Jacobian,nFitPar)
            aux2 = VxTxV_rR(Jacobian, Hessian, Jacobian, nFitPar)
            alpha = - aux1 / aux2

            FitParUpd = FitParUpd - alpha * Jacobian
            ! Centered value of SSR
            simData_y = FITFUN(nExpData, expData_x, nFixPar, FIXPAR, nFitPar, FitParUpd)
            SSR = Compute_SSR(expData_y, simData_y, nExpData)
            R2 = 1 - SSR/TSS
            ! Check if there is some change
            WRITE(*,'(A12, I4, A30)') 'Iteration ', i, 'complete with parameters:'
            WRITE(*,*) FitParUpd
            WRITE(*,*) 'R2: ', R2

        END DO
        ! - - - - - - - - - -
        WRITE(*,*) 'Optimization ended with succes status: ', FitSuccess
        ! - - - - - - - - - -

        ! - - - - - - - - - -
    END SUBROUTINE ConjugateGradientOptimization
    ! = = = = = = = = = =

END MODULE MOD_Optimization
! = = = = = = = = = =
