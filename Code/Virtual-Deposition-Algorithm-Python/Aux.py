""" modulo de funciones auxiliares """

import numpy as np
from scipy import stats

def find_string_in_file(fid, target, mandatory=False):
    fid.seek(0) # rewind
    target = target.lower()
    len_target = len(target)
    for linea in fid:
        if target == linea[:len_target].lower():
            ierr = 0
            break
    else:
        if mandatory:
            raise EOFError("final de archivo sin encontrar target string")
        ierr = 1
    return ierr

def iguales(x, y, tol=1.0e-8):
    """ returns True if x is equal to y, both floats """
    return np.abs(x-y)<tol

def calcular_longitud_de_segmento(r0, r1):
    """ dados dos nodos de un segmento
    r0: coordenadas "xy" del nodo 0
    r1: coordenadas "xy" del nodo 1
    calcula el largo del segmento """
    dx = r1[0] - r0[0]
    dy = r1[1] - r0[1]
    return np.sqrt(dx*dx + dy*dy)

def calcular_angulo_de_segmento(r0, r1):
    """ dados dos nodos de un segmento
    r0: coordenadas "xy" del nodo 0
    r1: coordenadas "xy" del nodo 1
    calcula el angulo que forman con el eje horizontal """
    dx = r1[0] - r0[0]
    dy = r1[1] - r0[1]
    if iguales(dx,0.): # segmento vertical
        if iguales(dy,0.): # segmento nulo
            raise ValueError("Error, segmento de longitud nula!!")
        elif dy > 0.: # segmento hacia arriba
            return 0.5*np.pi
        else: # segmento hacia abajo
            return -0.5*np.pi
    elif iguales(dy,0.): # segmento horizontal
        if dx>0.: # hacia derecha
            return 0.
        else: # hacia izquierda
            return np.pi
    else: # segmento oblicuo
        if dx<0.: # segundo o tercer cuadrante
            return np.pi + np.arctan(dy/dx)
        elif dy>0.: # primer cuadrante
            return np.arctan(dy/dx)
        else: # cuarto cuadrante
            return 2.*np.pi + np.arctan(dy/dx)

def compute_from_curve(x, xx, yy, extrapolar=False):
    # primero chequeo si x cae fuera de intervalo
    if x<xx[0] or x>xx[-1]:
        if extrapolar:
            if x<xx[0]:
                return yy[0]
            else:
                return yy[-1]
    # de lo contrario tengo que calcularlo dentro de intervalo interpolando linealmente
    nx = len(xx)
    for i in range(1,nx):
        if x<xx[i]:
            slope = ( yy[i] - yy[i-1] ) / ( xx[i] - xx[i-1] )
            return yy[i-1] + slope * (x - xx[i-1])

def compute_discrete_normal_distribution(mu=1.0, sigma=1.0, n=1001):
    from scipy.stats import norm
    x = np.linspace(mu-10.*sigma, mu+10.*sigma, num=n)
    y = norm.pdf(x)
    Y = norm.cdf(x)
    return x, y, Y

class Discrete_normal_distribution(object):
    def __init__(self, mu=0., sigma=1., n=1001):
        # armo curva discreta
        self.x = np.linspace(mu - 10.*sigma, mu+10.*sigma, n)
        self.y = stats.norm.pdf(self.x, mu, sigma)
        self.Y = stats.norm.cdf(self.x, mu, sigma)

    def get_sample(self):
        r = np.random.random()
        x = compute_from_curve(r, self.Y, self.x, True)
        return x
