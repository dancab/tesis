Como se discutió previamente, una gran ventaja del modelado multiescala es la posibilidad de vincular propiedades microscópicas con la respuesta mecánica macroscópica, y por ende brindar una herramienta para el diseño de microestructuras de materiales biomiméticos.
Como ejemplo de esto, el modelo propuesto en este capítulo se utilizó para determinar las propiedades microestructurales de un injerto vascular electrohilado capaz de imitar la respuesta en presión-diámetro para una arteria intracranial humana en el rango de presiones fisiológicas (\num{60}-\SI{140}{\mmHg}), cuyos datos experimentales se obtienen de la literatura [Hayashi1980].

Entonces, para poner a prueba la capacidad del modelo como herramienta de diseño, se optimizaron los parámetros en el modelo de membrana (figura \ref{fig:tubo_clip}), tomando un diámetro interno en reposo de \SI{2.5}{\milli\meter} y un espesor de \SI{0.04}{\milli\meter}.
Los parámetros elegidos para ser optimizados fueron los módulos de elasticidad de las fibras ($\Et$ y $\Eb$) y su distribución de reclutamiento ($\lamero$ y $\lamers$), mientras que la fracción de volumen se mantuvo en \num{0.3} y los límites de la FDR quedaron inalterados en \num{1.0} y \num{1.4}, respectivamente.
Cabe notar que los parámetros optimizados corresponden, en la práctica, a la selección de un material de base para las nanofibras que cumpla con la respuesta elástica pedida y la determinación de la morfología de la malla electrohilada para que cumpla con la FDR dada por los valores $\lamero$ y $\lamers$ optimizados.

La figura \ref{fig:arteria_ajuste} muestra las curvas de presión-diámetro para la arteria intracranial humana y para el injerto optimizado para biomímesis, mientras que los parámetros obtenidos se listan en la tabla \ref{tab:parametros_arteria}.
La curva simulada muestra un buen acuerdo en la región de presión fisiológica, a la vez que se mantiene similar en comportamiento para bajas presiones (\num{0}-\SI{60}{\mmHg}).
Este injerto necesitaría estar constituido por nanofibras menos rígidas, una propiedad que puede controlarse mediante la selección de un polímero diferente para el electrohilado.
Entre los valores de $\Et$ hallados en la literatura para nanofibras aisladas, se encuentra que para PCL (poli-$\varepsilon$-caprolactona) un diámetro aproximado de \SI{0.5}{\nano\meter}, el módulo de Young reportado de \SI{0.3}{\giga\Pa} se corresponde adecuadamente con el obtenido aquí mediante optimización \cite{Wong2008}.
También podrían probarse otros materiales conocidos que son menos rígidos que el PLLA, como por ejemplo poliuretano segmentado de grado médico, seda, poliglicerol sebacato (PGS), colágeno, entre otros.
Otra estrategia podría ser la combinación de diferentes materiales, regulando la relación de cada polímero para obtener la propiedad deseada.
Adicionalmente, la distribución de reclutamiento obtenida implica la necesidad de fibras tanto con un mayor valor medio de tortuosidad así como una mayor dispersión de la misma, para poder imitar, mediante el proceso de reclutamiento progresivo, la débil compliancia de los materiales biológicos a bajas presiones junto con una rigidez similar a presiones fisiológicas.
Vale mencionar que, de todas maneras, fuera del rango fisiológico no se hace necesaria la biomímesis.

\begin{figure}[ht]
	\centering
	\captionof{table}[Parámetros optimizados para biomímesis de una arteria intracranial humana.]{Parámetros del modelo optimizados para un injerto electrohilado capaz de imitar la respuesta mecánica de una arteria intracranial humana.}
	\label{tab:parametros_arteria}
	{
		\small
		\begin{tabular}{L L L L L L L}
			\hline
			\rowcolor{Gray}
			$\fv$ & $\Et {\scriptsize(\si{\giga\pascal})}$ & $\Eb {\scriptsize(\si{\mega\pascal})}$ & $\lamero$ & $\lamers$ & $\lamermin$ & $\lamermax$ \\
			\hline
			0.3   & 0.303       & 6.824       & 1.3338 & 0.0710 & 1.00     &  1.40    \\
			\hline
		\end{tabular}
	}
	{
	\newcommand{\EtGPa}{ \Et {\scriptstyle(\si{\giga\pascal})} }
	\newcommand{\EbMPa}{\Eb {\scriptstyle(\si{\mega\pascal})} }
	\newcommand{\SEmmHg}{ SE {\scriptstyle(\si{\mmHg})} }
	\small
	\begin{tabular}{L L}
		\hline
		$\fv$		&	0.3 \\
		$\EtGPa$	&	0.303 \\ 
		$\EbMPa$	&	6.824 \\
		$\lamero$	&	1.3338 \\
		$\lamers$	&	0.0710 \\
		$\lamermin$	&	1.0 \\
		$\lamermax$ &	1.4 \\
		\hline
		$\SEmmHg$ &	1.92\\
		\hline
	\end{tabular}
	}	
	\includegraphics[width=0.7\linewidth]{../Imagenes/02-Modelo/arteria_ajuste_spa.pdf}
	\captionof{figure}[Curvas presión-diámetro de una arteria intracranial humana y un injerto biomimético.]{Respuesta en presión-diámetro para una arteria intracranial humana y para un injerto simulado biomimético capaz de reproducir la respuesta mecánica en presión-diámetro en el rango fisiológico (indicado por la zona gris). El coeficiente de ajuste $R^2$ para el rango completo de presiones es de $0.89$, mientras que para el rango fisiológico mejora a $0.98$.}
	\label{fig:arteria_ajuste}
\end{figure}