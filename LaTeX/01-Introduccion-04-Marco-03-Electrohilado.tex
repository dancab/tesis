Como se mencionó en la sección previa, un requisito fundamental para un \textit{scaffold} de ingeniería de tejidos es que presente una microestructura altamente porosa e interconectada, de modo de permitir el transporte de sustancias así como la migración, adhesión y proliferación celular.
Consecuentemente, el número de publicaciones científicas relacionadas con andamios para aplicaciones biomédicas se incrementó notablemente en los últimos años, hecho que revela un alto y sostenido interés en el diseño y preparación de matrices porosas\cite{Hutmacher2004}. 
Teniendo en cuenta los requisitos indispensables que debe reunir una matriz extracelular artificial, la tecnología de procesamiento para su producción debe poseer un control importante de las propiedades macro y microestructurales. 
El comportamiento viscoso de los polímeros por encima de su temperatura de transición vítrea o temperatura de fusión y su solubilidad en diferentes solventes orgánicos, determina la aplicación de una amplia variedad de técnicas para preparar matrices poliméricas porosas a partir de polímeros sintéticos y naturales, en algunos casos se realizan compuestos con materiales cerámicos \cite{Karp2003,Ma2005,Thomson2000}. 
No existe una metodología única para crear matrices porosas, la elección de la técnica más apropiada resulta entonces crítica y depende de cada material polimérico y la aplicación específica \cite{Abraham2007}.

La microestructura, porosidad, y topografía del injerto poroso son factores fundamentales para su desempeño exitoso. La producción de injertos vasculares se puede abordar mediante la utilización de diferentes técnicas. P.M. Crapo y colaboradores fabricaron injertos vasculares de poli(ácido láctico-co-glicólico) (PLGA) y poli(glicerol sebacato) (PGS) mediante la técnica de evaporación de solvente y disolución de partículas \cite{Crapo2010,Gao2008}. 
El grupo de D.A. Vorp desarrolló injertos de poli(éster uretano)urea (PEUU) obtenidos mediante la técnica de separación de fases inducida por temperatura (TIPS) \cite{Nieponice2008}. 
Otros grupos utilizaron polímeros naturales, C.E. Ghezzi y colaboradores produjeron injertos densos de colágeno simplemente al envolver circunferencialmente láminas de gel de colágeno densas, comprimidas plásticamente, alrededor de un soporte cilíndrico \cite{Ghezzi2012}. 
S. Liu y colaboradores desarrollaron injertos bicapa reforzados de fibrina de seda con heparina \cite{Liu2013}.

Si bien se han logrado injertos con propiedades interesantes mediante las técnicas mencionadas, la tecnología de electrohilado resulta una técnica más prometedora para la producción de injertos vasculares. 
Esta es una técnica versátil, que permite la producción de fibras de nano/microescala que presentan un gran potencial para imitar el microambiente fibroso de la matriz extracelular natural presente en las arterias \cite{Hasan2014}. 
El electrohilado ofrece la posibilidad de ajustar finamente las propiedades mecánicas durante la fabricación, controlando la microarquitectura porosa, tamaño y la orientación de las fibras, logrando comportamientos anisotrópicos como los observados en los vasos sanguíneos.
Además, la posibilidad de usar un colector rotatorio de pequeño diámetro, resulta en la obtención de una tubuladura sin costura ideal para aplicaciones vasculares. 
Todas estas ventajas, en conjunto con las ya mencionadas en la sección previa, posicionan a la técnica de electrohilado como una técnica ideal para la producción de injertos vasculares de pequeño diámetro.

\subsubsection*{Proceso de electrohilado} \label{sec:cap1_electrohilado}
La tecnología de electrohilado constituye uno de los métodos de procesamiento de vanguardia que presenta mayores ventajas para la producción de nanofibras. 
La técnica tiene la habilidad única de producir nanofibras de diferentes materiales y geometrías, bajo costo, relativamente alta velocidad de producción y simplicidad en el diseño del equipamiento. 
En los últimos años, se han electrohilado numerosos tipos de materiales que incluyen prácticamente todos los polímeros sintéticos y naturales que sean solubles o puedan fundirse, y nanocompuestos, para obtener fibras continuas de unos pocos nanometros hasta algunos micrones que generan una matriz hilada no tejida altamente porosa \cite{Greiner2007}. 

Aunque la técnica de electrohilado constituye una vía versátil para la producción de nanofibras, el proceso es sumamente complejo y depende de numerosos parámetros. 
El diseño experimental básico para electrohilado de soluciones consta de cuatro componentes (figura \ref{fig:electrohilado_proceso}): un reservorio de solución o material fundido, una bomba de infusión que permite suministrar un flujo constante y controlado de solución, una fuente de alta tensión y un sistema colector sobre el que se deposita el material electrohilado. 
Al aplicar una tensión de \rango{5}{30}{kV}, la solución polimérica se electrifica fuertemente.
Se inducen cargas eléctricas que se distribuyen sobre la superficie de la gota de solución polimérica que pende de una boquilla. 
La gota experimenta un conjunto de fuerzas: fuerza de repulsión eléctrica entre las cargas inducidas, fuerza electrostática producto del campo eléctrico externo generado al aplicar la tensión, fuerza gravitatoria, fuerzas viscoelásticas que dependen del polímero y solvente, y la tensión superficial que se opone al estiramiento y afinamiento de la gota. 
Bajo la acción de estas interacciones, la gota se distorsiona en forma cónica, fenómeno conocido como cono de Taylor. 
En estas condiciones el balance de fuerzas llega a un equilibro.
Luego, cuando las fuerzas electrostáticas repulsivas superan la tensión superficial del polímero, se produce una situación inestable que provoca la expulsión de un microchorro líquido cargado desde la boquilla del capilar. 
Este microchorro electrizado sufre estiramiento y movimientos tipo látigo, dando lugar a la formación de hilos largos y delgados. 
A medida que el chorro líquido se deforma continuamente y se evapora el solvente (o solidifica el fundido), las cargas superficiales aumentan conduciendo a una disminución drástica del diámetro de las fibras. 
Los entrecruzamientos físicos de las cadenas poliméricas permiten dar continuidad al microchorro, formando fibras que se depositan en el sistema colector que, por otra parte, se encuentra conectado eléctricamente a tierra \cite{Andrady, Ramakrishna2006}.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.9\linewidth]{../Imagenes/01-Tejidos/electrohilado_proceso.png}
	\caption[Proceso de electrohilado]{Esquema básico de los componentes necesarios para el electrohilado de soluciones sobre un colector cilíndrico.}
	\label{fig:electrohilado_proceso}
\end{figure}

\subsubsection*{Microestructura} \label{sec:cap1_microestructura}
En el proceso de electrohilado el solvente se evapora casi completamente en la trayectoria que recorre el microchorro entre los electrodos durante el tiempo de proyección.
Las nanofibras así formadas se depositan de manera continua sobre el colector, ya sea plano o rotatorio, debido a la atracción eléctrica por el campo inducido.
La estructura resultante es la de una malla de nanofibras muy largas, onduladas, no tejidas, con uniones de tipo soldadura en los puntos de contacto donde ha quedado solvente residual.
Además, dado que las fibras se van superponiendo unas a otras, se obtiene una estructura de capas en la dirección normal al plano colector (dirección radial en el caso de un colector cilíndrico).
Esta morfología microscópica se asemeja a la de los tejidos arteriales (figura \ref{fig:comparacion_microestructura_electrohilado_arteria}), dando lugar a la idea de que las matrices electrohiladas son buenos candidatos para su utilización como andamios vasculares.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.7\linewidth]{../Imagenes/01-Tejidos/comparacion_microestructura_electrohilado_arteria}
	\caption[Comparación microestructural de una matriz electrohilada con la matriz extracelular arterial]{Comparación visual entre la microestructura de la matriz extracelular arterial y las matrices producidas mediante la técnica de electrohilado, incluyendo la morfología plana y la estructura de capas: (a) Imagen SEM de la capa superior de un andamio de PEUU electrohilado (adaptada de \cite{Carleton2015}), (b) Imagen SEM de la superficie de la capa adventicia descelularizada (adaptada de \cite{Williams2009a}), (c) Imagen SEM transversal de PEUU electrohilado mostrando la estructura de capas (adaptada de \cite{Carleton2015phdthesis}) y (d) Imagen SHG (\textit{second-harmonic generation}) transversal de las capas adventicia y media (de izquierda a derecha) mostrando su estructura de capas (adaptada de \cite{Niestrawska2016}).}
	\label{fig:comparacion_microestructura_electrohilado_arteria}
\end{figure}

Se puede lograr cierto nivel de control sobre la distribución de orientaciones de las nanofibras variando las velocidades angular y axial del mandril colector (figura \ref{fig:SEM_alineacion_vs_vel_rot}) \cite{Amoroso2011}, mientras que otros aspectos de la geometría de la malla pueden controlarse mediante otros parámetros del proceso como el caudal de infusión, la distancia entre la boquilla y el colector, la tensión aplicada, la introducción de terceras fases, etc.
De esta manera se obtienen mallas con nanofibras de distinto diámetro y curvatura, así como también se puede variar la densidad de uniones entre fibras y el grado de alineamiento sobre una dirección preferencial.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.9\linewidth]{../Imagenes/01-Tejidos/SEM_alineacion_vs_vel_rot}
	\caption[Alineación de las nanofibras según la velocidad de rotación del mandril]{Mediante la velocidad de rotación aplicada al mandril colector pueden obtenerse mallas con diferente grado de alineación a lo largo de la dirección circunferencial. (a) Malla isotrópica con $v\approx0$, (b) $v=\SI{0.3}{m/s}$, (c) $v=\SI{1.5}{m/s}$, (d) $v=\SI{4.5}{m/s}$, (e) $v=\SI{9.0}{m/s}$,(f) $v=\SI{13.8}{m/s}$. Imagen adaptada de \cite{Courtney2006}.}
	\label{fig:SEM_alineacion_vs_vel_rot}
\end{figure}

Es relevante señalar que se ha observado experimentalmente bajo carga, las grandes rotaciones y deformaciones sufridas por las fibras elastoméricas permiten a estos \textit{scaffolds} de microestructura nanofibrosa soportar el mismo nivel de deformaciones que experimentan los tejidos vasculares que pretenden reemplazar \cite{Butler2000,Fung1993,Sacks2000}.




