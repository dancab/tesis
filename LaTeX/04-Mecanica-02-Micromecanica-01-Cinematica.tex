Cada fibra de la malla, comprendida entre dos nodos, está determinada por una curva paramétrica en el dominio bidimensional de la capa a la que pertenece.
Además, puede darse una representación reducida de la fibra mediante su linea extremo-extremo y su valor de elongación de reclutamiento.
De esta forma, la posición y elongación efectiva de cada fibra queda determinada enteramente por las posiciones de sus nodos extremos.
Luego, el desplazamiento de la malla queda determinado únicamente por los desplazamientos de sus nodos.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.7\linewidth]{../Imagenes/04-Mecanica/malla_esquema.pdf}
	\caption[Esquema de una malla de nanofibras interconectadas]{Esquema de fibras electrohiladas interconectadas en puntos de unión entre dos fibras: a) Las fibras con su geometría curvada y b) Las fibras descriptas por la linea extremo-extremo (sólida) entre nodos, además de un valor de reclutamiento como la relación entre la longitud extremo-extremo y la longitud de contorno de la fibra curva (linea punteada).}
	\label{fig:malla_esquema}
\end{figure}

Resulta conveniente establecer numeraciones para el conjunto de nodos ($\setN$) y para el conjunto de fibras ($\setF$) de la malla, para lo cual se establece la siguiente notación:

\begin{align}
\setN &= \braces{i; i=1,...,N_n} \\
\setF &= \braces{i; i=1,...,N_f}
\end{align}

Cada nodo $n$ de la malla se desplaza desde su posición inicial $\Y_n$ hasta su posición actual $\y_n$, pudiendo definir así al vector desplazamiento nodal como:

\begin{equation}
\Umu_n = \y_n - \Y_n
\end{equation}
donde el sobreíndice $\mu$ se utiliza para indicar que se trata de desplazamientos a nivel de microescala, para diferenciarlos de los desplazamientos de la macroescala.

Mientras que el desplazamiento de la malla resulta una $N_n-tupla$ de vectores, pudiendo definirse como un elemento del siguiente espacio vectorial:

\begin{equation}
\setUmu = \braces{ \Umesh = \braces{ \Umu_n } ; \Umu_n \in \mathbb{R}^3, n=1,...,N_n }
\end{equation}

Dado que las fibras están definidas siempre entre dos nodos, puede identificarse cada fibra como una dupla ordenada de nodos:

\begin{equation}
\setF = \braces{i \mapsto \parent{i_1, i_2} , i_1\in\setN, i_2\in\setN, i=1,...,N_f}
\end{equation}
donde $i_1$ e $i_2$ son los nodos inicial y final, respectivamente, de la fibra $i$.

Además, cada fibra $i$ de la malla posee un diámetro $\diami$ y una longitud de contorno inicial $\locooi$ (longitud  de arco entre dos extremos).
Adicionalmente, determinados por las posiciones de sus nodos extremos puede definirse su longitud extremo-extremo inicial $\leteoi$ y su correspondiente versor orientación inicial $\aoi$ (figura \ref{fig:esquema_una_fibra}):

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.9\linewidth]{../Imagenes/04-Mecanica/Esquema_una_fibra}
	\caption[Esquema de una fibra]{En la micrografía se identifica una nanofibra entre dos puntos de unión (izq.) y se muestra un esquema aumentado de la misma con los parámetros que determinan su configuración inicial (der.): longitud de contorno en reposo $\locoo$, longitud extremo a extremo en reposo $\leteo$ y un versor orientación $\ao$. El diámetro $\diam$ puede ser medido a partir de una imagen con mayor magnificación.}
	\label{fig:esquema_una_fibra}
\end{figure}

\begin{equation}
	\leteoi = \norma{\Y_{i_2} - \Y_{i_1}}
\end{equation}

\begin{equation}
	\aoi = \frac{\Y_{i_2} - \Y_{i_1}}{\leteoi}
\end{equation}

La deformación elástica de la fibra $i$ está determinada por tres parámetros: su longitud de contorno en reposo $\locooi$, su longitud extremo-extremo en reposo $\leteoi$ y su longitud extremo-extremo actual $\letei$.
Se define como medida de esta deformación a la elongación extremo-extremo:

\begin{equation}
	\lamei = \frac{\letei}{\leteoi}
\end{equation}

Dado que afecta significativamente su comportamiento mecánico, es importante identificar si la fibra se halla enrulada o recta.
Para ello se define el valor de elongación de reclutamiento, como el valor de $\lamei$ para el cual la fibra deviene recta:

\begin{equation}
\lameri = \frac{\locooi}{\leteoi}
\end{equation}

Entonces, la fibra se encuentra: i) curvada o enrulada si $\lamei<\lameri$ ($\letei < \locooi$) o ii) recta si $\lamei \geq \lameri$ ($\letei \geq \locooi$).

Luego, en caso de existir deformación plástica, ésta resulta en un alargamiento de la longitud natural de la fibra.
Como se han identificado dos medidas de longitud de referencia ($\locooi$ y $\leteoi$), pueden establecerse una medida de elongación plástica para cada una: la elongación plástica de contorno $\lam_p^c$ y la elongación plástica extremo-extremo $\lam_p^e$.
De esta forma, las longitudes naturales o de reposo de la fibra resultan:

\begin{align} 
	{\locooi}' &= \lam_p^c {\locooi} \label{eq:defor_plas_fibra_loco} \\
	{\leteoi}' &= \lam_p^e {\leteoi} \label{eq:defor_plas_fibra_lete}
\end{align}
donde ${\locooi}'$ y ${\leteoi}'$ representan las longitudes de reposo de la fibra luego de sufrir deformación plástica ($\locooi$ y $\leteoi$ son los valores iniciales, previo a toda deformación plástica).

Adicionalmente, se define el vector orientación deformado $\ai$ según las posiciones actuales de los nodos extremos:

\begin{equation}
	\ai = \frac{\y_{i_2} - \y_{i_1}}{\leteoi}
\end{equation}
de donde puede deducirse además que $\norma{\ai}=\lamei$.

Por otro lado, cada nodo $n$ de la malla se encuentra conectado al conjunto de $N_f^n$ fibras $\setF_n$:

\begin{equation}
\setF_n = \braces{i=\parent{i_1, i_2} , i_1\in\setN, i_2\in\setN, \ i=1,...,N_f, \ \text{tal que} \ n \in \parent{i_1, i_2}}
\end{equation}

Y cada fibra de este conjunto posee un vector orientación que puede ser saliente o entrante al nodo según los siguientes casos (figura \ref{fig:nodo_conectado_a_fibras}):

\begin{align}
n &= i_1 \implies \text{vector orientación saliente} \\
n &= i_2 \implies \text{vector orientación entrante}
\end{align}

Luego, resulta útil definir un vector orientación siempre saliente para la fibra $i$ conectada con el nodo $n$, dado por:

\begin{equation} \label{eq:vector_orientacion_saliente}
	\ai^n = 
	\begin{cases}
		\ai &\quad \text{si} \quad n=i_1 \\
		-\ai &\quad \text{si} \quad n=i_2
	\end{cases}
\end{equation}

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.7\linewidth]{../Imagenes/04-Mecanica/nodo_conectado_a_fibras}
	\caption[Esquema de nodo de la malla con las fibras conectadas al mismo.]{Esquema del nodo $n$ de la malla y las fibras que se conectan con el mismo. De las cuatro fibras conectadas se indican dos con numeración $i$ y $j$ respectivamente. De los nodos circundantes se indican dos con la numeración $m$ y $o$. Para la fibra $i$, el nodo $n$ es su nodo final ($n=i_2$, su vector orientación $\a_i$ es entrante al nodo), por lo que se da el caso que $\a_i^n=-\a_i$ (ver ecuación \ref{eq:vector_orientacion_saliente}). En cambio, para la fibra $j$ el nodo $n$ es su nodo inicial ($n=j_1$, su vector orientación $\a_j$ es saliente del nodo), por lo que $\a_j^n=\a_j$.}
	\label{fig:nodo_conectado_a_fibras}
\end{figure}
