El interés en el modelado mecánico de los biomateriales fibrosos ha crecido sostenidamente en las últimas décadas, sin embargo todavía no se ha establecido una base teórica sólida y consensuada al respecto.
Como una primera aproximación, el desarrollo de modelos fenomenológicos específicos para biomateriales representó un importante avance al lograr capturar la respuesta no lineal en tensión-deformación típica de estos materiales, compuesta por una región de baja rigidez y una de alta rigidez \cite{Choi1990,Courtney2006,Tong1976}.
Sin embargo, en estos modelos la relación en tensión-deformación consiste en ecuaciones matemáticas macroscópicas cuyos parámetros se ajustan para predecir la respuesta constitutiva del material correspondiente.
Por lo tanto, no aportan información acerca de la relación entre los fenómenos microscópicos subyacentes en el material y la respuesta macroscópica observada.

Los modelos microestructuralmente inspirados, en cambio, representan un avance respecto de los fenomenológicos, ya que permiten derivar las ecuaciones constitutivas macroscópicas a partir de un análisis de los procesos microscópicos bajo deformación.
Estos modelos, en base a suponer deformación afín de la microescala, han permitido vislumbrar que el comportamiento macroscópico anisotrópico y altamente no lineal de los biomateriales se debe en gran medida a la rotación y reclutamiento de las nanofibras, además de a la no linealidad propia del material constituyente \cite{Criscione2003,Fan2014,HolzapfelOgden2009,Lanir1979,Sacks2000,Sacks2003}.
En el campo del modelado de materiales fibrosos artificiales, una gran cantidad de trabajos se abocaron a encontrar las propiedades mecánicas macroscópicas efectivas en base a un análisis microestructural.
Bajo esta perspectiva, se desarrollaron modelos para diferentes materiales fibrosos como papel, lana, hilados, y otros materiales textiles, con creciente grado de complejidad geométrica \cite{VanWyk1946,Cox1952,Narter1999,Carnaby1989,Pan1989}.
Si bien la microestructura se tiene en cuenta en estos modelos para derivar una relación constitutiva, no deja de tratarse de ecuaciones matemáticas que presuponen a priori los modos de deformación microscópicos.
Por lo tanto, no logran capturar la heterogeneidad cinemática a escala de cada nanofibra, ya que su deformación estará determinada por las posiciones y deformaciones de las nanofibras circundantes.

La necesidad de una mejor comprensión de los fenómenos microestructurales de las matrices electrohiladas ha llevado el campo de estudio en la dirección del modelado  multiescala, entendiendo al mismo como una técnica de modelado en la que múltiples modelos en diferentes escalas se plantean de manera simultánea para resolver un mismo sistema \cite{MultiscaleModeling}.
Resulta pertinente aclarar que si bien sería teóricamente posible abordar el problema mediante simulación directa, es decir un modelo del componente macroscópico con un nivel de detalle que alcance hasta la microestructura, tal enfoque resulta prácticamente irrealizable por el alto costo computacional asociado.
Un importante primer paso para la realización de simulaciones confiables de este tipo consiste en identificar las características geométricas microscópicas que describen la microestructura nanofibrosa.
Algunos de los procesos microestructurales bajo deformación han podido ser observados mediante imágenes SEM de matrices electrohiladas \cite{Stella2010tissue,Silberstein2012}.
Aún así, dicha información está restringida a la superficie exterior del material, y poco se puede inferir acerca de cuestiones tridimensionales como la densidad de vínculos cruzados entre las nanofibras, o el largo promedio de los segmentos entre vínculos \cite{Zundel2017}.
Como consecuencia, el modelado multiescala se presenta como una oportunidad no sólo de reproducir la micromecánica de los materiales electrohilados, sino también de elucidar cuáles y cómo son los mecanismos microscópicos que no pueden ser observados experimentalmente.
Los modelos basados en el concepto de RVE se encuentran con diversos grados de complejidad, incluyendo composiciones de unas pocas fibras discretas hasta redes fibrosas de geometrías aleatorias tridimensionales.
En todo caso, las suposiciones sobre las que se basa la construcción del RVE y el comportamiento mecánico de las fibras resultan esenciales para el comportamiento mecánico obtenido. 
Los modelos más sencillos requieren mayor número de hipótesis simplificativas, pero permiten focalizar el análisis sobre unas pocas características de interés a la vez que su resolución es muy eficiente.
Los modelos más complejos, en cambio, poseen un alto número de variables acopladas que derivan en la respuesta conjunta, conllevando un alto costo computacional asociado.
Dependiendo de la aplicación, por lo tanto, se debe hacer un balance entre eficiencia computacional y complejidad del modelo.
En las últimas décadas han proliferado los modelos basados en RVEs discretos, encontrando una gran variedad de modelos dependiendo de la combinación de los parámetros que determinan la topología microscópica, así como el comportamiento mecánico individual de las fibras y de sus enlaces.

Stylianopoulos y colaboradores \cite{Stylianopoulos2008} investigaron la influencia de la distribución de orientaciones de las nanofibras sobre la respuesta mecánica macroscópica. 
Para ello, utilizaron un modelo multiescala computacional para simular matrices electrohiladas de poliuretano bajo tracción uniaxial. Generando geometrías con diferentes grados de alineación a lo largo de la dirección de carga, encontraron que el módulo de elasticidad a la tracción aumenta considerablemente con la alineación debido a un mayor número de fibras compartiendo la carga.
Si bien ese modelo otorgó buena concordancia con resultados experimentales, los parámetros ajustados mostraron discrepancias para la rigidez de las nanofibras y el módulo de rigidez transversal a la dirección de alineación de las fibras.
Estas discrepancias se deben, posiblemente, a la falta de consideración por una distribución de enrulamiento inicial así como por el método artificial de determinación de uniones entre fibras.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.7\linewidth]{../Imagenes/00-Introduccion/stylianopoulos2008_rve}
	\caption[RVE de Stylianopoulos y colaboradores.]{RVE de Stylianopoulos y colaboradores \cite{Stylianopoulos2008} para: a) una malla isotrópica, b) una malla alineada.}
	\label{fig:stylianopoulos2008_rve}
\end{figure}

Para estudiar el efecto de la curvatura de las fibras sobre el módulo de elasticidad de matrices electrohiladas, Pai y colaboradores \cite{Pai2011} propusieron un RVE conformado por un arreglo de 4 fibras con curvatura variable.
Además de la resistencia de las fibras a la tracción, tuvieron en cuenta también la energía de deformación de las fibras bajo flexión.
Encontraron que la curvatura de las fibras es una característica esencial y que su desenrulamiento durante la deformación resulta en un comportamiento más compliante para geometrías con fibras más enruladas.
Además, se determinó que otros factores de importancia son la porosidad, el diámetro de las fibras, y la distancia entre uniones.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.7\linewidth]{../Imagenes/00-Introduccion/pai2011_rve}
	\caption[RVE de Pai y colaboradores.]{RVE de Pai y colaboradores \cite{Pai2011}: a) fibras rectas y b) fibras con curvatura.}
	\label{fig:pai2011_rve}
\end{figure}

Wei y colaboradores \cite{Wei2009} tomaron un enfoque diferente modelando la mecánica microscópica con métodos de dinámica molecular.
Plantearon un RVE compuesto por cientos de fibras aleatoriamente distribuidas en un dominio cuadrado, donde cada fibra se representa de forma análoga a una cadena polimérica, con masas esféricas unidas por enlaces covalentes simulados mediante barras elásticas y juntas elásticas.
Con este método, se focalizaron en el análisis de las uniones entre las fibras sobre la respuesta macroscópica, pudiendo considerar uniones soldadas e interacciones por proximidad de tipo van der Waals.
Realizaron simulaciones con uniones soldadas, mitad soldadas, y no soldadas, encontrando que la densidad de uniones incrementa sensiblemente el módulo de rigidez y la resistencia a la rotura, aunque un exceso de fusiones deriva en la disminución de la energía de fractura.
Un resultado interesante es la pequeña diferencia entre uniones soldadas y mitad soldadas, ya que indica que si bien controlar la densidad de puntos de fusión resulta crucial, no es necesario que esa fusión sea total, pudiendo concluir que la resistencia del propio punto de unión se vuelve irrelevante por encima de un determinado valor umbral.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.6\linewidth]{../Imagenes/00-Introduccion/wei2009_rve}
	\caption[RVE de Wei y colaboradores.]{RVE de Wei y colaboradores \cite{Wei2009}.}
	\label{fig:wei2009_rve}
\end{figure}

Rizvi y colaboradores \cite{Rizvi2012,Rizvi2014} presentaron un modelo matemático en el que se representan las propiedades microestructurales mediante funciones estadísticas, planteando un paralelismo entre un RVE formado por fibras discretas y una descripción probabilística con tres variables de estado: diámetro, longitud de contorno y separación lateral entre los extremos.
En esta descripción, la curvatura y la orientación se hallan implícitos como funciones de los tres parámetros de control, pudiendo efectivamente obtener microestructuras con diferentes grados de alineación y tortuosidad.
Los resultados de simulaciones bajo tracción uniaxial refuerzan la idea de que las variables microestructurales determinan en gran medida la respuesta macroscópica, destacando que la resistencia a la rotura macroscópica de la matriz depende principalmente de la población de fibras inicialmente curvadas.

Carleton y colaboradores \cite{Carleton2015} implementaron un algoritmo estocástico de \textit{Random Walk} capaz de obtener RVEs formados por capas bidimensionales con nanofibras de geometrías variables en orientación y tortuosidad.
En su modelo, hicieron uso de técnicas de muestreo estadístico para conformar las fibras como una concatenación de segmentos lineales, imponiendo además la condición de periodicidad en los bordes por considerar al RVE como una celda unitaria.
Comparando estimaciones matemáticas con simulaciones geométricas obtuvieron una buena concordancia para la fracción de volumen ocupado por las fibras así como para la densidad de intersecciones.
En un trabajo subsiguiente \cite{Carleton2017}, modelaron la respuesta mecánica de este RVE donde fibras fueron representadas como vigas de Euler-Bernoulli compuestas de un material hiperelástico de Yeoh, obteniendo buena concordancia con datos experimentales.
Adicionalmente, estudiaron el efecto de las variables microscópicas (orientación y tortuosidad de las fibras) sobre la respuesta macroscópica, aunque la utilización de un material no lineal puede haber ocultado los efectos no lineales debidos a estas características.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.6\linewidth]{../Imagenes/00-Introduccion/carleton2015_rve}
	\caption[RVE de Carleton y colaboradores.]{RVE de Carleton y colaboradores \cite{Carleton2015}: a) malla isotrópica, b) malla alineada.}
	\label{fig:carleton2015_rve}
\end{figure}

Zundel y colaboradores \cite{Zundel2017} formularon un modelo basado en un RVE también conformado por capas bidimensionales, introduciendo el concepto de \concepto{capa de interacción} como una distancia normal entre las fibras para la cual se produce una unión.
En su trabajo, las fibras se describieron mediante curvas sinusoidales y se les asignó un comportamiento mecánico elástico lineal seguido por plasticidad con endurecimiento lineal, lo cual se asemeja al comportamiento real observado experimentalmente para nanofibras poliméricas.
Las simulaciones mostraron muy buena concordancia con datos de ensayos experimentales bajo carga uniaxial sobre matrices elecrohiladas de PA6(3)T previamente reportados por Silberstein y colaboradores \cite{Silberstein2012}.
Además llevaron a cabo un análisis sobre la realineación de las fibras y la relación entre la orientación y el estiramiento de las nanofibras bajo deformación.
Estos resultados permitieron obtener una diferencia sustancial en el estiramiento de las fibras entre el modelo discreto y modelos afines, algo que en trabajos previos no se había podido elucidar.
Posteriormente, Domaschke y colaboradores \cite{Domaschke2019} llevaron a cabo una extensión del modelo en tres dimensiones, encontrando que el modelo bidimensional es una aproximación válida para matrices con porosidades del orden de las encontradas en materiales electrohilados.

%\begin{figure}[!h]
%	\centering
%	\includegraphics[width=0.7\linewidth]{../Imagenes/00-Introduccion/zundel2017_rve}
%	\caption[RVE de Zundel y colaboradores.]{RVE de Zundel y colaboradores \cite{Zundel2017}: a) Vista lateral \textit{renderizada} donde se aprecia la capa de interacción ($t_1$), b) Vista superior \textit{renderizada}, c) Vista superior proyectada.}
%	\label{fig:zundel2017_rve}
%\end{figure}