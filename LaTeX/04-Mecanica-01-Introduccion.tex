Los reemplazos de las arterias naturales, al igual que éstas, se encuentran sometidos a una presión fluctuante impuesta por el corazón que influye en el comportamiento de las células y los tejidos. 
Además, la implantación de injertos o prótesis altera la hemodinámica local y la distribución de tensiones en la pared arterial en la zona de anastomosis, causando restricciones mecánicas perjudiciales en la deformación de la arteria receptora durante el ciclo cardíaco \cite{Rachev2011}.
Por lo tanto, en el campo de la ingeniería de tejidos, las propiedades mecánicas de los materiales que constituyen los injertos vasculares son un factor de relevancia en su diseño \cite{Lee2008}.

Actualmente, los injertos sintéticos aprobados clínicamente presentan una alta probabilidad de falla in vivo, debido en la mayoría de los casos a una discrepancia mecánica con la arteria nativa \cite{Lee2008}.
La discrepancia en la zona de anastomosis causa hiperplasia intimal y una reducción en la tasa de permeabilidad \cite{Abbott1987}.
En particular, la discrepancia entre la compliancia del injerto vascular y la arteria nativa conduce a una falla para períodos prolongados de implantación, especialmente para conductos de pequeño diámetro \cite{Sonoda2001}.
En términos del comportamiento biomimético, un injerto vascular necesita una compliancia mecánica que logre un acuerdo geométrico, de tensiones y de deformaciones durante todo el ciclo pulsátil \cite{Abbott1987}.
Teniendo en cuenta esto, la caracterización del desempeño mecánico de los injertos electrohilados juega un papel clave en el diseño y desarrollo de los mismos. 

La utilización de simulaciones mecánicas realistas que logren vincular los fenómenos que ocurren a diferentes escalas puede resultar de gran ayuda para el diseño y manufactura de injertos vasculares de ingeniería de tejidos, especialmente si se considera la capacidad de los métodos de fabricación actuales de ejercer control sobre propiedades microestructurales de relevancia.
Más aún, un modelo multiescala que permita especificar una geometría microscópica que resulte en un comportamiento mecánico macroscópico deseado habilitaría la posibilidad de fabricar, en tiempos razonables, injertos vasculares hechos a medida no sólo desde un punto de vista geométrico sino también constitutivo.

Como se mencionó en el capítulo \ref{ch:introduccion}, las matrices electrohiladas poseen, al igual que muchos tejidos biológicos, una microestructura conformada por capas fibrosas superpuestas.
Además, al igual que en los tejidos nativos, la heterogeneidad y anisotropía a nivel microscópico afecta el comportamiento mecánico macroscópico y la funcionalidad de los correspondientes biomateriales sintéticos.
La respuesta constitutiva en tensión-deformación a nivel macroscópico es un resultado del promedio de las tensiones y deformaciones que se producen a nivel microscópico, y a su vez, la deformación de cada fibra a escala microscópica resulta de las posiciones y deformaciones de las fibras circundantes a través de las interconexiones de la malla fibrosa \cite{Chandran2007}.
Claramente, para obtener un entendimiento detallado de estos fenómenos multiescala, es necesario proveer simulaciones mecánicas multiescala con un alto nivel de detalle a nivel microscópico.

En este capítulo se presenta un modelo micromecánico para mallas de fibras con el objetivo de ser implementado en un modelo multiescala para injertos vasculares electrohilados.
Inicialmente se da una descripción detallada de la cinemática de una malla de fibras interconectadas y se explicitan las expresiones del equilibrio mecánico.
Luego el modelo se valida con datos de ensayos experimentales de tracción uniaxial sobre matrices electrohiladas de PLLA.
Finalmente, se efectúa un análisis de la evolución de la microestructura durante la tracción, evidenciando los mecanismos microscópicos que tienen lugar durante la deformación macroscópica.

