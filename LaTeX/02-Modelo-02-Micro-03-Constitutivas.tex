\subsubsection{Fibra individual}

Las nanofibras son elementos de gran esbeltez que presentan una alta rigidez a la tracción pero al querer comprimirlas se enrulan con facilidad debido a su baja resistencia a la flexión \cite{BilliarSacks1999}.
Por lo tanto, su comportamiento elástico puede representarse adecuadamente por medio de una ley bilineal en tensión-deformación caracterizada por un módulo de rigidez fuerte ($\Et$) y uno débil ($\Eb$) como se muestra en la figura \ref{fig:respuesta_fibra_bilineal}.
$\Et$ representa la resistencia de la fibra recta y tensa a la tracción, mientras que $\Eb$ representa la débil resistencia de la fibra enrulada \cite{Schwartz1994}.

Además, las fibras en su estado inicial no se hallan necesariamente rectas, sino que pueden presentar diferentes niveles de enrulamiento.
Luego, a medida que se la estira (es decir, que sus extremos se alejan entre sí) se va desenrulando hasta que, en un determinado valor de elongación, la fibra deviene recta.
Debido a que en ese instante la nanofibra incrementa considerablemente su capacidad de tomar carga, se dirá que es \concepto{reclutada}.
Se define como \concepto{elongación de reclutamiento} ($\lamer$) al valor de elongación para el cual esto ocurre.
También, en ocasiones, se referirá a $\lamer$ como el \concepto{nivel o grado de enrulamiento inicial} que presenta una fibra, entendiendo que mayores valores de $\lamer$ se corresponden de forma biunívoca con mayores enrulamientos iniciales.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.9\linewidth]{../Imagenes/02-Modelo/respuesta_fibra_bilineal}
	\caption[Respuesta bilineal de una nanofibra.]{Respuesta mecánica bilineal individual de una fibra. La nanofibra se deposita enrulada en su estado de reposo (A). Si se la comprime (acercando sus extremos) o estira (alejando sus extremos) sin llegar a rectificarla, ejerce una leve resistencia flexural. Si se la estira hasta devenir recta (C), su rigidez a la tracción se incrementa considerablemente.}
	\label{fig:respuesta_fibra_bilineal}
\end{figure}

Luego, se puede expresar la tensión ingenieril ($\tf$) de la fibra de la siguiente manera:

\begin{equation} \label{eq:nanofibra_ecuacion}
\tf(\lame) = 
\begin{cases}
\Eb \parent{\lame - 1}  \quad &if \quad \lame < \lamer  \\
\Eb \parent{\lamer - 1} + \Et \parent{ \frac{\lame}{\lamer} - 1}  \quad &if \quad \lame \geq \lamer
\end{cases} 
\end{equation}

Es válido aclarar que la ecuación constitutiva para una nanofibra se define en términos de la tensión ingenieril, es decir, carga soportada por unidad de área de la sección de referencia.
De esta forma se tiene la ventaja de poder correlacionar directamente la curva tensión-deformación con datos de mediciones experimentales.

\subsubsection{Haz de fibras}

La matriz electrohilada se compone de fibras individuales que se depositan con orientaciones, en general, aleatorias y con distintos niveles de enrulamiento.
De acuerdo con la cinemática adoptada, todas las fibras que comparten una orientación, están sometidas a igual deformación.
Por lo tanto, englobar a todas las fibras que tienen una misma orientación en un solo objeto permite simplificar el análisis y las expresiones que se obtienen.
Para ello, se introduce el concepto de \concepto{haz de fibras} como un cierto número de fibras con orientación compartida y que en lugar de encontrarse rectas en su estado inicial, presentan una variabilidad estadística en sus niveles de enrulamiento.

Como se detalló previamente, las fibras enruladas poseen una rigidez a la elongación mucho menor que las que se hayan rectas.
Es por eso que a medida que el haz es sometido a tracción, el subconjunto de fibras de la matriz que se encuentran rectas soporta la mayor parte de la carga, en cuanto que el aporte a la resistencia de las fibras enruladas es prácticamente despreciable.
Mientras se incrementa la carga, las nanofibras enruladas van progresivamente deviniendo rectas, aumentando en gran medida su resistencia a incrementos de elongación.
Este proceso de \concepto{reclutamiento progresivo} de fibras provoca que el módulo de rigidez del haz dependa no solamente de la rigidez de las fibras individuales, sino también de la distribución estadística de enrulamientos de las fibras que componen al haz.
Para ejemplificar esto, la figura \ref{fig:haz_esquema} muestra un haz esquemático compuesto por tres fibras con tres elongaciones de reclutamiento diferentes.
Bajo una determinada elongación (dada por el alejamiento de los extremos), la fibra $f_1$ se mantiene enrulada y su resistencia a la tracción es despreciable, la fibra $f_2$ está deviniendo recta en su elongación de reclutamiento y la fibra $f_3$ se encuentra recta y tensa, por encima de su elongación de reclutamiento, por lo que ejerce una tensión mucho mayor que $f_1$ y $f_2$.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.9\linewidth]{../Imagenes/02-Modelo/haz_esquema}
	\caption[Esquema de un haz de fibras.]{Esquema de un haz sencillo compuesto de tres fibras con diferentes valores de reclutamiento. En la parte superior, las nanofibras que componen el haz se encuentran encerradas por un cilindro imaginario. En la configuración de reposo (A) las nanofibras ($f_1$, $f_2$ y $f_3$) se encuentran enruladas según su estado de deposición con distintos valores de reclutamiento ($\lamer_1>\lamer_2>\lamer_3$). En la configuración deformada (B) la fibra $f_1$ se mantiene enrulada, la fibra $f_2$ se encuentra en su elongación de reclutamiento y la fibra $f_3$ se encuentra recta y estirada. Bajo esta configuración, la resistencia de las fibras $f_1$ y $f_2$ es marginal respecto de la fibra $f_3$.}
	\label{fig:haz_esquema}
\end{figure}

Si se asume un gran número de fibras por cada haz, es posible realizar un enfoque estadístico para incorporar la dispersión en las elongaciones de reclutamiento de las fibras al análisis bajo la forma de una función densidad de probabilidad de reclutamiento $\tdf(\lam)$.
En tal caso, puede considerarse que la cantidad de fibras poseen niveles de enrulamiento entre $\lamer$ y $\lamer+\dlamer$ viene dada por $\tdf(\lam)\diff\lam$.
Para implementar esta distribución en el RVE propuesto, se asume una distribución de enrulamientos normal truncada, dada por:

\begin{equation} \label{eq:distribucion_reclutamientos}
	\tdf(\lamer) = 
		\begin{cases}
			\displaystyle 0 \quad &if \quad \lamer < \lamermin \\
			\parent{\frac{1}{C}} \displaystyle e^{\sqbrac{-\frac{\parent{\lamer-{\lamero}}^2}{2 {\parent{\lamers}}^2}}} \quad &if \quad  \lamermin \leq \lamer \leq \lamermax \\
			\displaystyle 0 \quad &if \quad \lamer > \lamermax
		\end{cases}
\end{equation}
donde $\lamero$ es el valor medio de la distribución, $\lamers$ es el valor de desviación estándar, $\lamermin$ y $\lamermax$ son los valores mínimo y máximo de reclutamiento de las fibras en el haz, y $C$ es una constante que toma el valor necesario para asegurar la condición de normalización \ref{eq:condicion_normal}:

\begin{equation} \label{eq:condicion_normal}
	\int_{\lamermin}^{\lamermax} \tdf(\lamer) \diff\lamer = 1 
\end{equation}

Consecuentemente, la tensión desarrollada por el haz bajo una determinada deformación está dada por el promedio de las tensiones de las fibras que lo componen bajo esa misma deformación:

\begin{equation} \label{eq:tension_haz}
\thaz(\lam) = \int_{\lamermin}^{\lamermax} \tf(\lam, \lamer) \tdf(\lamer) \dlamer 
\end{equation}
donde se ha explicitado la dependencia de la tensión de las fibras de sus correspondientes elongaciones de reclutamiento. 

A causa de la dispersión en los enrulamientos de las nanofibras, la cantidad de fibras reclutadas para cada valor de deformación varía de forma gradual como se muestra en la figura \ref{fig:haz_curvas_genericas_fdr_fda}.
En su configuración inicial, el haz posee ninguna o pocas fibras que se encuentran efectivamente rectas, y ninguna está aún soportando carga ya que se trata del estado de reposo.
En cuanto el haz se somete a tracción, las fibras comienzan a desenrularse, por lo cual progresivamente van deviniendo rectas e incrementan sustancialmente su aporte a la resistencia frente a la carga externa.
A este fenómeno gradual se lo denominará \concepto{reclutamiento progresivo}, y se evidencia en la respuesta en tensión-elongación del haz por una curva en forma de \dquotes{J}, donde el módulo tangente efectivo se incrementa continuamente desde $\approx 0$ hasta un valor final constante cuando todas las fibras están reclutadas (\ref{fig:haz_curvas_genericas_tension}).

\begin{figure}[ht] 
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=0.9\linewidth]{../Imagenes/02-Modelo/haz_curvas_genericas_fdr_fda.pdf}
		\subcaption{}\label{fig:haz_curvas_genericas_fdr_fda}
		\vspace{4ex}
	\end{subfigure}%% 
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=0.9\linewidth]{../Imagenes/02-Modelo/haz_curvas_genericas_tension.pdf} 
		\subcaption{}\label{fig:haz_curvas_genericas_tension}
		\vspace{4ex}
	\end{subfigure} 

	\caption[Distribución de enrulamiento y respuesta en tensión-elongación del haz de fibras.]{a) Distribución de reclutamiento normal truncada esquemática, incluyendo la función densidad de reclutamiento $\tdf(\lamer)$ (línea sólida, indicada como FDR) y la función de distribución acumulada (línea punteada, indicada como FDA) del haz. b) Curva de tensión-elongación correspondiente, donde se evidencia el reclutamiento progresivo en la forma \dquotes{J}.}
	\label{fig:haz_curvas_genericas} 
\end{figure}

Para mayor abundancia, es posible identificar tres regiones bien diferenciadas en la respuesta mecánica del haz: i) una región inicial floja que corresponde a una configuración en la que muy pocas o todas las fibras se hallan enruladas ($\lam<\lamero-\lamers$), ii) una región de transición donde se produce el reclutamiento progresivo de las fibras a medida que se incrementa la deformación del haz ($\lamero-\lamers < \lam < \lamero + \lamers$) y iii) una región final de respuesta lineal o cuasilineal donde todas o prácticamente todas las fibras se encuentran reclutadas ($\lam > \lamero+\lamers$).
Queda en evidencia que el rango de elongación en el cual ocurre la transición, es decir la severidad con que se incrementa el módulo tangente del haz, está determinado en mayor medida por la dispersión de la distribución normal truncada ($\lamers$).
En todos los casos, sin embargo, la aplicación de una distribución continua de enrulamientos resulta en curvas de tensión-deformación suaves a pesar de haber adoptado, para las fibras individuales, una ley constitutiva bilineal de derivada discontinua en $\lam=\lamer$.