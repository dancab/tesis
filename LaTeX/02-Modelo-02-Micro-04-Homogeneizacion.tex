Dado que el dominio del RVE se corresponde con una partícula material macroscópica, se asume como ley de homogeneización que en cada punto macroscópico el tensor de tensiones de Cauchy ($\T$) equivale al promedio en volumen de su contraparte microscópica ($\T_\mu$) sobre el volumen deformado del RVE ($V_\mu$) \cite{NematNasserMicromechanics}.

\begin{equation}
	\T = \frac{1}{V_\mu} \int_{V_\mu} \T_\mu \dif{V_\mu}
\end{equation}

Sin embargo, para un RVE discreto con espacios vacíos carentes de partículas materiales tanto la deformación como el tensor de tensiones no están definidos.
En este caso conviene expresar el promedio sobre el volumen microscópico efectivamente ocupado por material.
Particularmente para el caso de una microestructura fibrosa, esto resulta en una sumatoria sobre los volúmenes de las nanofibras, obteniendo:

\begin{equation} \label{eq:homogeneizacion_01}
	\T = \frac{1}{V_\mu} \sum_{i=1}^{N_f} \int_{V_i} \T_\mu \dif{V_i}
\end{equation}
donde $N_f$ es el número de fibras en el RVE y $V_i$ es el volumen ocupado por cada nanofibra $i$.

Luego, resulta necesario establecer una expresión para el tensor de tensiones dentro de cada fibra $i$, en función de la tensión ingenieril desarrollada por la fibra ($\tfi$), de su elongación extremo-extremo ($\lamei$) y de su vector orientación ($\ai$):

\begin{equation} \label{eq:tensor_fibra}
	\evalat{\T_\mu}{V_i} = \frac{\tfi}{\lamei} \parent{\ai \otimes \ai}
\end{equation}

Cabe resaltar que esta expresión implica una tensión constante en la fibra, lo cual tiene sentido para fibras reclutadas (rectas).
Para fibras enruladas se mantiene esta expresión a modo de aproximación, considerando despreciable el error que se introduce dado que el aporte de estas fibras a la tensión macroscópica es marginal.

Resulta de interés explicitar como variable, por su importancia experimental, a la fracción de volumen material de la malla ($\fv$), definida como la fracción del volumen del RVE ($V_\mu$) que se halla ocupado por las nanofibras.

\begin{equation} \label{eq:fracvol}
	\fv = \frac{V_f}{V_\mu}
\end{equation}
donde $V_f = \sum_{i=1}^{N_f} V_i$ es el volumen del RVE ocupado por las nanofibras.

Además, dada la condición de deformación afín en la frontera, es posible expresar el volumen deformado del RVE en función del tensor gradiente de deformaciones macroscópico y del volumen inicial del RVE ($V_\mu^0$):

\begin{equation} \label{eq:volmu_afin}
	V_\mu = \det{\F} V_\mu^0
\end{equation}

Incorporando las expresiones \ref{eq:tensor_fibra}, \ref{eq:fracvol} y \ref{eq:volmu_afin} en \ref{eq:homogeneizacion_01}, la fórmula de homogeneización adopta la forma:

\begin{equation} \label{eq:homogeneizacion_02}
\T = \frac{\fv}{\det{\F} V_f} \sum_{i=1}^{N_f} \frac{\tfi}{\lamei} \parent{\ai \otimes \ai} V_i
\end{equation}

%\subsubsection*{Malla bajo deformación afín u homogénea}

La ecuación \ref{eq:homogeneizacion_02} admite la posibilidad de mallas con estructuras aleatorias bajo modos de deformación diferentes para cada fibra.
Sin embargo, es posible incorporar la deformación del RVE propuesta donde $\ai = \F \aoi$, pudiendo reescribir la fórmula de homogeneización en función de las orientaciones iniciales de las fibras:

\begin{equation} \label{eq:homogeneizacion_03_afin}
\T = \frac{\fv}{\det{\F} V_f} \F \sqbrac{\sum_{i=1}^{N_f} \frac{\tfi}{\lamei} \parent{\aoi \otimes \aoi} V_i} \FT
\end{equation}

%\subsubsection*{Malla con haces de fibras}

A continuación, para hacer surgir naturalmente el concepto de haz de fibras, se reordena la sumatoria agrupando en una segunda sumatoria a todas las fibras que comparten una misma orientación: 

\begin{equation} \label{eq:homogeneizacion_03_afin_haces_01}
\T = \frac{\fv}{\det{\F} V_f} \F \braces{\sum_{j=1}^{N_h} \sqbrac{\sum_{k=1}^{N_f^j} \frac{\tfjk}{\lamej} \parent{\aoj \otimes \aoj} V_{jk}}} \FT
\end{equation}
donde $N_h$ es el número de haces que componen al RVE (en el caso propuesto $N_h=6$), $N_f^j$ es el número de fibras que componen el haz $j$ y el sobreíndice $jk$ indica a la fibra $k$ del haz $j$.

Al igual que para la distribución de enrulamiento, también es posible, mediante un enfoque estadístico, llevar en consideración una distribución uniforme sobre los volúmenes de las fibras que componen cada haz.
De modo que el volumen ocupado por las fibras puede expresarse según:

\begin{equation}
	V_f = \sum_{j=1}^{N_h} \sum_{k=1}^{N_f^j} V_{jk}
\end{equation}

Luego, si todas las fibras poseen idéntico diámetro y, por lo tanto, misma sección transversal $A_f$:

\begin{equation}
	V_f = \sum_{j=1}^{N_h} \int_{\lamermin}^{\lamermax} A_f \lamer \leteo \tdf(\lamer) \dlamer
\end{equation}

Además, se asume que todos los haces presentes en el RVE se componen de igual manera, es decir, que todos tienen la misma función distribución de probabilidad $\fdrp(\lamer)$.
Luego, es posible llegar a una expresión relativamente sencilla para el volumen ocupado por las fibras cuando éstas se agrupan en haces:

\begin{equation}
	V_f = N_h A_f \leteo \lamero
\end{equation}

Aplicando esta simplificación a la ecuación \ref{eq:homogeneizacion_03_afin_haces_01}, la fórmula de homogeneización puede reexpresarse la tensión macroscópica en función de la deformación macroscópica y de los parámetros geométricos y constitutivos microscópicos:

\begin{equation} \label{eq:homogeneizacion_03_afin_haces_02}
\T(\F) = \frac{\fv}{\det{\F} N_h\lamero} \F \sqbrac{\sum_{j=1}^{N_h} \int_{\lamermin}^{\lamermax} \frac{\tf(\lam,\lamer)}{\lamej} \parent{\aoj \otimes \aoj} \tdf(\lamer) \dlamer } \FT
\end{equation}

Finalmente, se condensa la expresión anterior haciendo surgir la tensión del haz (ecuación \ref{eq:tension_haz}):

\begin{equation} \label{eq:homogeneizacion_03_afin_haces_03}
\T(\F) = \frac{\fv}{\det{\F} N_h\lamero} \F \sqbrac{\sum_{j=1}^{N_h} \frac{\thaz^{j}(\lam)}{\lamej} \parent{\aoj \otimes \aoj} } \FT
\end{equation}

Cabe resaltar que aunque se aplicó la noción de un haz compuesto por un gran número de fibras, es posible mantener esta última expresión para haces con número reducidos de fibras.
En estos casos la tensión de haz debe calcularse como el promedio de las tensiones de las fibras ponderado con las secciones transversales, en lugar de aplicar la ecuación \ref{eq:tension_haz}, utilizada bajo la aproximación de infinitas fibras.

La fórmula de homogeneización \ref{eq:homogeneizacion_03_afin_haces_03} tiene la ventaja adicional de poder transformarse fácilmente para ser utilizada tanto con el primer tensor de tensiones de Piola-Kirchhoff ($\P$) como con el segundo ($\Spk$), según las clásicas definiciones $\T = \frac{1}{\det{\F}} \P \FT$ y $\P = \F \Spk$:

\begin{align}
	\P &= \frac{\fv}{ N_h\lamerm} \F \sqbrac{\sum_{j=1}^{N_h} \frac{\thaz^{j}}{\lam^j} \parent{\aoj \otimes \aoj} } \\
	\Spk &= \frac{\fv}{ N_h\lamerm} \sqbrac{\sum_{j=1}^{N_h} \frac{\thaz^{j}}{\lam^j} \parent{\aoj \otimes \aoj} }
\end{align}