El modelado convencional, en el marco de la mecánica del continuo, trata con materiales idealizados en los que se asume que la distribución de tensiones y deformaciones puede considerarse homogéneo en el entorno infinitesimal de una dada partícula material (elemento material infinitesimal).
En este caso, además, los esfuerzos internos en un punto material cualquiera del continuo queda determinado por la historia de las deformaciones en ese punto.
Adicionalmente, en ciertos casos, como el de materiales hiperelásticos, se puede prescindir de las deformaciones pasadas, pudiendo establecer leyes constitutivas que sólo dependen de la deformación actual.

Sin embargo, la homogeneidad aparente a nivel macroscópico suele no ser tal a nivel de microescala, donde el entorno de un punto resulta ser una región que incluye distintos elementos constituyentes con diferentes propiedades y formas.
Es decir, el elemento material infinitesimal tiene su propia complejidad bajo la forma de una microestructura no homogénea que, además, evoluciona según la deformación a la que se somete.
Por lo tanto, los campos de tensión y deformación dentro del elemento material son igualmente no uniformes a escala microscópica \cite{NematNasserMicromechanics}.
El planteo de un marco teórico en más de una escala surge debido a la necesidad de modelar con precisión materiales que naturalmente presentan esta separación de escalas, siendo ejemplos típicos: las aleaciones metálicas, combinaciones de polímeros, materiales compuestos, medios porosos, materiales policristalinos y materiales biológicos.
Para estos casos, la respuesta macroscópica depende del tamaño, forma, propiedades y distribución espacial de sus constituyentes microestructurales \cite{Kouznetsova2002}.

La teoría multiescala tiene sus inicios en los trabajos pioneros de Hill \cite{Hill1963,Hill1965a,Hill1965b,Hill1972}, Hashin y Shtrikman \cite{HashinShtrikman1963}, Budiansky \cite{Budiansky1965} y Mandel \cite{Mandel1972}, entre otros.
Mayormente se ha enfocado la atención en el desarrollo de modelos basados en el concepto de Elemento de Volumen Representativo (\textit{Representative Volume Element}, RVE): a cada punto material de la macroescala se lo asocia con un dominio de la microescala apropiadamente identificado, que representa la configuración de referencia de la microestructura en ese punto.
Es importante que la longitud característica de la microescala sea considerablemente menor a la longitud característica de la macroescala, y que el tamaño del dominio microscópico sea lo suficientemente grande para ser verdaderamente representativo de la microestructura (figura \ref{fig:RVE_small}).
Dadas estas condiciones, existe separación de escalas y el dominio asociado a la microescala es considerado estadísticamente representativo del material en el punto material macroscópico asociado, y en consecuencia se lo denomina RVE.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.5\linewidth]{../Imagenes/02-Modelo/RVE_small}
	\caption[Esquema de una microestructura y RVEs de distinto tamaño]{Esquema de una microestructura y dominios microscópicos de distinto tamaño. Se ve claramente que a medida que se aumenta el tamaño, el dominio encuadrado contiene en su interior mayor número de constituyentes haciéndolo más representativo de la microestructura.}
	\label{fig:RVE_small}
\end{figure}

Esta disociación de un punto material macroscópico de su entorno, detallado en el RVE, requiere de dos operaciones para completar el sistema: la inserción y la homogeneización.
La inserción se refiere a la comunicación de la deformación macroscópica sobre el dominio microscópico, derivando generalmente en un problema de valores de contorno donde se suele optar por condiciones de borde homogéneas o periódicas, aunque otras alternativas son posibles.
Inversamente, la homogeneización es la obtención de la tensión macroscópica a partir del campo de tensiones en el RVE, siendo lo más usual considerar el promedio de las tensiones en el volumen microscópico (figura \ref{fig:Multiescala_esquema_concepto_RVE}).

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.9\linewidth]{../Imagenes/02-Modelo/Multiescala_esquema_concepto_RVE.pdf}
	\caption[Modelado constitutivo convencional y multiescala]{Esquema de los dos tipos de modelado constitutivo: convencional y multiescala. El modelado constitutivo convencional plantea un sola escala en la cual se establece una relación entre la tensión y la deformación en la forma de una ecuación matemática ($\P(\mathbf{F})$). El modelado multiescala, en cambio, asocia a cada punto material $\X$ un dominio microscópico que se resuelve simultáneamente a partir de la inserción de información desde la escala macroscópica, y como resultado se obtiene, mediante alguna técnica de homogeneización, la tensión aparente desde la macroescala.}
	\label{fig:Multiescala_esquema_concepto_RVE}
\end{figure}

En las últimas décadas, la utilización de modelos multiescala en aplicaciones prácticas se ha basado casi exclusivamente en técnicas de homogeneización computacional \cite{Kouznetsova2004,Michel1999,Miehe1999,TeradaKikuchi2001, Suquet1985,Guedes1990,TeradaKikuchi1995,Ghosh1995,Ghosh1996,Smit1999,Feyel2000,Kouznetsova2001}.
Esta técnica no busca la obtención de leyes constitutivas bajo la forma de un sistema cerrado de ecuaciones matemáticas, sino que promueve un análisis local-global en el que la tensión en cada punto de integración del modelo macroscópico se deriva de la resolución del problema microscópico en un RVE suficientemente detallado.
Esta metodología presentan una serie de ventajas:
\begin{itemize}
	\item No requieren de ningún tipo de suposición constitutiva a nivel macroscópico.
	\item Permiten la incorporación de grandes deformaciones y rotaciones tanto a nivel microscópico como macroscópico.
	\item Habilitan la posibilidad de incorporar al análisis macroscópico información detallada de la microestructura y su evolución geométrica y física durante la deformación macroscópica.
	\item Permiten la utilización de diferentes técnicas de modelado para cada escala, pudiendo combinar, por ejemplo, un modelo de continuo a escala macroscópica con un modelo microscópico de componentes discretos.
\end{itemize}

Consistentemente con las consideraciones previas, se plantea la base general del modelo multiescala para mallas electrohiladas.
El punto de partida es considerar el ensamble de nanofibras como una estructura con comportamiento mecánico regular, capaz de ser descripto en términos de los aspectos esenciales de su microestructura.
El modelo consiste de dos escalas: la escala microscópica que provee una representación de la microestructura y la escala macroscópica que representa el material homogeneizado con las propiedades emergentes de la malla electrohilada como un todo.
Conjuntamente se presentan los operadores de inserción y homogeneización que permiten el acoplamiento entre escalas.
A continuación se implementa esta formulación en un RVE prototipo relativamente sencillo que sirve como ejemplo de aplicación de la teoría formulada y se valida mediante comparación con datos experimentales.
También se estudia el efecto de las propiedades microestructurales más relevantes sobre la respuesta macroscópica observada.
Finalmente se ajustan los parámetros microscópicos para el diseño virtual de una malla electrohilada biomimética de una arteria intracranial humana.

