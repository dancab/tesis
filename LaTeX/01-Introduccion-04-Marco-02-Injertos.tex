Como se mencionó, un \textit{bypass} coronario requiere de un injerto vascular.
Para este procedimiento se emplean injertos a partir  de venas o arterias autólogas, homólogas y heterólogas, e injertos sintéticos de similares dimensiones.
De estos injertos, los autólogos siguen siendo hoy en día el \textit{gold standard}, tratándose de vasos sanguíneos del propio paciente que son extraídos de zonas no comprometidas.
En particular, la vena safena mayor y la arteria torácica interna (también denominada arteria mamaria interna) son los injertos que han presentado mejores resultados en el tiempo.
La vena safena mayor presenta una tasa de permeabilidad (grado de apertura) del 80-90\% luego de un año de implantada.
No obstante, a largo tiempo es propensa a sufrir el desarrollo de aterosclerosis y el 50\% de los injertos se cierran luego de 10 años de la operación. 
La arteria torácica interna presenta una tasa de permeabilidad de 90\% a los 10 años después de la operación. 
Las causas de este mejor rendimiento no son completamente conocidas. 
Sin embargo, la vena safena sigue siendo el injerto más elegido por los cirujanos. 
En los casos que la vena safena y la arteria torácica interna no se encuentran disponibles para su utilización como injertos, otros vasos sanguíneos tales como la arteria radial, la arteria gastro-omental derecha, la arteria epigástrica inferior, la vena safena corta y venas de las extremidades superiores, son utilizados \cite{Canver1995}.

Si bien los injertos autólogos presentan un buen desempeño, estos resultan inadecuados o inaccesibles en aproximadamente un tercio de los pacientes, requiriendo la utilización de alternativas como, por ejemplo, el uso de materiales sintéticos \cite{Kannan2005,Owens2009}.
Los injertos sintéticos aprobados para su uso en \textit{bypass} coronario son bioestables y presentan una alta rigidez, siendo los más comunes el {Dacron\textregistered} y el {Goretex\textregistered} (figura \ref{fig:dacron_goretex}).
Las tubuladuras de PET (polietileno tereftalato), patentadas como {Dacron\textregistered}, se forman mediante el tejido de múltiples filamentos de poliéster y poseen alta cristalinidad, alto módulo elástico y alta resistencia a la tracción.
Estos injertos se utilizan con éxito para \textit{bypass} de aorta y en la revascularización de injertos de gran diámetro periféricos.
Por otra parte, los injertos de ePTFE (politetrafluoroetileno expandido), conocidos comercialmente como {Goretex\textregistered} también presentan alta cristalinidad y alta rigidez, aunque menor que la del {Dacron\textregistered}.
El {Goretex\textregistered} se usa con excelentes resultados en injertos para las extremidades inferiores de diámetros internos entre \rango{7}{9}{mm}.
Respecto del desempeño de estos injertos como reemplazos para \textit{bypass} coronario, el injerto de ePTFE posee una tasa de permeabilidad media del 14\% a 45 meses de la operación y el de PET resulta en vasos abiertos luego de 17 meses de implantados, no habiendo resultados reportados a mayor tiempo de seguimiento. 

Sin embargo, tanto los injertos de {Dacron\textregistered} como de {Goretex\textregistered} fallan en la revascularización de arterias de pequeño diámetro ($<$\SI{6}{mm}) \cite{Abbott1993,Bennion1985}, debido principalmente a la trombogenicidad de la superficie sintética y al desacuerdo en las propiedades mecánicas entre injerto y tejido nativo en la zona de anastomosis\footnote{Anastomosis: zona de unión o costura entre dos vasos} \cite{Kannan2005,McBane2012,Hoenig2005}.
En el campo de la fisiología cardiovascular, la propiedad mecánica en juego se denomina \concepto{elastancia} o \concepto{compliancia} y cumple un rol fundamental ya que determina la respuesta en presión-diámetro a nivel de componente.
Es necesario, entonces, que la compliancia injerto pueda asemejar a la del vaso nativo.
Dado que los poliuretanos tienen una naturaleza más distensible y elastomérica que el PET y el ePTFE, son candidatos excelentes para reducir los problemas asociados a la rigidez de los injertos y el desacuerdo mecánico con los tejidos nativos \cite{Kannan2005,Ravi2010}.
Si bien se han reportado algunos resultados preliminares con injertos poliuretánicos, no existen datos de seguimiento a tiempo prolongados desde la implantación.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.7\linewidth]{../Imagenes/01-Tejidos/dacron_goretex}
	\caption[Injertos de {Dacron\textregistered} y {Goretex\textregistered}]{Fotografías de injertos vasculares sintéticos comerciales de a) {Dacron\textregistered} y b) {Goretex\textregistered}.}
	\label{fig:dacron_goretex}
\end{figure}

Ninguno de los injertos convencionales (sintéticos, autólogos, provenientes de un donante o de origen animal) ofrece potencial de regeneración y, más aún, todos están asociados con diferentes niveles de trombogenicidad, reestenosis y susceptibilidad a infecciones.
Además, las intervenciones quirúrgicas tratan solamente la manifestación de la aterosclerosis, sin tratar las causas de la condición, por lo que los síntomas tienden a reaparecer y los pacientes con frecuencia requieren una nueva intervención \cite{Kurobe2012,Seifu2013a}.

En este contexto entra la Ingeniería de Tejidos, entendida como el uso combinado de métodos de ingeniería y de las ciencias biológicas para el desarrollo de reemplazos que restauren, mantengan o mejoren la función de los tejidos vivos \cite{LangerVacanti1993}.
Un objetivo de la ingeniería de tejidos es el diseño de injertos vasculares vivos, capaces de responder a estímulos y con propiedades similares a los tejidos nativos \cite{Shinoka2001}.
En este sentido, el enfoque de la cirugía vascular emergente ha evolucionado de \squotes{reemplazar} a \squotes{regenerar} el tejido vascular.

Idealmente un injerto vascular debe carecer de características trombogénicas, tóxicas, cancerígenas e infecciosas, a la vez que debe poseer una adecuada compliancia, facilidad de manejo quirúrgico para su implantación , facilidad de sutura, facilidad de fabricación, disponibilidad en diversos tamaños, capacidad para liberar localmente agentes terapéuticos, capacidad funcional biológica y potencial de regeneración de tejidos. 
La relación estructura-función de los tejidos vasculares coronarios plantea un criterio de diseño exigente para los sustratos requeridos.
Por lo tanto, a la hora de diseñar un injerto vascular hay que tener en cuenta su estructura y funciones tanto a escala macroscópica como microscópica.

La ingeniería de tejidos vasculares plantea dos enfoques diferenciados: los injertos de láminas celulares autoensambladas y los injertos regenerados mediante el uso de andamios (\textit{scaffolds}). 

El enfoque de láminas celulares autoensambladas utiliza células humanas exclusivamente para fabricar vasos sanguíneos a partir de células autólogas sin el uso de sustratos externos al paciente.
En un biorreactor se cultivan células de músculo liso y fibroblastos \textit{in vitro} propias del paciente, luego se retiran de las placas y se ensamblan sobre un mandril del diámetro deseado, colocando primero células de músculo liso, siguiendo con fibroblastos y culminando con un recubrimiento de células endoteliales.
Estos injertos vasculares resisten presiones superiores a los \SI{2000}{mmHg}\footnote{Como referencia, la resistencia de la vena safena se estima en \SI{1700}{mmHg}.}, resistencia a la sutura adecuada y un endotelio funcional. Incluso estudios \textit{in vivo} demostraron que soportan las condiciones de flujo fisiológico \cite{Lheureux1998,Konig2009}.
Si bien los resultados obtenidos son exitosos, las láminas celulares requieren de un tiempo considerable para su fabricación, volviendo a este método poco prometedor para su traslado a la práctica clínica, en especial para los casos en que el injerto se necesita con cierta urgencia.
Más aún, existe la dificultad de obtener células funcionales capaces de regenerar el tejido vascular en un biorreactor, lo cual resulta una tarea dificultosa en pacientes de edad avanzada \cite{Villalona2010}.


Como alternativa, los injertos vasculares reconstruidos a partir de matrices soporte, o andamios, surgen como continuación natural en la investigación de injertos vasculares sintéticos, donde se adiciona el procedimiento \textit{in vitro} de infiltración y maduración de células vasculares previamente a la implantación.
Un andamio es una estructura tridimensional que provee el soporte mecánico y un ambiente propicio para el desarrollo y crecimiento de un tejido, al tiempo que facilita las funciones celulares como adhesión, diferenciación, migración, proliferación y secreción de los componentes de la matriz extracelular \cite{Kim1998,Zhang2007}.
Para que la infiltración y maduración celular sea exitosa, los andamios deben poseer estructuras altamente porosas e interconectadas, de modo de permitir el transporte de sustancias así como la migración celular \cite{Berglund2003,Vats2003}.
Además, para lograr la proliferación de tejido celular se utilizan biorreactores que simulen condiciones hemodinámicas fisiológicas, incluyendo las señales bioquímicas y biomecánicas que regulen correctamente el desarrollo tisular \cite{Martin2004}. 

Las primeras alternativas para la obtención de injertos mediante esta metodología se basaron en andamios constituidos de materiales naturales propios de los tejidos vasculares como el colágeno y presembrados con células vasculares. 
Sin embargo presentaron propiedades mecánicas inferiores a las requeridas y, contrariamente al objetivo inicial, necesitaron de refuerzos sintéticos para su uso clínico, generando nuevas complicaciones por rechazo o desacuerdo en compliancia \cite{Weinberg1986,Yao2005,Swartz2005}.
La utilización de biorreactores para favorecer la regeneración del tejido vascular con el soporte mecánico del andamio mejora sustancialmente las propiedades mecánicas de estos injertos, logrando pruebas con cierto grado de éxito en animales \cite{Peng2011}.
Otra opción es la utilización de andamios sintéticos, siendo los más empleados los  poliésteres biodegradables compuestos de glicolida y lactida, sus copolímeros (como el ácido poli-L-láctido, el ácido poliglicólico y la policaprolaptona) y los poliuretanos \cite{Tiwari2002}.
En comparación con los materiales naturales, los sintéticos presentan grandes ventajas respecto de su amplia disponibilidad y bajo costo, además de permitir mayor control en etapa de producción tanto a nivel de propiedades mecánicas como de porosidad y microestructura, lo que los vuelve candidatos muy atractivos para su fabricación para uso clínico \cite{Kim1998,Zhang2007,Isenberg2006}.
Como desventaja, los materiales sintéticos sufren de una baja bioactividad respecto de favorecer la implantación y proliferación natural, además de la importancia de tomar en consideración el efecto de la degradación del andamio sobre el tejido celular \cite{Yow2006,Higgins2003}.
Por otro lado, se exploró el uso de andamios a partir de matrices descelularizadas provenientes de donantes o animales (figura \ref{fig:fabricacion_de_injertos_descelularizados}).
Se trata de tejidos naturales a los que se los despoja, mediante el uso de diferentes técnicas, de las células pero manteniendo las proteínas de la matriz estructural, como colágeno y elastina, que son predominantemente no inmunogénicas.
De esta manera se obtiene un andamio que permitiría ser recelularizado con células del paciente reduciendo drásticamente el riesgo de rechazo inmune, pero aún así existe el riesgo de transmitir patógenos del animal o donante al paciente. 
Además, las propiedades biomecánicas de la matriz son variables debido a la variabilidad de especies donantes, así como también las distintas edades o sexo de la misma, reduciendo la repetibilidad en la producción con lo cual se ve afectado el resultado clínico \cite{Seifu2013a,Bouten2011}.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.9\linewidth]{../Imagenes/01-Tejidos/fabricacion_de_injertos_descelularizados}
	\caption[Método de fabricación de injerto diseñado a partir de un andamio descelularizado]{Método de obtención de un injerto diseñado por ingeniería de tejidos a partir de un andamio descelularizado. Se obtienen células vasculares del paciente y se cultivan \textit{in vitro}. Al mismo tiempo se extrae una arteria de una fuente alogénica, heterogénica o xenogénica, y se la descelulariza dejando solamente la matriz soporte. Luego se recelulariza el andamio con las células del paciente y se lo madura en un biorreactor, obteniendo un injerto vivo funcional. Imagen adaptada de \cite{Seifu2013a}.}
	\label{fig:fabricacion_de_injertos_descelularizados}
\end{figure}

En todo caso, si bien este tipo de injertos ha mostrado resultados interesantes y alentadores, poseen la importante limitación de necesitar un tiempo de cultivo \textit{in vitro} previo a la implantación, y más aún, existe la dificultad de obtener células funcionales capaces de regenerar el tejido vascular en un biorreactor, lo cual resulta una tarea difícil en pacientes de edad avanzada \cite{Villalona2010}.
Hasta la fecha tanto las alternativas sintéticas como las diseñadas por ingeniería de tejidos no consiguen igualar ni mejorar a los injertos autólogos para la cirugía vascular de pequeños vasos.
En los últimos 30 años poco ha cambiado en relación a los injertos sintéticos, y la obtención de un injerto vascular de pequeño diámetro ($<\SI{6}{mm}$) con un comportamiento apropiado y permanente aún representa un gran desafío. 
El desarrollo de un conducto para operaciones de \textit{bypass} coronario que pueda estar inmediatamente disponible, sin tiempos prolongados de cultivos \textit{in vitro}, con las propiedades mecánicas y biológicas necesarias es el centro de las investigaciones actuales en el campo de la ingeniería de tejidos vasculares.