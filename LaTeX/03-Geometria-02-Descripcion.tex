En el capítulo \ref{ch:introduccion} se mostró que una matriz electrohilada puede idealizarse como una superposición de capas bidimensionales (figura \ref{fig:comparacion_microestructura_electrohilado_arteria}), donde cada capa se conforma de una malla interconectada de fibras largas y posiblemente enruladas, que adicionalmente se conectan a las fibras de capas adyacentes en los puntos de intersección.
La caracterización topológica se realiza experimentalmente mediante imágenes SEM de matrices electrohiladas (figura \ref{fig:sem_fibras_medidas}) de donde se puede estimar el diámetro, curvatura y orientación de las fibras.

\begin{figure}[ht] 
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=0.9\linewidth]{../Imagenes/03-Geometria/sem_fibras_medidas_01.png} 
		\subcaption{}\label{fig:sem_fibras_medidas_01}
		%\vspace{4ex}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=0.9\linewidth]{../Imagenes/03-Geometria/sem_fibras_medidas_02.png} 
		\subcaption{}\label{fig:sem_fibras_medidas_02}
	\end{subfigure}%%
	\caption[Medición de orientaciones y enrulamientos de fibras a partir de una imagen SEM.]{Medición de orientaciones y enrulamientos de fibras a partir de una imagen SEM. Para llevarlo a cabo se seleccionan de la imagen fibras de manera aleatoria y se traza su linea de contorno abarcando la mayor longitud posible que la imagen permita sin perder la identificación de la fibra. Luego se traza la linea de extremo-extremo para poder medir su orientación y el valor de elongación de reclutamiento.}
	\label{fig:sem_fibras_medidas} 
\end{figure}

Si se realiza la medición de cada una de estas propiedades sobre un número suficientemente elevado de fibras, entonces se puede considerar que se trata de una muestra representativa de toda la población.
De esta forma puede calcularse un valor medio y dispersión estándar para la distribución de orientaciones obtenida ($\fdoom$ y $\fdoos$, respectivamente), así como un valor medio y dispersión estándar para la distribución de reclutamiento ($\fdrom$ y $\fdros$, respectivamente)\footnote{En el capítulo \ref{cap:modelo_multiescala} se presentó un modelo de haz de fibras donde se prescriben una distribución de enrulamientos $\fdrp$ en base a parámetros de valor medio $\fdrpm$ y dispersión $\fdrps$. Aquí, en cambio, se trata con las variables $\fdrom$ y $\fdros$, correspondientes a una distribución de enrulamientos obtenida a partir de mediciones experimentales.}.
Otra opción más detallada consiste en graficar el histograma normalizado correspondiente para visualizar la distribución discreta que caracteriza la geometría de la malla.

\begin{figure}[ht] 
	\centering
	\includegraphics[width=0.95\linewidth]{../Imagenes/03-Geometria/sem_fdo_ejemplo.png} 
	\caption[Distribución de orientaciones para una malla de fibras isotrópica y otra moderadamente alineada.]{Imágenes SEM de matrices electrohiladas y sus respectivos histogramas de orientaciones. a) Imagen SEM de una matriz isotrópica y b) su histograma de orientaciones. c) Imagen SEM de una matriz moderadamente alineada en la dirección vertical (se indica en la figura) y d) su histograma junto con una regresión gaussiana (línea roja). Imagen adaptada de \cite{Montini2015phdthesis}}
	\label{fig:sem_fdo_ejemplo} 
\end{figure}

Sin embargo, es de importancia para la caracterización topológica de una malla fibrosa, disponer de una noción de interconectividad, ya que el comportamiento final va a depender no sólo de la suma de las nanofibras, sino también de la interacción entre las mismas.
Se adopta entonces una medida usual: la densidad superficial de intersecciones $\denin$ definida como la cantidad de intersecciones por unidad de superficie en una capa de fibras.
No obstante, la información que puede extraerse a partir de microscopía es limitada ya que está restringida a la superficie exterior de la matriz y se presenta como una proyección bidimensional donde resulta difícil distinguir entre intersecciones aparentes e intersecciones reales donde las fibras se encuentran unidas entre sí.
En este punto resulta de utilidad el modelo geométrico propuesto en esta sección, ya que permite estimar las intersecciones reales entre las fibras, considerando como puntos de unión donde se cruzan fibras que se encuentran en una misma capa o de capas adyacentes entre sí.
