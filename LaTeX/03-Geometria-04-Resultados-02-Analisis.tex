El algoritmo de deposición virtual descripto puede generar mallas con un diverso rango de geometrías, permitiendo controlar la fracción de volumen, el grado de alineación de las fibras, su enrulamiento y diámetro.
También el proceso de electrohilado permite, a pesar de las limitaciones inherentes a la práctica experimental, ejercer cierto grado de control sobre algunas de estas propiedades y obtener gran diversidad en la topología microscópica.
En esta sección se estudia la microestructura obtenida y se exploran las posibilidades del algoritmo de generar distintos tipos de geometrías capaces de ser utilizadas como RVEs para modelos multiescala de matrices electrohiladas.

\subsubsection{Alineamiento de la malla}

Un ejemplo es la posibilidad de controlar la velocidad de rotación del mandril colector para conseguir microestructuras con diferente nivel de alineamiento de las fibras a lo largo de la dirección circunferencial.
A su vez, en el algoritmo de deposición virtual, para generar fibras con diferente grado de alineamiento a lo largo de una dirección se puede controlar la FDO prescripta.
La figura \ref{fig:analisis_alineacion} muestra mallas ($\fv=0.3$, $\Dm=\SI{1}{\micro\meter}$, $\adLcapa=50$ y $N_c=10$, $\devangmax=\ang{5}$) con diferente grado de alineación graficadas junto con su respectiva distribución de orientaciones, donde puede observarse la capacidad del algoritmo de reproducir geometrías con creciente nivel de alineamiento.

\newcommand{\aux}{0.3}
\begin{figure}[p] 
	\centering
	\begin{subfigure}[b]{\aux\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/analisis_alineacion_uniforme_malla.pdf} 
		\subcaption{}\label{fig:analisis_alineacion_uniforme_malla}
		%\vspace{4ex}
	\end{subfigure}
	\begin{subfigure}[b]{\aux\linewidth}
	\centering
	\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/analisis_alineacion_moderada_malla.pdf} 
	\subcaption{}\label{fig:analisis_alineacion_moderada_malla}
	%\vspace{4ex}
	\end{subfigure}
	\begin{subfigure}[b]{\aux\linewidth}
	\centering
	\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/analisis_alineacion_alta_malla.pdf} 
	\subcaption{}\label{fig:analisis_alineacion_alta_malla}
	%\vspace{4ex}
	\end{subfigure}

	\begin{subfigure}[b]{\aux\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/analisis_alineacion_uniforme_distribucion.pdf} 
		\subcaption{}\label{fig:analisis_alineacion_uniforme_distribucion}
	\end{subfigure}%%
	\begin{subfigure}[b]{\aux\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/analisis_alineacion_moderada_distribucion.pdf} 
		\subcaption{}\label{fig:analisis_alineacion_moderada_distribucion}
	\end{subfigure}%%
	\begin{subfigure}[b]{\aux\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/analisis_alineacion_alta_distribucion.pdf} 
		\subcaption{}\label{fig:analisis_alineacion_alta_distribucion}
	\end{subfigure}%%
	
	\caption[Diseño de geometrías con creciente nivel de alineamiento a lo largo de una dirección.]{Diseño de geometrías con creciente nivel de alineamiento a lo largo de una dirección. a), b) y c) Mallas generadas con $\fv=0.3$, $N_c=10$, $\Dm=\SI{1}{\micro\meter}$, $\adLcapa=50$, $\adls=1$, $\devangmax=\ang{5}$. Se muestran solo dos capas para una mejor visualización. a) FDO uniforme. b) Alineamiento moderado: FDO normal truncada con $\fdops=\pi/5$. c) Alineamiento alto: FDO normal truncada con $\fdops=\pi/10$. b), d) y e) muestran las correspondientes distribuciones de orientaciones obtenidas (histogramas normalizados, gris) junto con la FDO prescripta (línea sólida, rojo).}
	\label{fig:analisis_alineacion} 
\end{figure}

\subsubsection{Distribución de enrulamiento}

Asimismo, es posible obtener geometrías virtuales con fibras más rectas o más enruladas controlando el ángulo de desviación máxima con la que se concatenan los segmentos lineales $\devangmax$ y la longitud de los segmentos $\ls$ (figura \ref{fig:analisis_reclutamiento}).
En todos los casos, la tendencia de la distribución es decreciente, indicando que la mayoría de las fibras posee valores de $\lamer$ pequeños (cercanos a \num{1}).
Aún así, con ángulos $\devangmax$ más grandes se consiguen distribuciones con valores de reclutamiento más elevados en promedio, sin afectar la tendencia decreciente.

Es posible diferenciar entre la población de fibras largas (tal cual son depositadas) y la población de fibras cortas (subdivididas en los puntos de unión).
En el segundo caso, aunque se mantiene la misma tendencia en la distribución de $\lamer$, los valores son marcadamente menores en general.
Esto se debe a que las intersecciones provocan la división de una fibra enrulada en varias fibras más rectas.
Este resultado es muy interesante si se tiene en cuenta la posible importancia de la distribución de reclutamiento en el comportamiento no lineal de las matrices nanofibrosas como se determinó en el capítulo \ref{cap:modelo_multiescala}, ya que una mayor densidad de intersecciones afectaría a la distribución de reclutamiento y, consecuentemente, a la no linealidad de la respuesta mecánica.


\renewcommand{\aux}{0.3}
\begin{figure}[p] 
	\centering
	\begin{subfigure}[b]{\aux\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/analisis_reclutamiento_devangmax_5_malla.pdf} 
		\subcaption{}\label{fig:analisis_reclutamiento_devangmax_5_malla}
		%\vspace{4ex}
	\end{subfigure}
	\begin{subfigure}[b]{\aux\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/analisis_reclutamiento_devangmax_10_malla.pdf} 
		\subcaption{}\label{fig:analisis_reclutamiento_devangmax_10_malla}
		%\vspace{4ex}
	\end{subfigure}
	\begin{subfigure}[b]{\aux\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/analisis_reclutamiento_devangmax_20_malla.pdf} 
		\subcaption{}\label{fig:analisis_reclutamiento_devangmax_20_malla}
		%\vspace{4ex}
	\end{subfigure}
	
	\begin{subfigure}[b]{\aux\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/analisis_reclutamiento_devangmax_5_distribucion.pdf} 
		\subcaption{}\label{fig:analisis_reclutamiento_devangmax_5_distribucion}
	\end{subfigure}%%
	\begin{subfigure}[b]{\aux\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/analisis_reclutamiento_devangmax_10_distribucion.pdf} 
		\subcaption{}\label{fig:analisis_reclutamiento_devangmax_10_distribucion}
	\end{subfigure}%%
	\begin{subfigure}[b]{\aux\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/analisis_reclutamiento_devangmax_20_distribucion.pdf} 
		\subcaption{}\label{fig:analisis_reclutamiento_devangmax_20_distribucion}
	\end{subfigure}%%

	\begin{subfigure}[b]{\aux\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/analisis_reclutamiento_devangmax_5_distribucion_i.pdf} 
		\subcaption{}\label{fig:analisis_reclutamiento_devangmax_5_distribucion_i}
	\end{subfigure}%%
	\begin{subfigure}[b]{\aux\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/analisis_reclutamiento_devangmax_10_distribucion_i.pdf} 
		\subcaption{}\label{fig:analisis_reclutamiento_devangmax_10_distribucion_i}
	\end{subfigure}%%
	\begin{subfigure}[b]{\aux\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/analisis_reclutamiento_devangmax_20_distribucion_i.pdf} 
		\subcaption{}\label{fig:analisis_reclutamiento_devangmax_20_distribucion_i}
\end{subfigure}%%
	
	\caption[Diseño de geometrías con creciente grado de enrulamiento.]{Diseño de geometrías con creciente nivel de enrulamiento. a), b) y c) Mallas generadas con $\fv=0.3$, $N_c=10$, $\Dm=\SI{1}{\micro\meter}$, $\adLcapa=50$, $\adls=1$ y $\fdop(\theta) = \pi^{-1}$. Se muestran solo dos capas para mejor visualización. a) $\devangmax=\ang{5}$. b) $\devangmax=\ang{10}$. c) $\devangmax=\ang{20}$. d), e) y f) Distribuciones de reclutamiento correspondientes a cada malla, midiendo los valores de reclutamiento para cada fibra larga tal como fue depositada, sin subdividir las fibras en las intersecciones. g), h) y i) Distribuciones de reclutamiento correspondientes a cada malla, midiendo los valores de reclutamiento para cada fibra comprendida entre dos puntos unión.}
	\label{fig:analisis_reclutamiento} 
\end{figure}

Para verificar la posibilidad de generar geometrías que asemejen a las reales para matrices electrohiladas, se compara la distribución de reclutamiento extraída a partir de análisis óptico de imágenes SEM (figura \ref{fig:sem_fibras_medidas}) con la distribución obtenida para una malla virtual generada con $\Dm=\SI{1}{\micro\meter}$, $\adLcapa=250$, $\ls=5$, $\devangmax=\ang{17}$ y $N_c=5$, obteniendo una buena concordancia entre los histogramas (figura \ref{fig:Analisis_reclutamiento_comparacion}).

\renewcommand{\aux}{0.45}
\begin{figure}[p] 
	\centering
	\begin{subfigure}[b]{\aux\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/Analisis_reclutamiento_comparacion_malla.pdf} 
		\subcaption{}\label{fig:Analisis_reclutamiento_comparacion_malla}
		%\vspace{4ex}
	\end{subfigure}
	\begin{subfigure}[b]{\aux\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/Analisis_reclutamiento_comparacion_histograma.pdf} 
		\subcaption{}\label{fig:Analisis_reclutamiento_comparacion_histograma}
		%\vspace{4ex}
	\end{subfigure}
	
	\caption[Comparación de la distribución de enrulamiento obtenida en mallas virtuales con datos experimentales.]{Comparación de la distribución de enrulamiento obtenida para mallas generadas mediante el algoritmo de deposición virtual con datos experimentales obtenidos a partir de análisis óptico de imágenes SEM.}
	\label{fig:Analisis_reclutamiento_comparacion} 
\end{figure}

\subsubsection{Densidad superficial de intersecciones}

Finalmente, mediante el parámetro de la fracción de volumen ($\fv$) también es posible controlar el número de fibras que conforma cada capa, dado que con un menor número de fibras por capa, se generan menos intersecciones entre fibras de una misma capa y de capas adyacentes.
La figura \ref{fig:analisis_denin_vs_volfrac} muestra resultados de $\denin$ versus $\fv$ para mallas generadas con $\Dm=\SI{1}{\micro\meter}$, $\adLcapa=50$, $\adls=1$, $\devangmax=0$ y $N_f\approx200$ ($N_c=10$ para $\fv=0.3$, $N_c=15$ para $\fv=0.2$ y $N_c=30$ para $\fv=0.1$). 
Se observa un aumento significativo de $\denin$ respecto de la fracción de volumen (\ref{fig:analisis_denin_vs_volfrac_errorbars}) al mismo tiempo que disminuye la variabilidad estadística (\ref{fig:analisis_denin_vs_volfrac_varcoeffs}).

\begin{figure}[p] 
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/analisis_denin_vs_volfrac_errorbars.pdf} 
		\subcaption{}\label{fig:analisis_denin_vs_volfrac_errorbars}
		%\vspace{4ex}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/analisis_denin_vs_volfrac_varcoeffs.pdf} 
		\subcaption{}\label{fig:analisis_denin_vs_volfrac_varcoeffs}
	\end{subfigure}%%
	
	\caption[Densidad superficial de intersecciones vs. fracción de volumen.]{Dependencia de la densidad superficial de intersecciones respecto de la fracción de volumen. a) Valor medio de $\denin$ en un conjunto de 10 mallas vs. $\fv$. b) Coeficiente de variación para $\denin$ versus $\fv$.}
	\label{fig:analisis_denin_vs_volfrac} 
\end{figure}

Luego, también es posible estudiar la influencia del grado de alineamiento sobre la densidad superficial de intersecciones.
A medida que se incrementa el alineamiento de la malla a lo largo de una dirección preferencial, se generan más fibras que son paralelas entre sí y que, por lo tanto, no generarán intersecciones.
La figura \ref{fig:analisis_denin_vs_alin} muestra esta disminución para mallas generadas con $\fv=0.3$, $\Dm=\SI{1}{\micro\meter}$, $\adLcapa=50$ y $N_c=10$, $\devangmax=\ang{5}$ y diferente grado de alineamiento (figura \ref{fig:analisis_alineacion}): isotrópica (FDO uniforme $\fdop(\theta)=\pi^{-1}$), moderada ($\fdop(\theta)$ normal truncada con $\fdops=\pi/5$) y alta ($\fdop(\theta)$ normal truncada con $\fdops=\pi/10$).

\begin{figure}[ht] 
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/analisis_denin_vs_alin_errorbars.pdf} 
		\subcaption{}\label{fig:analisis_denin_vs_alin_errorbars}
		%\vspace{4ex}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/analisis_denin_vs_alin_varcoeffs.pdf} 
		\subcaption{}\label{fig:analisis_denin_vs_alin_varcoeffs}
	\end{subfigure}%%
	
	\caption[Densidad superficial de intersecciones vs. alineamiento de la malla.]{Dependencia de la densidad superficial de intersecciones respecto del grado de alineamiento de la malla. a) Valor medio de $\denin$ en un conjunto de 10 mallas vs. alineamiento. b) Coeficiente de variación para $\denin$ vs. alineamiento.}
	\label{fig:analisis_denin_vs_alin} 
\end{figure}

Tanto la dependencia de la densidad superficial de intersecciones respecto de la fracción de volumen como del grado de alineamiento de la malla resulta de importancia a la hora del diseño de microestructuras con fines específicos, dado que se trata de un parámetro predominante para la respuesta mecánica de las matrices fibrosas.
