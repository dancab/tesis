\renewcommand{\labelitemi}{$\bullet$}
\renewcommand{\labelitemii}{--}

\section{Conclusiones y resultados obtenidos}

Esta tesis introduce métodos para el modelado mecánico multiescala de materiales con microestructuras nanofibrosas compuestas por capas planas superpuestas.
Estos métodos tienen aplicación inmediata en el campo de la ingeniería de tejidos, especialmente en el diseño de injertos nanofibrosos con pretensiones biomiméticas.
Se reseñan a continuación los principales resultados y conclusiones del trabajo:

\begin{itemize}
	\item Se presentó un modelo constitutivo multiescala para el comportamiento mecánico elástico de matrices nanofibrosas electrohiladas con el objetivo de servir como herramienta de diseño para injertos vasculares electrohilados de ingeniería de tejidos.
	La escala macroscópica se modeló como un sólido continuo mientras que en la escala microscópica se implementó un RVE discreto compuesto por celdas triangulares superpuestas, dando cuenta de una microestructura conformada por una superposición de capas planas nanofibrosas.
	Cada fibra se modeló en base a una ley elástica bilineal llevando en consideración la diferencia en comportamiento entre fibras enruladas y rectas bajo tracción, evitándose el uso de modelos micromecánicos complejos.
	Conjuntamente, se tomó en cuenta el fenómeno de \concepto{reclutamiento} de una fibra al incrementar su módulo elástico tangente a la tracción al devenir recta.
	Adicionalmente se admitió el agrupamiento de nanofibras individuales en \concepto{haces} o \concepto{fascículos} como el elemento mínimo constituyente del RVE, con la ventaja de lograr implementar de forma sencilla y efectiva la distribución estadística de enrulamientos de las fibras.
	Mediante este enfoque estadístico se logró evidenciar, además, el fenómeno de \concepto{reclutamiento progresivo} bajo tracción, que toma lugar cuando las nanofibras van deviniendo rectas de manera gradual por causa de sus distintos niveles de enrulamiento.
	Los principales resultados obtenidos son:
	\begin{itemize}
		\item Se validó el modelo mediante comparación con datos experimentales de ensayos mecánicos de inflado de tubos de PLLA electrohilado, consiguiendo un buen ajuste entre las curvas presión-diámetro experimentales y simuladas,
		\item Se reprodujo con éxito la curva presión-diámetro (incluyendo la forma \dquotes{J}) para matrices fibrosas a causa del reclutamiento progresivo de las nanofibras constituyentes.
		\item Se estudiaron los efectos de las variaciones microestructurales y constitutivas sobre la respuesta mecánica macroscópica, encontrando que el efecto de controlar la distribución de enrulamientos actúa de manera complementaria al de controlar la rigidez de las fibras mediante la selección de distintos polímeros de base. 
		Esto evidenció que la capacidad de ejercer un mayor control sobre la distribución de enrulamientos de los injertos vasculares electrohilados puede ser un factor crucial en el esfuerzo por conseguir reemplazos verdaderamente biomiméticos.
		\item Se optimizaron los parámetros del modelo para un injerto electrohilado capaz de imitar la respuesta mecánica en presión-deformación de una arteria intracranial humana en el rango de presiones fisiológicas. 
		Los resultados indicaron que tal reemplazo debería poseer un mayor valor medio y una mayor dispersión en la distribución de enrulamientos de las fibras.
	\end{itemize}

	\item Para mejorar el modelo microscópico anterior admitiendo cinemáticas más complejas y realistas, se puso en evidencia la necesidad de contar con geometrías que sean realmente representativas de la microestructura electrohilada.
	Para ello se desarrolló un \concepto{algoritmo de deposición virtual} capaz de generar geometrías virtuales que reproducen los aspectos más importantes de la microtopología de capas nanofibrosas típica de las matrices electrohiladas.
	El algoritmo se inspira en la deposición real de las fibras durante el proceso de electrohilado, buscando imitar este fenómeno desde una perspectiva de dominio microscópico, incluyendo la naturaleza estocástica de los caminos que forman las fibras durante su deposición.
	En consecuencia, se obtienen geometrías que comparten muchos aspectos de la microestructura electrohilada como: fibras de gran longitud y relación de aspecto, estructura de capas planas superpuestas, distribución de enrulamiento similar, uniones entre las fibras en los puntos de contacto y posibilidad de controlar el grado de alineamiento.
	Los principales resultados alcanzados son los siguientes:
	\begin{itemize}
		\item Para establecer la validez de las geometrías generadas como elementos de volumen representativos, se llevó a cabo un estudio de la variabilidad estadística en los parámetros de importancia de las mallas virtuales, encontrando el tamaño necesario para que el dominio microscópico pueda considerarse un RVE, es decir, que la variabilidad caiga por debajo de un umbral preestablecido.
		\item Se demostró la capacidad del algoritmo para obtener geometrías que reproduzcan la microestructura de mallas con distinta fracción de volumen, grado de alineamiento, fibras de diferente diámetro y de mayor o menor grado de enrulamiento. 
		Esta versatilidad permite reproducir una amplia gama de microestructuras fibrosas, cumpliendo con dos finalidades bien diferenciadas: por un lado generar RVE específicos para evaluar la respuesta de matrices electrohiladas con microestructuras conocidas, y por otro, realizar ensayos \textit{in silico} sobre la microestructura virtual que permitan guiar a los estudios experimentales en la búsqueda de la biomímesis, optimizando el número de casos a ensayar experimentalmente.
		\item Se realizó una estimación de la densidad superficial de intersecciones presente en las capas interiores de las matrices electrohiladas.
		Se analizó también su interdependencia respecto de los demás parámetros geométricos, encontrando que su valor disminuye con el alineamiento de la malla y se incrementa con la fracción de volumen.
		Este resultado es de particular interés dado que la densidad de intersecciones es una variable de difícil acceso experimental y, a la vez, de relevancia para el comportamiento mecánico global de la matriz.
	\end{itemize}

	\item En base a los RVE obtenidos mediante el algoritmo de deposición virtual, se desarrolló un modelo micromecánico para mallas de nanofibras interconectadas en puntos de unión o nodos.
	Para ello se realizó una descripción detallada de la cinemática de la malla y de las relaciones de equilibrio, además se extendió la ley constitutiva previa para contemplar la plasticidad de las fibras y la posibilidad de rotura al superar un valor de tensión límite.
	Como complemento, se llevaron a cabo ensayos de tracción uniaxial sobre probetas electrohiladas de PLLA para obtener curvas de tensión-deformación experimentales con las cuales validar el modelo.
	Los principales resultados son:
	\begin{itemize}
		\item Se comparó la respuesta en tensión homogeneizada frente a la tracción uniaxial con datos experimentales para matrices electrohiladas de PLLA,	obteniendo un buen acuerdo entre las curvas de tensión versus deformación. 
		El RVE simulado se generó con parámetros geométricos que imitaron la microestructura de las matrices ensayadas.
		Más aún, los módulos elásticos de las fibras se adoptaron a partir de valores reportados en la bibliografía y el resto de los parámetros, que fueron ajustados para reproducir las curvas experimentales, resultaron en valores adecuados, dentro de los rangos esperados según lo reportado por estudios experimentales.
		\item Se estudió la evolución de la microestructura y la plasticidad de las fibras a medida que se produce la deformación.
		Este análisis puso en evidencia la existencia de un subconjunto de fibras, alineadas con la dirección de tracción, que soportan casi en su totalidad a la carga externa.
		Además, se identificó que este subconjunto crece a medida que se incrementa la deformación, pudiendo explicitar un fenómeno de \concepto{alineamiento progresivo} de la malla.
	\end{itemize}
\end{itemize}

Como conclusión general, los métodos desarrollados permiten reproducir con alta fidelidad la microtopología nanofibrosa en su estado de deposición y simular adecuadamente la respuesta mecánica de la microestructura, mientras que se emplean parámetros geométricos y constitutivos realistas.
Además, la capacidad de controlar tanto la geometría del RVE como la respuesta constitutiva de las fibras, habilita el ensayo \textit{in silico} de matrices electrohiladas con microestructuras diseñadas a medida previamente a su fabricación.
Más aún, es posible acoplar el modelo micromecánico desarrollado en ciclos de optimización con el objetivo de producir una respuesta mecánica deseada, obteniendo los parámetros geométricos y/o constitutivos necesarios para procesar injertos electrohilados verdaderamente biomiméticos.

\section{Trabajos futuros}

Durante el desarrollo de este trabajo se han identificado varias posibles direcciones para el trabajo futuro:
\begin{itemize}
	\item La dirección más relevante para el trabajo futuro es incrementar la capacidad del modelo de reproducir con fidelidad tanto las microestructuras electrohiladas como sus respuestas micromecánicas.
	Si bien se hizo un avance importante y se alcanzaron resultados prometedores, el RVE propuesto permite su modificación para incorporar aspectos geométricos y constitutivos adicionales.
	Un punto de mejoría reside en la determinación de los puntos de unión entre las fibras, que admite diferentes hipótesis para su realización.
	Por ejemplo, sería posible admitir el contacto entre fibras de capas no adyacentes a partir de una función de probabilidad.
	Además, la inclusión de fenómenos de fricción o viscoelásticos permitiría simular de manera realista los ciclos de histéresis típicos de estos materiales.
	Estos fenómenos podrían implementarse tanto a nivel constitutivo de las fibras como a nivel de la interacción, considerando enlaces con resbalamiento con fricción, o incluso falla por rotura en las uniones.
	Para estos fines resulta necesario proveer al modelado de la información experimental relevante para su validación, lo que requiere un trabajo mancomunado entre simulaciones, caracterización morfológica y ensayos mecánicos.
	\item Una vez extendida la capacidad del modelo, sería factible avanzar hacia simulaciones que reproduzcan la respuesta mecánica bajo solicitaciones de índole cíclica, con el fin de simular con fidelidad el comportamiento en condiciones hemodinámicas.
	La incorporación de esta capacidad permitiría la realización de ensayos \textit{in silico} para evaluar la funcionalidad de diferentes materiales y microestructuras como injertos vasculares reales.
	\item En linea con lo anterior, aparece la posibilidad de un trabajo coordinado entre el modelado y la experimentación, para realizar ciclos de fabricación-ensayo-optimización utilizando el modelo propuesto como herramienta de diseño para determinar las propiedades microestructurales necesarias de los injertos.
	En el capítulo \ref{cap:modelo_multiescala} se mostró esta capacidad del modelo para el caso de un posible injerto de PCL electrohilado capaz de reemplazar una arteria intracranial humana.
	Esta metodología podría extenderse a diferentes tejidos arteriales, ya sea para distintos pacientes o distritos vasculares, empleando también un \textit{vademecum} de materiales preestablecido para seleccionar los parámetros constitutivos.
	Desde el punto de vista experimental sería necesario controlar la distribución de enrulamiento de las nanofibras, lo que podría conseguirse mediante la técnica novedosa de \textit{Melt Electrospinning Writing}.
\end{itemize}