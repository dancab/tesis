Para alcanzar las demandas mecánicas y funcionales necesarias para el éxito de un andamio para ingeniería de tejidos, se necesitan modelos y simulaciones que otorguen un mayor entendimiento de los procesos microestructurales que ocurren durante la deformación y su relación con la respuesta macroscópica observada.
El modelado constitutivo de un material es el proceso de análisis mediante el cual se establece un modelo que representa algunos, sino todos, los aspectos de importancia involucrados, mientras que la simulación es el proceso que utiliza el modelo establecido para determinar la respuesta del material bajo condiciones específicas de carga y/o deformación.

En la realidad, los materiales presentan naturalmente una gran interacción multiescala, desde el comportamiento atómico y molecular, pasando por la microestructura, hasta lo macroscópicamente observado.
El modelado convencional restringe su alcance y validez a una sola escala, sin considerar los fenómenos que ocurren por fuera de la misma, excepto sólo por sus efectos observables en el nivel estudiado.
Si bien enfocarse en una sola escala simplifica el proceso de modelado, el advenimiento de la nanotecnología permite la fabricación de materiales con la posibilidad de controlar las características microscópicas.
Por lo tanto, si se desea optimizar la respuesta macroscópica del material mediante la modificación de su microestructura, se requieren modelos que lleven en consideración el acoplamiento entre las observaciones macroscópicas y los fenómenos microscópicos subyacentes con el fin de lograr un mayor entendimiento de los mecanismos microscópicos subyacentes y su acoplamiento con la escala macroscópica.

Los modelos multiescala surgen dada la necesidad de modelar simultáneamente aspectos estructurales en dos escalas bien diferenciadas\footnote{En rigor puede tratarse más de dos escalas, en cuyo caso el concepto no presenta mayores diferencias, por lo que se mantuvo el caso de dos escalas para dar mayor claridad a la explicación.}.
El modelado convencional implicaría el planteo de la escala macroscópica incorporando los aspectos microestructurales únicamente por sus efectos fenomenológicos mediante una ecuación constitutiva, se trata de un modelo eficiente pero se pierde información relevante sobre el comportamiento mecánico de la microestructura.
Otra opción es plantear un modelo en escala única cuya resolución permita considerar los aspectos microestructurales al mismo tiempo que la geometría macroscópica, pero se trataría de un problema con un altísimo costo computacional para su implementación.
En cambio, el modelado multiescala implica el planteo de diferentes modelos para cada escala de manera simultánea con algún método de acoplamiento entre ellos, consiguiendo un equilibrio razonable entre eficiencia y resolución.
Los modelos de las distintas escalas pueden originarse de leyes de comportamiento bien diferentes.
Mientras que la macroescala suele plantearse como un sólido continuo, la microestructura puede representarse mediante modelos continuos, discretos, estadísticos, de dinámica molecular, entre otros \cite{MultiscaleModeling}. 

El modelado multiescala plantea, por lo tanto, que el modelado constitutivo de un material puede plantearse como una jerarquía de modelos de simple escala con diferentes grados de complejidad y acoplados entre sí.
Para poder llevar a cabo esta técnica se debe cumplir con la separación de escalas, es decir, que las longitudes características típicas de la microescala sean órdenes de magnitud menores que las longitudes características propias de la macroescala.
Bajo esta consideración, siempre será posible encontrar una muestra representativa microscópica del material sobre la cual se puedan realizar y calcular propiedades que resulten estadísticamente representativas del comportamiento del material macroscópico.
A una muestra con esas características se la denomina Elemento de Volumen Representativo (\textit{Representative Volume Element}, RVE).
Dicho de otra manera, un RVE asociado a un punto material macroscópico de un cuerpo es un volumen material estadísticamente representativo del entorno infinitesimal (desde el punto de vista macroscópico) de ese punto material.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.7\linewidth]{../Imagenes/01-Tejidos/Multiescala_esquema_concepto_RVE_2}
	\caption[Concepto de muestra representativa]{Ejemplo de la idea de un tamaño de muestra estadísticamente representativo del material. Las tres muestras microscópicas son diferentes en un sentido estricto, pero no lo son estadísticamente hablando: las distribuciones de probabilidad de diámetro, orientación, enrulamiento son similares, así como la porosidad y otros parámetros que se puedan medir.}
	\label{fig:Multiescala_esquema_concepto_RVE_2}
\end{figure}
