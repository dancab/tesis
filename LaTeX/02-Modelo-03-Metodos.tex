Para validar el modelo propuesto se realizó una comparación con datos experimentales de ensayos de inflado donde un injerto vascular es sometido a presión interior monótonamente creciente a medida que se registra el cambio en diámetro.
Los datos experimentales se tomaron de un trabajo externo reportado por Suarez Bagnasco et. al. \cite{SuaresBagnasco2014}. 
A continuación se realiza una breve descripción de los materiales y métodos allí reportados.

\subsection{Materiales}

Los injertos ensayados son tubuladuras  de ácido poli-L-láctico (PLLA)\footnote{PLA2002D,$Mn = \SI{78.02}{\kg\per\mol}$, Natureworks MN, USA} de pequeño diámetro producidos mediante la técnica de electrohilado por Montini Ballarin et. al. \cite{MontiniBallarin2014}.
Los parámetros del proceso son: concentración de solución $\SI{10}{\% wt/V}$, caudal $\SI{0.5}{\milli\liter\per\hour}$, distancia aguja-colector $\SI{15}{\centi\meter}$, diferencia de potencial aplicada $\SI{13}{\kilo\volt}$ y velocidad de rotación de colector $\SI{1000}{rpm}$.
El equipo utilizado consiste de una fuente de alto voltaje\footnote{Gamma High Voltage Research Inc., Ormond Beach, Florida, USA}, una aguja de acero inoxidable de punta roma\footnote{18 gauge, Aldrich®}, una bomba de infusión a jeringa\footnote{Activa® A22 ADOX, Ituzaingó, Argentina} y un colector cilíndrico rotativo de acero inoxidable de 5mm de diámetro. 
Para cada tubuladura, el proceso de electrohilado se llevó a cabo durante \SI{2}{\hour}, cambiando la posición de la boquilla cada \SI{15}{\minute} para lograr un espesor uniforme según el largo del injerto.
Las tubuladuras obtenidas son de \num{8} a \SI{11}{\centi\meter} de largo y espesores alrededor de \SI{0.04}{\milli\meter}.
Dado el reducido espesor respecto del diámetro del colector, el largo de los especímenes no es una variable de influencia en el estado tensional de las muestras durante los ensayos de inflado.
Adicionalmente, se llevaron a cabo micrografías SEM de las paredes interior y exterior de los injertos (figura \ref{fig:microstructure_graft}) y se midió el diámetro medio de las fibras en ambas superficies, reportando \SI{0.48}{\micro\meter} ($\pm$ \SI{0.14}{\micro\meter}) y \SI{0.36}{\micro\meter} ($\pm$ \SI{0.07}{\micro\meter}) respectivamente.
Estos valores, si bien presentan una variación, son cercanos entre sí y se puede observar en las micrografías SEM microestructuras similares.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.7\linewidth]{../Imagenes/02-Modelo/microstructure_graft}
	\caption[Micrografías SEM de las tubuladuras de PLLA.]{Imágenes SEM de las microestructuras nanofibrosas de las paredes interior y exterior de un injerto tubular de PLLA electrohilado.}
	\label{fig:microstructure_graft}
\end{figure}

\subsection{Ensayos de inflado}

Los datos experimentales de los ensayos de inflado se toman de lo reportado por Suares Bagnasco y colaboradores \cite{SuaresBagnasco2014}
Los ensayos mecánicos se llevaron a cabo en un banco de prueba de simulación hemodinámica diseñado para medir presiones y diámetros instantáneos en vasos sanguíneos e injertos vasculares.
El aparato consiste básicamente de una bomba programable especialmente diseñada para suministrar un caudal de líquido en un circuito hidráulico cerrado, y un reservorio de fluido (solución fisiológica) donde las muestras se conectan mientras permanecen inmersas en el líquido.
El circuito hidráulico se compone de conducciones de silicona, constricciones variables, la muestra a ensayar y el reservorio de fluido.
Se pueden llevar a cabo ensayos estáticos, de presión en aumento monótono o simulaciones con pulsos de frecuencia variable para realizar ensayos dinámicos que imiten el pulso cardíaco de pacientes normales o hipertensos \cite{Balay2010}.
La medición \textit{in vitro} de presión interna se realiza mediante transductores de estado sólido de alta frecuencia\footnote{Konigsberg Inc., PA, USA}.
La variación temporal del diámetro se consigue mediante la técnica de sonomicrometría con transductores (pequeños cristales de ultrasonido)\footnote{Triton Technology Inc., SD, USA} fijados de forma diametralmente opuesta sobre las paredes del injerto.
Para estos ensayos la presión intraluminal se incrementó gradualmente desde aproximadamente \SI{50}{\mmHg} hasta \SI{150}{\mmHg} (figura \ref{fig:tubos_experimental}).

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.7\linewidth]{../Imagenes/02-Modelo/tubos_experimental_byn}
	\caption[Curvas presión-diámetro experimentales para las tres muestras de injertos de PLLA electrohilado.]{Curvas presión-diámetro experimentales para las tres muestras de injertos de PLLA electrohilado.}
	\label{fig:tubos_experimental}
\end{figure}

\subsection{Simulaciones computacionales}

Para simular computacionalmente los ensayos de inflado, se utiliza un modelo macroscópico de membrana tridimensional con geometría cilíndrica discretizado por elementos finitos (figura \ref{fig:tubo_clip}) \cite{WempnerSolidsAndShells}.
Sobre los extremos se imponen condiciones de desplazamiento acorde a cómo se sujetan los injertos en el banco de pruebas \cite{SuaresBagnasco2014}.
A cada punto de integración se le asigna una instancia del RVE propuesto y previamente detallado, componiendo así el modelo multiescala.
Si bien se reportó una leve diferencia entre la morfología de las paredes interior y exterior del andamio, se asumirá que la microestructura es uniforme según el espesor de la pared del injerto.
La tabla \ref{tab:dimensiones_tubos} lista las dimensiones en reposo de las muestras ensayadas.
El largo de los tubos se tomó en \SI{8}{\centi\meter} para todos los casos, habiendo conducido pruebas a diferentes largos sin encontrar variaciones apreciables en la respuesta constitutiva.
Esto tiene sentido dado que la elevada delgadez de la pared respecto del diámetro de los injertos conlleva una muy baja rigidez flexural de la misma.

\begin{figure}[ht]
	\centering
	\captionof{table}[Tabla de dimensiones de injertos de PLLA.]{Dimensiones de los injertos vasculares de PLLA electrohilado utilizados para validar el modelo.}
	\label{tab:dimensiones_tubos}
	{
		\small
		\begin{tabular}{L L L}
			\hline
			\rowcolor{Gray}
			Muestra de PLLA & Espesor de pared (\si{\milli\meter}) & Diámetro interno (\si{\milli\meter})\\
			\hline
			$\#1$       & 0.0410   & 4.9294 \\
			$\#2$       & 0.0385   & 4.9271 \\
			$\#3$       & 0.0375   & 4.9324 \\
			\hline
		\end{tabular}
	}
	\includegraphics[width=0.9\linewidth]{../Imagenes/02-Modelo/tubo_clip.png}
	\captionof{figure}[Modelo de elementos finitos de membrana cilíndrica.]{Modelo computacional de elementos finitos para simular los ensayos de inflado. La geometría inicial y la discretización se muestra en gris. La geometría deformada se muestra coloreada según la magnitud de los desplazamientos radiales (unidad: \si{\meter}).}
	\label{fig:tubo_clip}
\end{figure}
