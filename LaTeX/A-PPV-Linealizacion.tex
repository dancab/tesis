\renewcommand{\det}{\mathrm{det}}
\newcommand{\cuerpo}{\mathcal{C}}
\newcommand{\setV}{\mathcal{V}}
\newcommand{\setu}{\mathcal{U}_s}
\renewcommand{\setU}{\mathcal{U}_m}
\newcommand{\Kinu}{\mathrm{Kin}_\u}
\newcommand{\KinU}{\mathrm{Kin}_\U}
\newcommand{\Kinv}{\mathrm{Kin}_\v}
\newcommand{\Varv}{\mathrm{Var}_\v}
\renewcommand{\V}{\vector{V}}
\newcommand{\VarV}{\mathrm{Var}_\V}
\renewcommand{\D}{\mathscr{D}}
\newcommand{\DD}{\tensor{D}}
\newcommand{\setW}{\mathcal{W}}
\newcommand{\setNull}{\mathcal{N}}
\renewcommand{\R}{\mathbb{R}}
\newcommand{\potene}{\mathcal{P}_e}
\newcommand{\poteni}{\mathcal{P}_i}
\newcommand{\q}{\vector{q}}
\renewcommand{\T}{\tensor{T}}
\newcommand{\vv}{\hat{\v}}
\newcommand{\vw}{\hat{\w}}
\newcommand{\tr}[1]{\mathrm{tr}\parent{#1}}
\renewcommand{\div}{\mathrm{div}}
\newcommand{\n}{\vector{n}}
\renewcommand{\a}{\vector{a}}
\renewcommand{\b}{\vector{b}}
\renewcommand{\S}{\tensor{S}}

\newcommand{\dotl}[1]{\overset{.}{\overline{#1}}}
\newcommand{\dotlp}[1]{\overset{.}{\overline{\parent{#1}}}}
\newcommand{\gat}[1]{\left. \der{}{\tau} #1 \right\rvert_{\tau=0}}
\newcommand{\gatp}[1]{\left. \der{}{\tau} \parent{#1} \right\rvert_{\tau=0}}

\newcommand{\Fi}{\Finv}
\newcommand{\FiT}{\F^{-T}}
\newcommand{\FTN}{\FiT\N}
\newcommand{\normFTN}{\norma{\FTN}}
\newcommand{\fracFTN}{\frac{\FTN}{\normFTN}}

\newcommand{\Resids}[1]{\angles{\mathscr{R}_s\parent{#1},\vv}_{\Regs}}
\newcommand{\vV}{\hat{\V}}
\newcommand{\bRegmN}{\partial \Regm^N}
\newcommand{\Residm}[1]{\angles{\mathscr{R}_m\parent{#1},\vV}_{\Regm}}

\newcommand{\du}{\delta \u}

\section{Introducción}

En este apéndice se presenta con mayor detalle la formulación variacional de equilibrio utilizado en el problema macroscópico del modelo multiescala del capítulo \ref{cap:modelo_multiescala}.

En la formulación clásica de la mecánica se admite \textit{a priori} la existencia de los esfuerzos externos \cite{Gurtin1982introduction}, definidos a través de campos vectoriales asociados a una medida.
De esta forma, se introducen las fuerzas de volumen y fuerzas de superficie, entre otras.
En cambio, en la formulación variacional de la mecánica, se admite \textit{a priori} el concepto de potencia o trabajo virtual, mientras que los esfuerzos externos quedarán definidos a través de la dualidad entre los mismos y el movimiento que se realiza sobre el cuerpo, donde dicha dualidad caracteriza la potencia consumida para realizarlos.
Este segundo enfoque resulta natural y deviene de una experiencia física muy común: si alguien quiere conocer el peso de un objeto cualquiera, lo que hace es levantarla ligeramente y evaluar el peso por la potencia (o trabajo) que se necesitó para realizar el movimiento.

De manera similar, bajo esta conceptualización los esfuerzos internos surgen \textit{a posteriori} a partir del concepto de potencia consumida para realizar una deformación.
Para ejemplificar, si alguien quiere conocer la tensión a la que se encuentra sometida una correa, debe perturbar con los dedos su configuración actual y, a través de la potencia consumida para realizar esa deformación, puede evaluar el valor de la tensión. 

A continuación, entonces, se presenta la cinemática de medios continuos desde el punto de vista de la mecánica variacional, así como la dualidad entre esfuerzos y acciones de movimiento como elementos que generan potencia y, finalmente, el equilibrio a partir del Principio de Potencia Virtual como principio fundamental de la mecánica \cite{Germain1973}.

\section{Cinemática}

\subsection{Configuración material y espacial}

Un cuerpo $\cuerpo$ posee una configuración de referencia $\Regm$, llamada \textit{configuración material}, dada por la región del espacio determinada por las posiciones $\X$ de todas las partículas materiales que lo componen.
Luego, toda otra configuración $\Regs$ del cuerpo en un tiempo $t\in \sqbrac{t_0, t_f}$, llamada \textit{configuración espacial}, viene dada por la aplicación:

\begin{align}
	\map: \Regm &\rightarrow \Regs \\
			\X   &\mapsto \x
\end{align}
donde $\x$ indica la posición de la partícula que inicialmente se ubicaba en $\X$.

Siendo $\x = \map(\X,t)$, es posible introducir el campo de desplazamientos relativo a la configuración material:

\begin{equation}
	\U(\X,t) = \x(\X,t) - \X = \mapt(\X,t) - \X
\end{equation}

Por otra parte, la aplicación $\map$ debe satisfacer algunas propiedades para ser considerada una \textit{deformación}: no debe ocurrir interpenetración del material (para lo que $\map$ debe ser biunívoca) y no puede ocurrir la supresión de un volumen material:

\begin{equation}
	\det \Grad \map(\X,t) > 0 \quad \forall t\in\sqbrac{t_0,t_f}
\end{equation}
donde $\Grad \map$ indica el gradiente del mapeo $\map$, también conocido como \textit{tensor gradiente de deformaciones} $\F$.

Asimismo, el campo $\U$ debe satisfacer ciertas restricciones para garantizar que se cumpla lo anterior:

\begin{equation}
	\F = \Grad \X + \Grad \U = \I + \Grad \U
\end{equation}
donde $\I$ es el tensor identidad.

Es válido aclarar que, dada la configuración de referencia $\Regm$, la configuración espacial $\Regs(t)$ puede obtenerse a partir del campo de desplazamientos $\U(\X,t)$ definido en $\Regm$.
Por lo tanto es equivalente hablar de la configuración $\Regs(t)$ o del campo asociado $\U(\X,t)$.
Además, en base a los requerimientos previamente impuestos a la deformación, se define el espacio vectorial $\setU$ como el conjunto de todas las \textit{configuraciones posibles} que el cuerpo puede tomar.

También es posible definir el campo de desplazamientos $\u(\x)$ relativo a la configuración espacial:

\begin{align}
\u(\x,t) &= \x - \mapt^{-1}(\x,t) \\
\u(\x,t) &= \U(\map(\X),t), \quad \u\in\setu, \quad \U\in\setU
\end{align}
donde $\setu$ es la contraparte de $\setU$ en la configuración espacial. 

\subsection{Deformación de un elemento infinitesimal}

Dado un entorno suficientemente pequeño de un punto $\X_0$, las posiciones de las partículas pertenecientes a ese entorno pueden escribirse como:

\begin{equation}
	\X = \X_0  + \F(\X_0,t)(\X-\X_0) + o(\X-\X_0)
\end{equation}
donde $o(\X-\X_0)$ indica términos de orden superior.

Luego, la deformación de un segmento material infinitesimal dado por $\dX=\X-\X_0$ queda definida en este entorno por:

\begin{align}
	\dx &= \F \dX \\
	\dx \cdot \dx &= \FT\F \dX \cdot \dX
\end{align}

Siguiendo, una medida de deformación para el segmento $\dX$ al pasar a la configuración deformada $\dx$ está dado por:

\begin{equation}
	\dx\cdot\dx - \dX\cdot\dX = \parent{\FT\F-\I}\dX\cdot\dX = 2\E\dX\cdot\dX
\end{equation}

donde

\begin{equation}
	\E = \frac{1}{2} \parent{\FT\F - \I} = \frac{1}{2} \parent{\Grad\U +\Grad^T\U + \Grad^T\U \Grad\U}
\end{equation}
es conocido como \textit{tensor de deformación de Green} y el sobreíndice $T$ indica el tensor transpuesto ($\Grad^T\U=\parent{\Grad\U}^T$).

Hasta aquí se ha dado la descripción \textit{Lagrangiana} de la deformación, donde se sigue a la partícula material $\X$ durante la deformación.
Además, si bien es posible establecer otras medidas de deformación, para el objetivo de este apéndice resulta suficiente contar con la medida dada por el tensor $\E$, que a su vez depende del tensor $\F$.

\subsection{Movimiento y tasa de deformación}

El \textit{movimiento} del cuerpo $\cuerpo$ está dado por la familia uniparamétrica de configuraciones posibles $\Regs(t)$, $t\in\sqbrac{t_0,t_f}$.
A este movimiento, le corresponde en cada instante $t$ una deformación dada por $\map(\X,t)$ y un campo de velocidad $\v(\x,t)$ que se denomina \textit{acción de movimiento} \footnote{Es válido notar que se trata de un campo en función de las posiciones espaciales $\x$, por lo que se dice que es la \textit{descripción espacial} de la velocidad}:

\begin{equation}
	\v(\x,t) = \evalat{ \pder{\U(\X,t)}{t} }{ \X=\map^{-1}(\x,t) }
\end{equation}

Luego, se define el espacio vectorial $\setV$ como el conjunto de todas las acciones de movimiento posibles a partir de $\U(\X,t)$.
Notar que, bajo esta definición, el campo de velocidad real del cuerpo es en el instante $t$, un elemento de $\setV$.

Es usual que en un problema de equilibrio mecánico, el cuerpo deba satisfacer ciertas \textit{restricciones cinemáticas}.
Las configuraciones posibles del cuerpo que además cumplen con estas restricciones se conocen como \textit{configuraciones admisibles}, y el conjunto de estas define al subespacio vectorial:

\begin{equation}
	\Kinu = \braces{\u \in \setu \ ,\ {\footnotesize \u\text{ es una configuración cinemáticamente admisible}}}
\end{equation}

En función de esta última definición, se dice que todo movimiento a partir de $\u(\X,t)\in\Kinu$ se considera \textit{movimiento admisible} si todas las configuraciones que lo componen son configuraciones admisibles.
Además, a cada movimiento admisible a partir de $\u(\X,t)$ le corresponde una \textit{acción de movimiento admisible} y el conjunto de todas ellas constituye el subconjunto: 

\begin{equation}
	\Kinv = \braces{\v\in\setV \ ,\ {\footnotesize \v\text{ es una acción de movimiento cinemáticamente admisible}}}
\end{equation}

También es posible definir el subconjunto de acciones de movimiento \textit{variacionalmente admisible} $\Varv$ dado por el conjunto de los campos de velocidad tales que $\v=\vcero$ en aquellos puntos donde están prescriptas las acciones de movimiento.
Se verifica, entonces, que $\Kinv = \bar{v} + \Varv$, donde $\bar{v}\in\Kinv$ es una acción de movimiento arbitraria compatible con las restricciones cinemáticas.

Finalmente, se define un operador tasa de deformación ($\D$) que aplicado sobre el campo de velocidades ($\v$) otorga el tensor tasa de deformación ($\DD$) definido como:

\begin{equation}
	\D(\v) = \DD = \gradS\v
\end{equation}
donde $\gradS\v$ indica el gradiente simétrico del campo $\v$.

Esta definición introduce, además, el espacio vectorial $\setW$, cuyos elementos son todos los campos tensoriales simétricos definidos en la configuración $\u(\X,t)$.
Es posible notar que no todo $\DD\in\setW$ está asociado a un campo $\v\in\setV$, pues no todo tensor simétrico proviene de un gradiente simétrico sobre $\v$.
En cambio, se indica que, si dado $\DD\in\setW$, se puede hallar un campo $\v\in\setV$ asociado de forma que se verifique la expresión anterior, entonces $\DD$ es una tasa de deformación \textit{compatible} cinemáticamente admisible.

Finalmente, se identifica el conjunto de todas las acciones de movimiento posibles \textit{rígidas}, siendo aquellos $\v$ con tasas de deformación nula.
Este conjunto se denomina \textit{espacio nulo} del operador $\D$ y está dado por:

\begin{equation}
	\setNull(\D) = \braces{\v\in\setV,\ \D(\v)=\vcero \quad \forall\x\in\Regs}
\end{equation}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.4\linewidth]{../Imagenes/05-Apendices/ppv_cinematica_conjuntos_01}
	\caption[Espacios y subespacios vectoriales propios de la cinemática del PPV.]{Espacios y subespacios vectoriales introducidos por la cinemática adoptada.}
	\label{fig:ppv_cinematica_conjuntos_01}
\end{figure}

\section{Dualidad}

\subsection{Entre fuerzas y acciones de movimiento}

La forma \dquotes{clásica} de caracterizar la acción que un cuerpo realiza sobre otro es a través del concepto \textit{fuerza} como un ente conocido \textit{a priori} y de manera independiente de la cinemática adoptada.
En el PPV, en cambio, la fuerza surge sólo como un elemento en \textit{dualidad} a una determinada acción de movimiento, donde la dualidad se define a través del concepto, ahora sí \textit{a priori}, de potencia o trabajo virtual.

Entonces, se define a la \textit{potencia externa} ($P_e$) como el valor de un funcional lineal y continuo en $\setV$ ($\potene(\v)$).
Luego, en base al Teorema de la Representación de Riesz, es posible identificar un elemento $\f$, de la misma naturaleza que $\v$\footnote{En este caso, como $\v$ es un campo vectorial, $\f$ también lo es.}.

\begin{equation}
	P_e = \angles{\f,\v}
\end{equation}

El conjunto de todos los sistemas de fuerzas $\f$ (es decir, el conjunto de todos los funcionales lineales y continuos en $\setV$) define al espacio vectorial $\setV'$, denominado \textit{espacio de fuerzas externas}.
Este espacio es \textit{dual} al espacio de acciones de movimiento $\setV$: una vez definido el modelo cinemático, el sistema de fuerzas compatible queda totalmente definido por la dualidad a través de la siguiente forma bilineal:

\begin{align}
	\angles{\cdot,\cdot}: \setV'\times\setV &\rightarrow \R  \\
							\parent{\f,\v}   &\mapsto  P_e = \angles{\f,\v} \quad \f\in\setV', \quad \v\in\setV 
\end{align}

\subsection{Entre esfuerzos internos y tasas de deformación}

En la mecánica del continuo \textit{clásica}, el concepto de tensión se obtiene como una extensión del concepto de fuerza.
En el PPV, similarmente a lo expuesto en la sección anterior, los esfuerzos internos se obtienen a partir de un funcional lineal y continuo sobre las tasas de deformación ($\poteni(\DD)$).
Este funcional resulta en un campo escalar $p_i$ definido en $\Regs$.
Luego, \textit{potencia interna total} viene dada por:

\begin{equation}
P_i = \int_{\Regs} p_i \diff V = -\int_{\Regs} \T\cdot\DD \diff V
\end{equation}

En la expresión anterior, el esfuerzo interno $\T$ es el elemento que caracteriza el funcional lineal y continuo sobre las acciones de deformación ($\DD\in\setW$) , es decir, $\T$ es el elemento dual de $\DD$. 
Por lo tanto, podemos definir el espacio $\setW'$ como aquel constituido por todos los funcionales lineales y continuos sobre $\DD$, siendo los mismos representados por campos tensoriales simétricos $\T$\footnote{Resulta evidente a esta altura que $\T$ se corresponde con el tensor de tensiones de Cauchy.}.

\subsection{Operador de equilibrio}

Como se vio, los espacios $\setV'$ y $\setW'$ son los espacios duales de $\setV$ y $\setW$, mientras que las formas bilineales $\angles{\cdot,\cdot}$ y $\parent{\cdot,\cdot}$ representan los pares en dualidad de $\setV'\times\setV$ y $\setW'\times\setW$, respectivamente. 
También se definió el operador tasa de deformación $\D: \setV\rightarrow\setW$.

Ahora, es posible introducir el \textit{operador adjunto} u \textit{operador de equilibrio} $\D^*: \setW'\rightarrow\setV'$, que se define de la siguiente manera:

\begin{equation}
	\parent{\T,\D(\v)} = \angles{\D^*(\T),\v}
\end{equation}

Este operador permite, conjuntamente con el Principio de Potencias Virtuales, caracterizar los esfuerzos internos \textit{compatibles} con el modelo cinemático elaborado.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.6\linewidth]{../Imagenes/05-Apendices/ppv_cinematica_conjuntos_02}
	\caption[Espacios y subespacios vectoriales del PPV.]{Espacios y subespacios vectoriales del PPV.}
	\label{fig:ppv_cinematica_conjuntos_02}
\end{figure}

\section{Principio de Potencia Virtual}

Para todo sistema de referencia inercial, y para cada instante de tiempo $t$, el cuerpo $\cuerpo$ se encuentra en equilibrio (estático\footnote{La formulación puede extenderse fácilmente al caso de equilibrio dinámico si se incluye en el análisis a las fuerzas de inercia como fuerzas externas adicionales.}) en la configuración $\Regs$, con restricciones cinemáticas bilaterales y bajo la acción del sistema de fuerzas externas $\f\in\setV$ si la \textit{potencia virtual externa} de dichas fuerzas es nula para toda acción de movimiento virtual rígida a partir de esta configuración: 

\begin{equation} \label{eq:ppv_libre_01}
	P_e = \angles{\f,\vv} = 0 \quad \forall \vv\in\Varv\cap\setNull(\D)
\end{equation}

Además, se satisface que la suma de las potencias externa e interna es nula para toda acción de movimiento virtual:

\begin{equation}
	P_e + P_i = \angles{\f,\vv} -\parent{\T,\D(\vv)} = 0 \quad \forall\vv\in\Varv
\end{equation}

La primera parte (\ref{eq:ppv_libre_01}) permite caracterizar el sistema de fuerzas compatible con la cinemática adoptada:

\begin{equation}
	\angles{\f,\vv} = \angles{\D^*(\T),\vv} = \parent{\T,\D(\vv)} \quad \forall \vv\in\setV
\end{equation}

Específicamente para el caso de la cinemática planteada de cuerpos continuos, se tiene que:

\begin{align}
	\angles{\f,\vv} = \parent{\T,\D(\vv)} 
		& = \int_{\Regs} \T\cdot\gradS\vv\diff V \\
		& = \int_{\Regs} \T\cdot\grad\vv\diff V \\
	 	& = \int_{\Regs} \sqbrac{\div\parent{\T\vv} - \div\T\cdot\vv} \diff V \\
	 	& = \int_{\bRegsN} \T\n\cdot\vv \diff S + \int_{\bRegs} \div\T\cdot\vv  \diff V \\
	 	& = \int_{\bRegsN} \a \cdot \vv \diff S + \int_{\bRegs} \b \cdot \vv \diff V \\
	 	& = \angles{\D^*(\T),\vv}
\end{align}
donde $\bRegsN$ es la frontera de Neumann de $\Regs$, donde no se encuentran prescriptas las acciones de movimiento.

Es decir, se ha encontrado la forma del operador de equilibrio para este caso:

\begin{equation}
	\D^*(\cdot) = 
	\begin{cases}
		-\div(\cdot) \quad &en\ \Regs \\
		(\cdot)\n \quad &en \bRegsN
	\end{cases}
\end{equation}

Además, se ha encontrado que el sistema de fuerzas externas que es compatible con acciones de movimiento $\vv\in\Varv$, está caracterizado por una fuerza por unidad de volumen ($\b$) en $\Regs$ y por una fuerza por unidad de superficie ($\a$) en $\bRegsN$ (figura \ref{fig:cuerpo_DN}).

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.6\linewidth]{../Imagenes/05-Apendices/cuerpo_DN}
	\caption[Esquema del cuerpo sometido a esfuerzos externos.]{Esquema del cuerpo en la configuración $\Regs$, con restricciones cinemáticas bilaterales en la frontera de Dirichlet ($\bRegsD$) y bajo dos tipos de esfuerzos externos: una fuerza de volumen ($\b$) en la región $\Regs$ y una fuerza de superficie ($\a$) en la frontera de Neumann ($\bRegsN$).}
	\label{fig:cuerpo_DN}
\end{figure}

Finalmente, el principio de potencia virtual, para el caso particular bajo consideración, adopta la siguiente forma:

\begin{equation} \label{eq:ppv_config_espacial}
	\int_{\Regs} \T\cdot\gradS\vv \diff V - \int_{\Regs} \b\cdot\vv \diff V - \int_{\bRegsN} \a\cdot\vv \diff S = 0 \quad \forall \vv\in\Varv
\end{equation}

\section{Linealización}

\subsection{Expresión del PPV en la configuración material}

Hasta aquí se presentó el PPV en la configuración en que se encuentra realmente definido, es decir, en la configuración actual o espacial.
Sin embargo, como es usual en la mecánica, se desea resolver el campo de desplazamientos a partir de conocer la configuración material.

Para ello, se hace uso de las siguientes relaciones: 

\begin{align}
(\diff V)_m &= \det\F \diff V_m \\
(\diff S)_m &= \det\F \normFTN \diff S_m \\
\n_m        &= \fracFTN \\
\S          &= \det\F \ \F^{-1} \T \F^{-T}
\end{align}
donde $\N$ es el versor normal a la superficie $\bRegmN$, $\S$ el tensor de tensiones de Piola-Kirchhoff de segunda especie y el subíndice $m$ indica que se expresan en términos de las coordenadas materiales ($\X$), variables que naturalmente están dadas en términos de las coordenadas espaciales ($\x$).

Luego, el problema mecánico puede expresarse equivalentemente de la siguiente manera: encontrar $\U\in\KinU$ tal que

\begin{equation} \label{eq:ppv_config_material}
\begin{aligned}
	&\int_{\Regm} \S(\E(\U))\cdot\parent{\F^T \Grad\vV}^S \diff V_m 
	- \int_{\bRegmN} \b_m\cdot\vV \ \det\F \ \diff S_m\\
	&\qquad - \int_{\bRegmN} \a_m\cdot\vV \ \det\F \ \normFTN \ \diff S_m = 0 \quad \forall \vV\in\VarV
\end{aligned}
\end{equation}
donde $\vV = \vv_m$ y $\VarV=(\Varv)_m$.

Por practicidad, en adelante se expresará (\ref{eq:ppv_config_material}) en forma compacta como sigue: 

\begin{equation}
\Residm{\U} = 0 \quad \forall \vV\in\VarV
\end{equation}

\subsection{Linealización}

Una vez definida la expresión del PPV en la configuración material (conocida), se plantea el método de Newton-Raphson como la siguiente aproximación: dada una configuración $\U^k\in\KinU$, hallar el incremento $\dU\in\VarV$ tal que 

\begin{equation}
	\begin{aligned}
	&\Residm{\U^k} \\
	&\qquad + \gat{\Residm{\U^k+\tau\dU}} = 0 \quad \forall \vV\in\VarV
	\end{aligned}
\end{equation}
donde el operador $\gatp{\cdot}$ es la conocida derivada de Gateaux.

En consecuencia PPV linealizado en la configuración material queda:

\begin{equation}
	\begin{aligned}
		&\int_{\Regm} \S\cdot\parent{\F^T \Grad\vV}^S\ \diff V_m 
		- \int_{\bRegmN} \b_m\cdot\vV \ \det\F \ \diff S_m\\
		&\qquad - \int_{\bRegmN} \a_m\cdot\vV \ \det\F \ \normFTN \ \diff S_m \\
		&\qquad + \int_{\Regm} \parent{\pder{\S}{\E}}\parent{\FT\Grad\dU}^S \cdot \parent{\FiT\Grad\vV}\ \diff V_m \\
		&\qquad - \int_{\Regm} \b_m\cdot\vV \parent{\FiT\cdot\Grad\dU}\ \det\F\ \diff V_m \\
		&\qquad - \int_{\bRegmN} \a_m\cdot\vV \sqbrac{\parent{\Grad\dU \Fi}\cdot\P^t_m} \ \det\F\ \normFTN\ \diff S_m \\
		&\qquad \qquad = 0 \quad \forall \vV\in\VarV
	\end{aligned}
\end{equation}
donde $\P^t_m=\parent{\I - \fracFTN\otimes\fracFTN}$ es el tensor proyección tangencial en términos de variables materiales.

Es importante tener en cuenta que si bien en la expresión anterior se ha dejado implícita la dependencia de $\U^k$ para facilitar la lectura, se debe tener en cuenta que donde se lee $\S$ y $\F$, se trata de $\S(\E(\U^k))$ y $\F(\U^k)$, respectivamente.

Finalmente, es posible reescribir el PPV linealizado nuevamente en la configuración espacial.
Esto presenta la ventaja de mejorar la precisión con la que se evalúan los términos en la frontera en esquemas discretos, por no intervenir el tensor $\F$ en su cálculo:

\begin{equation}
\begin{aligned}
	&\int_{\Regs} \sqbrac{\frac{1}{\det\F}\F\S\F}_e \cdot \parent{\grad\vv}^S \ \diff V 
	- \int_{\Regs} \b\cdot\vv \ \diff V - \int_{\bRegsN} \a\cdot\vv \ \diff S \\
	&\qquad + \int_{\Regs} \tensor{D}_e \parent{\grad\du}^S \cdot \grad\vv \ \diff V 
	- \int_{\Regs} \b\cdot\vv\parent{\div\du}\ \diff V \\
	&\qquad - \int_{\bRegsN} \a\cdot\vv\parent{\grad\du \cdot \P^t}\ \diff S = 0 \quad \forall \vv\in\Varv
\end{aligned}
\end{equation}
donde $\P^t=\parent{\I - \n\otimes\n}$ es el tensor proyección tangencial.