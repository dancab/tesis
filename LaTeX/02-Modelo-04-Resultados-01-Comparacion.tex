Los parámetros microscópicos del modelo constitutivo multiescala se ajustaron para reproducir las curvas experimentales de presión-diámetro reportadas para los injertos vasculares de PLLA electrohilado (figura \ref{fig:tubos_experimental}).
Estas curvas presentan una respuesta en forma de \dquotes{J} frente al aumento de la presión interna: la variación en diámetro del injerto frente al aumento de la presión es relativamente menor en el rango de presiones más bajas ($\approx \SI{50}{\mmHg}$) respecto del rango de presiones más altas ($\approx \SI{150}{\mmHg}$), con una transición gradual en el medio.
El modelo constitutivo logra capturar apropiadamente esta característica para los tres conjuntos de datos, tal como se muestra en la figura \ref{fig:tubos_ajuste}.
Los valores correspondientes a los parámetros que ajustan la respuesta de cada muestra están listados en la tabla \ref{tab:parametros_tubos}.
A medida que se incrementa la presión interna, las curvas de presión-diámetro capturadas en las simulaciones muestran la misma respuesta con forma de \dquotes{J}, con la pendiente aumentando como resultado de un creciente número de fibras que van siendo reclutadas a medida que avanza la deformación.
Este cambio en pendiente se reduce a medida que el número de fibras pendientes de reclutar es menor y finalmente se llega a una región prácticamente lineal al tener a todas las fibras rectas y reclutadas.

\begin{figure}[p]
	\centering
	\captionof{table}[Parámetros ajustados para reproducir curvas experimentales de presión-diámetro.]{Parámetros del modelo ajustados para reproducir las curvas de presión-diámetro experimentales de las tres muestras de PLLA. El valor medio (VM) y la desviación estándar (DE) de cada parámetro también se encuentra listado.}
	\label{tab:parametros_tubos}
	{
		\small
		\begin{tabular}{L L L L L L L L}
			\hline
			\rowcolor{Gray}
			& $\fv$ & $\Et {\scriptstyle(\si{\giga\pascal})}$ & $\Eb {\scriptstyle(\si{\mega\pascal})}$ & $\lamero$ & $\lamers$ & $\lamermin$ & $\lamermax$ \\
			\hline
			$\#1$    & 0.3   & 2.9         & 5.186       & 1.1091 & 0.0313 & 1.00     &  1.40    \\
			$\#2$    & 0.3   & 2.9         & 7.961       & 1.1079 & 0.0265 & 1.00     &  1.40    \\
			$\#3$    & 0.3   & 2.9         & 9.625       & 1.1190 & 0.0328 & 1.00     &  1.40    \\
			\hline
			VM       & 0.3   & 2.9         & 7.591       & 1.1120 & 0.0302 & 1.00     &  1.40    \\
			DE       & 0.0   & 0.0         & 1.831       & 0.0050 & 0.0027 & 0.00     &  0.00    \\
			\hline
		\end{tabular}
	}
	\includegraphics[width=0.7\linewidth]{../Imagenes/02-Modelo/tubos_ajuste_byn.pdf}
	\captionof{figure}[Curvas de presión-diámetro ajustadas mediante el modelo multiescala.]{Curvas de presión-diámetro experimentales y simuladas para las tres muestras de injertos vasculares de PLLA. Los coeficientes $R^2$ del ajuste son $0.989$ para el ensayo $\#1$ y $0.990$ para los ensayos $\#2$ y $\#3$.}
	\label{fig:tubos_ajuste}
\end{figure}

Mayores deformaciones podrían provocar fenómenos de rotura de nanofibras o de plasticidad, conllevando adicionales efectos no lineales.
Estos escenarios no fueron contemplados en este primer RVE prototipo, donde el objetivo ha sido modelar el comportamiento elástico de un injerto vascular en el rango fisiológico de presiones.
Es importante notar que un injerto que busque reemplazar tejido biológico mecánicamente no lineal, debe mostrar un comportamiento semejante no lineal sin incurrir en deformación plástica o rotura de fibras que conlleven a subsecuentes mecanismos de falla.
Sin embargo, estos fenómenos son tenidos en cuenta en los capítulos siguientes con RVEs de mayor complejidad geométrica.

La mayoría de los modelos publicados, incluidos los recientes, no llevan en consideración la tortuosidad de las fibras o su dispersión estadística, por lo que no logran capturar el proceso de reclutamiento progresivo de las fibras durante la deformación \cite{Carleton2017,Silberstein2012,Stylianopoulos2008,Niu2014,Johnsonjed2007}.
Adicionalmente, la respuesta macroscópica no lineal suele reproducirse en los modelos multiescala mediante la adopción de leyes constitutivas hiperelásticas no lineales para las fibras en la microescala.
Sin embargo, son numerosos los trabajos experimentales que reportan un comportamiento mecánico lineal para las nanofibras sometidas a deformaciones bajas como las fisiológicas \cite{Inai2005,Tan2005a,Tan2005b,Tan2006,Chen2008,Wong2008,Niu2014}.
En el modelo aquí presentado, la adopción de una distribución estadística para la tortuosidad de las fibras permite reproducir exitosamente la respuesta constitutiva macroscópica no lineal, aún contemplando un material mecánicamente lineal para las nanofibras individuales.