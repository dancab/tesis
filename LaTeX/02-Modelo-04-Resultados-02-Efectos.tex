Las propiedades microscópicas son las que determinan la respuesta macroscópica del material.
Dado que es posible acceder a algún grado de control sobre estas propiedades durante el proceso de electrohilado, resulta de interés estudiar el efecto macroscópico producido por variaciones de estos parámetros microestructurales.
Para hacerlo, se toma como punto de partida los valores obtenidos para el ajuste con datos experimentales (tabla \ref{tab:parametros_tubos}).
Se seleccionaron los parámetros más significativos: la orientación del RVE para evaluar el nivel de anisotropía, el módulo elástico de las nanofibras como variable más importante relacionada con la rigidez del material y los parámetros de la distribución de reclutamiento para evaluar su función en la respuesta no lineal con forma \dquotes{J}.
Luego se llevó a cabo una serie de simulaciones considerando variaciones en estos parámetros y los resultados se compararon entre sí en términos de tensión circunferencial versus elongación circunferencial, cuya relevancia es fundamental en arterias, por ejemplo, si se quieren desarrollar matrices biomiméticas.

\begin{figure}[p] 
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/02-Modelo/sensibilidad_angulo.pdf}
		\subcaption{}\label{fig:sensibilidad_angulo}
		\vspace{4ex}
	\end{subfigure}%% 
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/02-Modelo/sensibilidad_Et.pdf} 
		\subcaption{}\label{fig:sensibilidad_Et}
		\vspace{4ex}
	\end{subfigure} 
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/02-Modelo/sensibilidad_lro.pdf} 
		\subcaption{}\label{fig:sensibilidad_lro}
	\end{subfigure}%%
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/02-Modelo/sensibilidad_lrs.pdf} 
		\subcaption{}\label{fig:sensibilidad_lrs}
	\end{subfigure} 
	\caption[Efecto de los parámetros microscópicos sobre la respuesta mecánica macroscópica.]{Efecto de los parámetros microscópicos sobre la respuesta mecánica macroscópica. Se muestran curvas simuladas de tensión-elongación circunferencial como resultado de variaciones de la dirección de deformación uniaxial ($\theta$) (a), módulo elástico de las fibras a la tracción (b), valor medio de la distribución de reclutamiento (c) y valor de dispersión de la distribución de reclutamiento (d).}
	\label{fig:sensibilidad} 
\end{figure}

\subsubsection{Orientación del RVE}

Para evaluar la anisotropía del RVE se estudia la sensibilidad de la respuesta mecánica frente a cambios en la dirección de deformación uniaxial.
Se realizan dos simulaciones con direcciones de deformación de \ang{90} y \ang{75} respecto de la horizontal (ver figura \ref{fig:microstructura_microscala_rve}).
Dada la geometría de dos celdas triangulares superpuestas (con simetría angular cada \ang{30}), las dos direcciones de carga elegidas maximizan la variación en la respuesta del RVE.
La comparación entre las dos simulaciones se muestra en la figura \ref{fig:sensibilidad_angulo}, donde puede verse que ambas curvas están prácticamente solapadas, significando que los efectos de anisotropía son despreciables para este RVE prototipo.
En consecuencia, este RVE es un buen candidato para simular mallas de orientación isotrópica de fibras, pero necesitaría modificaciones para poder simular correctamente mallas de fibras alineadas.

\subsubsection{Módulo elástico de las nanofibras}

El módulo tangente elástico macroscópico, medido sobre el final de la curva cuando todas las fibras se encuentran reclutadas, depende principalmente del módulo elástico a la tracción de las nanofibras.
La figura \ref{fig:sensibilidad_Et} muestra cómo varía la curva en tensión-elongación circunferencial para el injerto acorde a variaciones de $\Et$.
Un incremento en la rigidez de las nanofibras resulta en un módulo tangente macroscópico mayor, sin modificar la forma \dquotes{J} de la curva e incluso manteniendo inalterados los valores de elongación para los cuales comienza y termina el reclutamiento de las fibras y, por ende, la transición del módulo tangente desde el pequeño valor inicial hasta su valor final.
En consecuencia, controlar la rigidez de las nanofibras (por ejemplo por medio de seleccionar diferentes polímeros base) permite modificar la compliancia del injerto electrohilado en su estado deformado.
Este aspecto es esencial para el objetivo de alcanzar la biomímesis, aunque no resulta suficiente por cuenta propia, ya que la respuesta mecánica del material en reposo se mantiene inalterada.

\subsubsection{Parámetros de la distribución de tortuosidades}

La distribución de enrulamiento cuasi normal adoptada permite controlar dos parámetros: el valor medio $\lamero$ y la dispersión $\lamers$.
En primera instancia se realizó la variación del valor medio de reclutamiento con el objetivo de aumentar la tortuosidad de todas las nanofibras sin alterar su dispersión.
Esto resultó en una traslación de la curva de tensión-elongación circunferencial, sin cambios en su forma (figura \ref{fig:sensibilidad_lro}).
En otras palabras, valores más altos de $\lamero$ conllevan valores más altos de elongación circunferencial macroscópica para los cuales las nanofibras devienen rectas y se reclutan.
En segunda instancia, se varió el valor de dispersión, generando distribuciones de reclutamiento con mayor o menor número de nanofibras con valores de reclutamiento alejados del valor medio.
En este caso, se observó que a mayores valores de $\lamers$ se obtienen curvas de tensión-elongación macroscópicas con una transición de módulo elástico más suave y prolongada (figura \ref{fig:sensibilidad_lrs}).
En consecuencia, modificar la FDR, es decir generar microestructuras con diferentes distribuciones de enrulamiento de las nanofibras, conlleva un cambio en la transición de la respuesta mecánica debido al proceso de reclutamiento progresivo.
Por lo tanto, en el diseño de materiales biomiméticos, la posibilidad de controlar la FDR puede ser un factor crucial, ya que su efecto resulta complementario al de seleccionar diferentes polímeros base para controlar la rigidez de las fibras.