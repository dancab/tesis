En esta sección se presenta un \concepto{algoritmo de deposición virtual de fibras} para generar geometrías de matrices fibrosas planas simulando la deposición de las fibras en el proceso de electrohilado descripto en la sección \ref{sec:cap1_electrohilado}, para luego conformar una geometría de matriz fibrosa tridimensional mediante la superposición de muchas capas planas.

El algoritmo busca reproducir la geometría a escala microscópica de la malla de fibras, por lo cual se delimita mediante un recinto cuadrado $\Omega_\mu$ de tamaño $\Lcapa$, dentro del cual se simulará la deposición de las fibras.
Cada fibra es inicialmente una curva continua lineal por tramos que se construye mediante la concatenación de segmentos lineales de igual longitud $\ls$. 
Además, como se consideran fibras muy largas con respecto al tamaño del recinto, se asume que su deposición, desde la perspectiva microscópica, comienza y termina siempre en un borde $\Omega_\mu$.
Luego, el algoritmo para la deposición de una sola fibra se detalla de la siguiente manera:

\begin{enumerate}
	\item Se selecciona de forma aleatoria un punto $\mathcal{P}_1$ sobre el borde de $\Omega_\mu$ como punto inicial para la construcción de la fibra.
	\item Se deposita el primer segmento de la fibra, de largo $\ls$, con origen en $\mathcal{P}_1$ y orientación dada por el ángulo $\theta_1$ que se obtiene de muestrear una función distribución de orientación (FDO) prescripta ($\fdop$), consistiendo en una función de densidad de probabilidad de manera que la probabilidad de que el segmento inicial de una fibra cualquiera posea una orientación entre $\theta$ y $\theta + \dif{\theta}$ es $\fdop(\theta)\diff\theta$. Por tratarse del ángulo que forma un segmento recto con la horizontal, la FDO es periódica tal que $\fdop(\theta) = \fdop(\theta+\pi)$, por lo que conviene restringir al ángulo $\theta$ al intervalo $[0,\pi)$.
	\item Subsecuentemente, se concatena un segundo segmento con punto de origen en el punto final del segmento anterior, con un ángulo $\theta_2$ igual a $\theta_1$ más una posible desviación $\theta_{d2}$ que se obtiene de forma aleatoria a partir del intervalo $I_d = \sqbrac{-\devangmax,\devangmax}$. Por lo tanto el ángulo de orientación de este segmento es $\theta_2 = \theta_1 + \theta_{d2}$. 
	\item Se continua el proceso de concatenación para cada segmento subsiguiente hasta que un segmento eventualmente sale del recinto $\Omega_\mu$. Este segmento es el último de la fibra y es recortado en el borde del recinto, de forma que la fibra recorre un camino lineal por tramos entre dos puntos del borde.
\end{enumerate}

%\begin{figure}[ht]
%	\centering
%	\includegraphics[width=0.6\linewidth]{../Imagenes/03-Geometria/periodic_normal.pdf}
%	\caption[Función densidad de probabilidad de orientación periódica.]{Función de distribución periódica: se muestra un caso de una función densidad de probabilidad periódica (línea sólida) correspondiente a una distribución con cierta alineación sobre el ángulo.}
%	\label{fig:periodic_normal}
%\end{figure}

De esta forma, los parámetros del algoritmo de deposición virtual para una sola fibra son: la FDO prescripta ($\fdop(\theta)$), la longitud de segmento ($\ls$) y el ángulo de desviación máximo ($\devangmax$).
Es válido resaltar que la curva así conformada es continua pero de derivada discontinua y que, de ser necesario, se podría realizar una interpolación mediante \textit{splines} para culminar con una curva de mayor grado de continuidad.
La figura \ref{fig:algoritmo_una_fibra} muestra una fibra compuesta de pocos segmentos con el objetivo de esquematizar el algoritmo previo.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.6\linewidth]{../Imagenes/03-Geometria/algoritmo_una_fibra}
	\caption[Algoritmo de deposición virtual de una fibra.]{Deposición virtual de una fibra según el algoritmo detallado. Se muestra la deposición de 4 segmentos donde se detalla el punto de origen $\mathcal{P}_1$, el ángulo del primer segmento $\theta_1$, el ángulo de desviación del segundo segmento $\theta_{d2}$ y el ángulo del segundo segmento $\theta_2=\theta_1+\theta_{d2}$. El cuarto y último segmento (que sale del recinto cuadrado de largo $\Lcapa$) es recortado de forma que su longitud es menor a la del resto de los segmentos ($\ls$).}
	\label{fig:algoritmo_una_fibra}
\end{figure}

Es importante notar que el valor de $\devangmax$ está relacionado con la tortuosidad de las fibras, cumpliéndose que para un valor nulo se obtendrán fibras rectas.
Además, dado que las fibras observadas no poseen cambios bruscos de orientación en puntos cercanos a lo largo de su línea media, es razonable admitir que el valor de $\devangmax$ será pequeño ($\devangmax<\ang{90}$).

%Es importante en esta instancia diferenciar entre la orientación de un segmento en el algoritmo y de una fibra medida en una imagen SEM.
%La orientación de las fibras tiene periodicidad de forma que un ángulo $\theta$ equivale a $\theta + \pi$, ya que se trata de medir el ángulo que una recta forma respecto del eje horizontal.
%En cambio los segmentos, por el método de construcción descripto, tienen un punto inicial y un punto final, por lo que la periodicidad será tal que un ángulo $\theta$ será equivalente a $\theta + 2\pi$, pero no a $\theta + \pi$ que indicará un segmento de sentido opuesto.

Resulta útil formular el algoritmo respeto de longitudes adimensionales, utilizando el diámetro promedio $\Dm$ de las fibras como medida de normalización.
Los parámetros adimensionales se denotan mediante el símbolo $\tilde{(\cdot)}$.
Entonces, el largo adimensional de cada lado del recinto es $\adLcapa=\Lcapa/\Dm$, mientras que cada fibra $i$ tiene una longitud de contorno adimensional $\tilde{\loco_i}=\loco_i/\Dm$, una longitud extremo-extremo adimensional $\tilde{\letei}=\letei/\Dm$ y un diámetro adimensional $\tilde{D}_i = D_i/\Dm$.
Cabe mencionar que en el caso particular que todas las fibras posean un mismo diámetro, todas tendrán diámetro adimensional unitario.
Además, durante la construcción de las fibras, cada segmento lineal se construye con una longitud adimensional $\adls=\ls/\Dm$.

Ya establecidos los pasos para la generación de cada fibra mediante un camino aleatorio, puede ahora resumirse el algoritmo para la construcción de la geometría total de la malla como sigue:

\begin{enumerate}
	\item Seleccionar los parámetros del algoritmo: el diámetro promedio de las fibras $\Dm$, la FDO prescripta $\fdop(\theta)$, el ángulo de desviación máximo $\devangmax$, el tamaño de recinto $\adLcapa$, la longitud de segmento $\adls$ y la fracción de volumen objetivo $\fv$.
	\item Generar una capa depositando fibras mediante los pasos descritos previamente hasta alcanzar la fracción de volumen $\fv$.
	Se asume que el espesor de cada capa es equivalente al diámetro medio de las fibras $\Dm$, por lo que el volumen de la capa es $\Lcapa\times \Lcapa \times \Dm$.
	Luego, la fracción de volumen de la capa $k$ es:
	
	\begin{equation}
	\fv_k = \frac{\sum_{i=1}^{N_i^k} V_i}{\parent{\Lcapa}^2 \Dm}
	\end{equation}
	donde $V_i=L_i \frac{\pi D_i^2}{4}$ es el volumen de la fibra $i$.
	\item Repetir el paso 2 hasta tener el número deseado de capas ($N_c$), considerando cada capa nueva superpuesta sobre la capa anterior. De esta forma se tiene que la capa $k$ está en contacto directo con las capas $k-1$ y $k+1$ (figura \ref{fig:malla}).
	\item Calcular los puntos de intersección entre las fibras dentro de cada capa y entre capas adyacentes. De esta forma se evita la inclusión de intersecciones aparentes en la proyección bidimensional (entre fibras que se encuentran distanciadas entre sí más de un diámetro medio en la dirección normal al plano colector) (figura \ref{fig:malla_i}).
	\item Con los puntos de intersección identificados, realizar la subdivisión de las fibras originales (\concepto{fibras largas}) en los puntos de unión, obteniendo así las fibras finales (\concepto{fibras cortas}) como elementos estructurales entre dos puntos de unión (figura \ref{fig:malla_i}).
	\item Calcular la longitud de contorno, longitud extremo-extremo y elongación de reclutamiento para cada una de las fibras cortas (figura \ref{fig:malla_s}).
\end{enumerate}

Como resultado se obtiene una geometría de red de fibras interconectadas como se muestra en la figura \ref{fig:comparacion_cualitativa}, donde puede observarse gran similitud de forma en una primera observación entre una geometría generada mediante el algoritmo (parámetros: $\Dm=\SI{1}{\micro\meter}$, $N_c=10$, $\adLcapa=100$, $\adls=10$, $\devangmax=\ang{20}$, $\fv=0.2$, $\fdop(\theta)=\pi^{-1}$)  y la que se ve en imágenes SEM de matrices electrohiladas.

\begin{figure}[ht]
	\centering
	\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/comparacion_cualitativa}
	\caption[Comparación cualitativa entre una imagen SEM de una malla electrohilada de PLLA y una malla obtenida mediante el algoritmo de deposición virtual.]{Comparación cualitativa entre una imagen SEM de una malla electrohilada de PLLA (izq., colores invertidos para mejor visualización) y una malla obtenida mediante el algoritmo de deposición virtual (der.) donde la escala de grises indica la capa de cada fibra. Puede observarse a primera vista una gran similitud entre la geometría de la malla simulada y la microestructura electrohilada.}
	\label{fig:comparacion_cualitativa}
\end{figure}

\begin{figure}[p] 
	\centering
	\begin{subfigure}[b]{0.49\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/Malla2.pdf} 
		\subcaption{}\label{fig:malla2}
		%\vspace{4ex}
	\end{subfigure}
	\begin{subfigure}[b]{0.49\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/Malla.pdf} 
		\subcaption{}\label{fig:malla}
	\end{subfigure}%%
	\vspace{4ex}
	\begin{subfigure}[b]{0.49\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/Malla_i.pdf} 
		\subcaption{}\label{fig:malla_i}
		%\vspace{4ex}
	\end{subfigure}
	\begin{subfigure}[b]{0.49\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/Malla_s.pdf} 
		\subcaption{}\label{fig:malla_s}
	\end{subfigure}%%

	\caption[Mallas generadas mediante el algoritmo de deposición virtual.]{Ejemplo de una malla sencilla de tres capas y reducido número de fibras (para su mejor visualización) generada mediante el algoritmo de deposición virtual descripto ($\adLcapa=100$, $\tilde{l_s}=5$, $\devangmax=\ang{20}$). b) Las fibras en su estado de deposición, sin intersecciones (barra de color indicando $\lamer$). b) Idem pero con escala de grises indicando a qué capa corresponde cada fibra. b) Las fibras intersectadas (barra de color indicando $\lamer$ para cada fibra comprendida entre dos puntos de unión). c) Líneas extremo-extremo de las fibras entre puntos de unión (barra de color indicando $\lamer$).}
	\label{fig:malla_ejemplo} 
\end{figure}
