Las enfermedades cardiovasculares son la principal causa de muerte a nivel mundial. 
Cada año se pierden 17 millones de vidas por causa de estas enfermedades, lo que representa un 29\% de la totalidad de las muertes globales. 
En particular, las enfermedades coronarias representan un 50\% de las causas de muerte por enfermedades cardiovasculares \cite{Seifu2013a}.

Los tejidos arteriales son un subgrupo de los tejidos vasculares, que incluyen también a los capilares y las venas.
Dado que las afecciones arteriales representan un mayor riesgo en términos de salud y poseen mayor impacto e incidencia que las enfermedades vasculares venosas y capilares, considerables esfuerzos se han aplicado en el estudio y la búsqueda de estrategias para su reemplazo y regeneración.

Dado que, en gran medida, las afecciones de la pared arterial están en relación a sus propiedades mecánicas, a lo que se suma que las probabilidades de éxito en las intervenciones aumentan cuando los materiales de reemplazo muestran propiedades biomiméticas con los tejidos fisiológicos, es necesario caracterizar el comportamiento mecánico y constitutivo de dichos tejidos. En un sentido descriptivo macroscópico, la estructura de la pared arterial se compone de tres capas distintas: la \concepto{intima}, la \concepto{media}, y la \concepto{adventicia }(figura \ref{fig:estructura_pared_arterial}).
La \concepto{intima} es la capa interna y se compone de una capa de células endoteliales soportada por una fina membrana basal y una capa subendotelial.
Cumple la función de controlar la transferencia molecular desde el flujo sanguíneo hacia el interior de la pared y evitar la trombogénesis, además de estar involucrada en el mantenimiento de la homeostasis, la regulación del tono muscular, y la regulación inmunogénica e inflamatoria.
La \concepto{media}, como su nombre lo indica, es la capa del medio de la pared arterial, y está compuesta por células de músculo liso orientadas circunferencialmente en conjunto con un arreglo doble helicoidal de fibras de colágeno embebidas en una matriz de elastina.
La \concepto{adventicia} es la capa externa de la arteria y consiste principalmente de fibroblastos y fibrocitos (células que sintetizan colágeno y elastina), sustancia fundamental y haces de fibrillas de colágeno conformando un tejido fibroso \cite{HolzapfelOgden2010}.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.8\linewidth]{../Imagenes/01-Tejidos/estructura_pared_arterial}
	\caption[Estructura de la pared arterial]{Diagrama de la estructura en capas de las arterias, indicando los principales elementos constituyentes de cada una de las tres capas: intima, media y adventicia. Figura adaptada de \cite{HolzapfelGasserOgden2000}}
	\label{fig:estructura_pared_arterial}
\end{figure}


Resulta importante resaltar que, desde el punto de vista mecánico, los elementos constituyentes relevantes son las células de músculo liso, la matriz de elastina y las fibras de colágeno.
Las células de músculo liso poseen capacidad contráctil y son las responsables, por lo tanto, de regular el tamaño del vaso. 
La elastina es altamente distensible y le confiere a las arterias una capacidad de deformación elástica elevada a bajas presiones, mientras que las fibras de colágeno, que se encuentran enruladas en reposo, devienen rectas y mecánicamente activas a altas presiones, protegiendo y reforzando el vaso \cite{Bouten2011,Rhodin1980}.

Hoy en día, se concentra el mayor esfuerzo sobre el estudio de las arterias musculares (que poseen un diámetro menor a \SI{6}{\milli\meter}), haciendo especial foco en las arterias coronarias, dado que los tratamientos actuales frente a enfermedades coronarias presentan serios inconvenientes.

Las principales causas de enfermedad coronaria son la trombosis y aterosclerosis.
La trombosis es la obstrucción del flujo sanguíneo en algún sector del sistema circulatorio debido a la formación de un coágulo en el interior de un vaso sanguíneo.
La formación de un coágulo toma lugar normalmente como respuesta natural frente a una lesión del endotelio, aunque puede ocurrir también frente a factores hereditarios o debido a enfermedades de la sangre.
Cuando un coágulo se libera en el torrente sanguíneo se lo denomina émbolo, por lo cual es también usual la denominación \concepto{tromboembolismo} cuando se produce la oclusión del flujo.
Cuando la trombosis ocurre en arterias (trombosis arterial) se ve afectada la irrigación sanguínea de los tejidos, pudiendo derivar en isquemias y necrosis \cite{Furie2008}.
La aterosclerosis, en cambio, consiste en el crecimiento de placas de ateroma dentro de la capa intima de la pared arterial, compuestas de grasa, colesterol, calcio y otras sustancias provenientes de la sangre.
El material acumulado engrosa la pared arterial que se proyecta hacia el interior del vaso, reduciendo la luz arterial (estenosis) y restringiendo el flujo sanguíneo.
En casos severos puede devenir en enfermedad coronaria, infarto, enfermedad vascular periférica o problemas renales, dependiendo de la arteria afectada.
Aunque generalmente comienza a edades tempranas, prácticamente todas las personas sufren algún grado de aterosclerosis a partir de los 65 años de edad, tratándose además, de la principal causa de muerte y morbilidad en el mundo desarrollado \cite{TheTopolSolution}.
Evidentemente, estas afecciones no son mutuamente excluyentes y pueden ocurrir en simultáneo con efectos sinérgicos: la estenosis generada por la acumulación de ateroma resulta un lugar propicio para el alojamiento de un émbolo trombótico, derivando en un tromboembolismo (ver figura \ref{fig:tromboembolismo}).
El infarto de miocardio ocurre cuando disminuye
bruscamente el flujo coronario después de una oclusión trombótica de una
arteria coronaria, ya estrechada por aterosclerosis (figura \ref{fig:tromboembolismo}).

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.5\linewidth]{../Imagenes/01-Tejidos/tromboembolismo}
	\caption[Tromboembolismo]{El diagrama muestra el bloqueo del flujo sanguíneo provocado por un coágulo suelto en el torrente sanguíneo (émbolo trombótico) alojado en la reducción del lumen (estenosis) provocada por la acumulación de materia grasa (placa de ateroma) en la capa intima de la pared arterial.}
	\label{fig:tromboembolismo}
\end{figure}

Si bien el principal foco de ataque contra estas enfermedades es la prevención, en los casos en que la oclusión o estenosis son severos existen una serie de tratamientos que se pueden realizar.
Cuando es posible, se realiza un tratamiento de trombólisis con el fin de reestablecer de forma inmediata la permeabilidad de la arteria coronaria, mediante la administración intravenosa de agentes trombolíticos de activación tisular del plasminógeno (tPA), estreptoquinasa y complejo anisoilado del activador del plasminógeno y la estreptoquinasa (APSAC). 
Estos agentes facilitan la conversión del plasminógeno en plasmina, que posteriormente disuelve los trombos de fibrina.
En cambio, cuando los medicamentos trombolíticos se encuentran contraindicados, se requiere de intervenciones invasivas de revascularización como la angioplastía o la técnica de \textit{bypass}.

La angioplastía busca recuperar la luz arterial en arterias con estenosis causadas, típicamente, por aterosclerosis.
Es un tratamiento invasivo endovascular que consiste en la introducción remota, a través de las arterias femoral o radial, de una guía flexible con un balón inflable dentro de la arteria coronaria afectada.
El balón se infla repetidas veces en la zona de la estenosis hasta que la obstrucción desaparece o disminuye.
Bajo ciertas condiciones se puede colocar, en el mismo procedimiento, un conducto expansible de metal denominado \textit{stent} que refuerza mecánicamente la zona afectada para evitar estenosis residual y, al mismo tiempo, reducir la probabilidad de futuras reestenosis \cite{Libby1998,Willerson1991}.
Otra posible solución es la cirugía de derivación arterial coronaria (o \textit{bypass} coronario), que consiste en reestablecer el flujo sanguíneo en una arteria obstruida por medio de una conexión artificial por medio de un injerto vascular entre la aorta y la arteria coronaria afectada.
