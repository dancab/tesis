Para validar el modelo micromecánico propuesto se comprueba la capacidad de reproducir correctamente la respuesta mecánica de las mallas electrohiladas.
Para ello se compara la respuesta homogeneizada obtenida de simulaciones con los datos experimentales ya presentados de ensayos mecánicos de tracción uniaxial (figura \ref{fig:curvas_experimentales_123}).

Los parámetros geométricos para la construcción del RVE se toman para reproducir correctamente las distribuciones de orientación y reclutamiento, mientras que se adopta para todas las fibras el diámetro medio (tabla \ref{tab:parametros_geometria}).
El módulo elástico de la fibras rectas a la tracción se fija en \SI{3.0}{\giga Pa}, valor que ha sido reportado en la literatura para fibras de PLLA con diámetros cercanos al valor medido \cite{Inai2005, MorelA2018}.
El módulo elástico de las fibras enruladas, si bien es crucial para evitar indeterminaciones en los desplazamientos, es mucho menor que el de las fibras rectas y posee una baja incidencia en la respuesta mecánica tal como se vio en el capítulo \ref{cap:modelo_multiescala}.
En este apartado se adopta un valor de \SI{3.0}{\mega Pa}.
El resto de los parámetros ($\dotepso$, $\soplast$, $\nhard$, $\tfrot$) son optimizados por cuadrados mínimos para obtener un buen ajuste entre la curva tensión-deformación simulada y los datos experimentales (tabla \ref{tab:parametros_optimizados}).
La respuesta mecánica simulada reproduce fielmente las curvas de tensión vs. deformación obtenidas experimentalmente, incluyendo el módulo elástico, tensión de fluencia, endurecimiento por plasticidad y tensión de rotura (figura \ref{fig:Comparacion_ajuste}).

Se halla también una discrepancia en la pendiente de la curva posterior a la rotura de las primeras fibras.
Mientras que en las curvas experimentales se tiene un descenso pronunciado de la tensión indicando un rompimiento brusco de un gran número de fibras, en los resultados computacionales se obtiene un rompimiento gradual de fibras, otorgando un descenso igualmente gradual de la tensión.
Es factible que esta diferencia se deba al hecho de haber realizado las simulaciones sobre un RVE en lugar de sobre un componente macroscópico que emule macroscópicamente a la probeta ensayada, donde el efecto del encuellamiento genera un aumento localizado de la deformación, resultando en un rompimiento más brusco.

\begin{table}[ht]
	\centering
	\small
	\caption[Parámetros del algoritmo de deposición virtual de fibras para validar el modelo micromecánico.]{Parámetros del algoritmo de deposición virtual de fibras utilizados para generar los RVE necesarios para validar el modelo micromecánico.}
	\begin{tabular}{L L L L L L L}
		\hline
		\rowcolor{Gray}
		$\Dm [\si{\micro \meter}]$ & $\fv$ & $\adLcapa$ & $\adls$ & $\devangmax$ & $N_c$ & $\fdop$ \\
		\hline
		\num{0.45} & \num{0.1} & \num{250} & \num{5} & \ang{17} & \num{5} & $\pi^{-1}$ \\
		\hline
	\end{tabular}
	\label{tab:parametros_geometria}
\end{table}

\begin{table}[ht]
	\centering
	\small
	\caption[Parámetros optimizados para ajustar la respuesta en tensión-deformación con datos experimentales.]{Parámetros optimizados para reproducir la respuesta en tensión-deformación de ensayos experimentales de tracción uniaxial.}
	\begin{tabular}{L L L L L L}
		\hline
		\rowcolor{Gray}
		$\Et [\si{\mega Pa}]$ & $\Eb [\si{\mega Pa}]$ & $\dotepso [\si{1/\second}]$ & $\soplast [\si{\mega Pa}]$ & $\nhard$ & $\tfrot [\si{\mega Pa}]$ \\
		\hline
		\num{3.0e3}	 & \num{3.0} & \num{1e-3} & \num{17.0} & \num{0.5} & \num{36} \\
		\hline
	\end{tabular}
	\label{tab:parametros_optimizados}
\end{table}

\begin{figure}[p] 
	\centering
	\begin{subfigure}[b]{0.7\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/04-Mecanica/curva_ajustada.pdf} 
		\subcaption{}\label{fig:curva_ajustada}
		%\vspace{4ex}
	\end{subfigure}

	\begin{subfigure}[b]{0.7\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/04-Mecanica/curva_ajustada_fibra.pdf} 
		\subcaption{}\label{fig:curva_ajustada_fibra}
	\end{subfigure}%%
	
	\caption[Comparación de la respuesta homogeneizada con datos experimentales.]{Comparación de la respuesta homogeneizada del RVE con curvas de tensión-deformación experimentales. a) La curva tensión-deformación simulada muestra muy buen acuerdo con las curvas obtenidas de ensayos experimentales. El ajuste con la curva promedio arroja un $R^2=0.9907$ (sin considerar la sección posterior a la rotura). b) Curva constitutiva correspondiente una fibra individual con los parámetros optimizados.}
	\label{fig:Comparacion_ajuste} 
\end{figure}

Es importante resaltar que esta respuesta se obtiene con una geometría de RVE realista que concuerda con las distribuciones medidas de orientación, diámetro y enrulamiento.
Además el valor optimizado para la fracción de volumen resulta razonable ya que se encuentra en el rango reportado por estudios experimentales sobre matrices electrohilada de PLLA ($\fv>80\%$) \cite{Yang2004}.
Asimismo, la curva constitutiva para cada nanofibra (figura \ref{fig:curva_ajustada_fibra}) permanece cercana a lo reportado para nanofibras individuales de PLLA de diámetros similares \cite{Inai2005}.
Esta cuestión refuerza la capacidad del modelo multiescala como herramienta de diseño, ya que logra relacionar de forma acertada las variables microestructurales y micromecánicas con la respuesta mecánica macroscópica.
