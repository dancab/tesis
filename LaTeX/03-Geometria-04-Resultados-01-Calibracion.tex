En esta sección se establece el tamaño mínimo de malla para que la misma pueda ser tratada como un RVE.
Para cada tamaño de malla se construye un conjunto de 10 mallas generadas mediante el algoritmo de deposición virtual, utilizando los mismos parámetros de entrada.
Luego, se estudia la variabilidad estadística de las variables de interés para cada set de parámetros calculando su valor medio y dispersión estándar dentro de cada conjunto, tomando el tamaño de RVE como la variable independiente.

En primera instancia se desea determinar el tamaño mínimo de recinto $\adLcapa$ que permite obtener geometrías estadísticamente repetitivas.
Para ello, se selecciona como variable a medir la densidad superficial de intersecciones ($\denin$) y se mide el coeficiente de variación estadística como la relación entre la desviación estándar ($\deninm$) y el valor medio ($\denins$) en un conjunto de 10 geometrías generadas bajo un mismo set de parámetros.

Para el primer conjunto se toman mallas isotrópicas de fibras rectas de \SI{1}{\micro\meter} de diámetro: $\devangmax=0$, $\fdop(\theta)=\pi^{-1}$, $\Dm=\SI{1}{\micro\meter}$, mientras que el tamaño de recinto $\adLcapa$ se adopta como variable independiente.
Para este caso la longitud de cada segmento $\adls$ se vuelve irrelevante.
La figura \ref{fig:calibracion_rve_1capa_densidad_vs_L} muestra los resultados obtenidos para $\deninm$ vs. $\adLcapa$  y para $\coefvar$ versus $\adLcapa$, donde puede observarse que la dispersión estándar disminuye a medida que se aumenta el tamaño de recinto.
Este resultado es importante ya que sugiere que se puede obtener menor variabilidad estadística en la construcción de las geometrías controlando el tamaño de las mismas.
De esta forma es posible alcanzar una repetitividad estadística deseada dependiendo de la aplicación.
Por ejemplo, para obtener $\coefvar < 4\%$ es necesario que $\adLcapa \geq 100$.


\begin{figure}[ht]
	\centering 
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/calibracion_rve_1capa_densidad_vs_L_errorbars.pdf} 
		\subcaption{}\label{fig:calibracion_rve_1capa_densidad_errorbars}
		%\vspace{4ex}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/calibracion_rve_1capa_densidad_vs_L_varcoeff.pdf} 
		\subcaption{}\label{fig:calibracion_rve_1capa_densidad_varcoeff}
	\end{subfigure}%%
	
	\caption[Variabilidad estadística de la densidad superficial de intersecciones en función del tamaño de una malla de fibras de una sola capa.]{Variabilidad estadística de la densidad superficial de intersecciones en función del tamaño de una malla de fibras de una sola capa. a) Valor medio $\deninm$ (círculos) y dispersión estándar $\denins$ (barras de error) de la densidad de intersecciones. Se observa que la dispersión se reduce a medida que se aumenta $\adLcapa$. b) Coeficiente de variación $\coefvar=\denins/\deninm$ versus $\adLcapa$. Se observa que para $\adLcapa=100$ la variabilidad cae por debajo de $4\%$.}
	\label{fig:calibracion_rve_1capa_densidad_vs_L} 
\end{figure}

Si se consideran mallas conformadas por más capas, las intersecciones que se producen entre capas adyacentes entre sí, no incluidas en el caso de una sola capa, provoca valores más elevados para $\denin$.
La figura \ref{fig:calibracion_rve_denin_vs_ncaps_no_period} muestra este aumento en función del número de capas para mallas generadas con $\fv=0.3$, $\Dm=\SI{1}{\micro\meter}$, $\Lcapa=50$, $\devangmax=0$, $\adls=1$.
Es interesante notar que las capas intermedias incrementan en mayor medida el número de intersecciones que las capas exteriores, puesto que cada capa intermedia es adyacente a dos capas mientras que cada capa exterior es adyacente a solo una.
Como resultado se tiene que al aumentar el número de capas, el valor de $\denin$ aumenta de forma monótona pero asintóticamente a un valor límite.
Este comportamiento conlleva la necesidad de generar mallas con un elevado número de capas (en el caso de la figura, mayor a 20) para mantener una buena representación de una matriz real respecto de la densidad de intersecciones.
No obstante, también hay que mantener en consideración el costo computacional asociado, tanto para la generación de la geometría como para su posterior análisis numérico y su implementación en un modelo multiescala mecánico.


\begin{figure}[ht] 
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/calibracion_rve_denin_vs_ncaps_no_period_errorbars.pdf} 
		\subcaption{}\label{fig:calibracion_rve_denin_vs_ncaps_no_period_errorbars}
		%\vspace{4ex}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/calibracion_rve_denin_vs_ncaps_no_period_varcoeff.pdf} 
		\subcaption{}\label{fig:calibracion_rve_denin_vs_ncaps_no_period_varcoeff}
	\end{subfigure}%%
	
	\caption[Dependencia de la densidad superficial de intersecciones respecto del número de capas de una malla virtual.]{Dependencia de la densidad superficial de intersecciones respecto del número de capas de una malla virtual considerando 1, 5, 10, 20 y 40 capas ($\adLcapa=50$). a) Valor medio (círculos) y dispersión estándar (barras de error) de $\denin$ versus $N_c$. b) Coeficiente de variación de $\denin$ versus $N_c$.}
	\label{fig:calibracion_rve_denin_vs_ncaps_no_period} 
\end{figure}

Como solución es posible tomar un número reducido de capas, pero aplicando una condición de periodicidad sobre las intersecciones, de forma que las fibras de la capa inferior intersecten a las fibras de la capa superior.
De esta manera se obtiene un valor de $\denin$ independiente del número de capas, con excepción del caso obvio de una sola capa (figura \ref{fig:calibracion_rve_denin_vs_ncaps_period}).

\begin{figure}[ht] 
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/calibracion_rve_denin_vs_ncaps_period_errorbars.pdf} 
		\subcaption{}\label{fig:calibracion_rve_denin_vs_ncaps_period_errorbars}
		%\vspace{4ex}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/calibracion_rve_denin_vs_ncaps_period_varcoeff.pdf} 
		\subcaption{}\label{fig:calibracion_rve_denin_vs_ncaps_period_varcoeff}
	\end{subfigure}%%
	
	\caption[Dependencia de la densidad superficial de intersecciones respecto del número de capas de una malla virtual con periodicidad en intersecciones.]{Dependencia de la densidad superficial de intersecciones respecto del número de capas de una malla virtual con la condición periódica de intersecciones, considerando 1, 5, 10, 20 y 40 capas ($\adLcapa=50$). a) Exceptuando el caso de una sola capa, se observa que, debido a la periodicidad en intersecciones, el valor de $\denin$ se mantiene constante sin importar el número de capas b) La variabilidad estadística se mantiene respecto del caso sin periodicidad.}
	\label{fig:calibracion_rve_denin_vs_ncaps_period} 
\end{figure}

Además, dado que un mayor número de capas implica un mayor número de fibras, se observa una marcada disminución en la variabilidad estadística al aumentar el número de capas.
Esto puede apreciarse en la figura \ref{fig:calibracion_rve_10capas_densidad_vs_L_varcoeff} donde se ve que para $\Lcapa=100$ el valor de $\coefvar$ cae por debajo del $1\%$, mientras que para una sola capa se mantenía por encima del $3\%$.

\begin{figure}[ht] 
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/calibracion_rve_10capas_densidad_vs_L_errorbars.pdf} 
		\subcaption{}\label{fig:calibracion_rve_10capas_densidad_vs_L_errorbars}
		%\vspace{4ex}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/calibracion_rve_10capas_densidad_vs_L_varcoeff.pdf} 
		\subcaption{}\label{fig:calibracion_rve_10capas_densidad_vs_L_varcoeff}
	\end{subfigure}%%
	
	\caption[Variabilidad estadística de la densidad superficial de intersecciones en función del tamaño de una malla de fibras de diez capas.]{Variabilidad estadística de la densidad superficial de intersecciones en función del tamaño de una malla de fibras de diez capas. a) Valor medio (círculos) y dispersión estándar (barras de error) de $\denin$ vs. $\adLcapa$. b) Coeficiente de variación vs. $\adLcapa$.}
	\label{fig:calibracion_rve_10capas_densidad_vs_L} 
\end{figure}

Adicionalmente, puede introducirse como variable la tortuosidad de las fibras considerando mallas generadas con $\devangmax>0$.
En este caso, como se mostró en la figura \ref{fig:N_vs_L_and_tortuosity}), el número de fibras es menor para un mismo $\Lcapa$ dado que cada fibra enrulada ocupa más volumen que una fibra recta.
Aún así, el efecto de la disminución de $N_f$ no es apreciable en la variabilidad estadística de las geometrías generadas desde el punto de vista de $\denin$ (figura \ref{fig:calibracion_rve_1capa_devang10_enrulada_densidad}).

%\begin{figure}[ht] 
%	\centering
%	\begin{subfigure}[b]{0.4\linewidth}
%		\centering
%		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/calibracion_rve_1capa_devang05_densidad_errorbars.pdf} 
%		\subcaption{}\label{fig:calibracion_rve_1capa_devang05_densidad_errorbars}
%		%\vspace{4ex}
%	\end{subfigure}
%	\begin{subfigure}[b]{0.4\linewidth}
%		\centering
%		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/calibracion_rve_1capa_devang05_densidad_varcoeff.pdf} 
%		\subcaption{}\label{fig:calibracion_rve_1capa_devang05_densidad_varcoeff}
%	\end{subfigure}%%
%
%	\begin{subfigure}[b]{0.4\linewidth}
%	\centering
%	\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/calibracion_rve_1capa_devang10_densidad_errorbars.pdf} 
%	\subcaption{}\label{fig:calibracion_rve_1capa_devang10_densidad_errorbars}
%	%\vspace{4ex}
%	\end{subfigure}
%	\begin{subfigure}[b]{0.4\linewidth}
%		\centering
%		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/calibracion_rve_1capa_devang10_densidad_varcoeff.pdf} 
%		\subcaption{}\label{fig:calibracion_rve_1capa_devang10_densidad_varcoeff}
%	\end{subfigure}%%
%	
%	\caption[Variabilidad estadística de la densidad superficial de intersecciones en función del tamaño de una malla de fibras enruladas.]{Variabilidad estadística de la densidad superficial de intersecciones en función del tamaño de una malla de fibras enruladas. (a) y (b) corresponden a $\devangmax=\ang{5}$ mientras que (c) y (d) corresponden a $\devangmax=\ang{10}$. Comparando con \ref{fig:calibracion_rve_1capa_densidad_vs_L} puede observarse que la tortuosidad de las fibras no afecta la variabilidad estadística de la geometría generada respecto de la densidad superficial de intersecciones.}
%	\label{fig:calibracion_rve_1capa_enrulada_densidad} 
%\end{figure}

%\begin{figure}[ht] 
%	\centering
%	\begin{subfigure}[b]{0.45\linewidth}
%		\centering
%		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/calibracion_rve_1capa_devang05_densidad_errorbars.pdf} 
%		\subcaption{}\label{fig:calibracion_rve_1capa_devang05_densidad_errorbars}
%		%\vspace{4ex}
%	\end{subfigure}
%	\begin{subfigure}[b]{0.45\linewidth}
%		\centering
%		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/calibracion_rve_1capa_devang05_densidad_varcoeff.pdf} 
%		\subcaption{}\label{fig:calibracion_rve_1capa_devang05_densidad_varcoeff}
%	\end{subfigure}%%
%	
%	\caption[Variabilidad estadística de la densidad superficial de intersecciones en función del tamaño de una malla de fibras enruladas.]{Variabilidad estadística de la densidad superficial de intersecciones en función del tamaño de una malla de fibras enruladas ($\devangmax=\ang{5}$). Comparando con \ref{fig:calibracion_rve_1capa_densidad_vs_L} puede observarse que la tortuosidad de las fibras no afecta la variabilidad estadística de la geometría generada respecto de la densidad superficial de intersecciones.}
%	\label{fig:calibracion_rve_1capa_devang05_enrulada_densidad} 
%\end{figure}

\begin{figure}[ht] 
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/calibracion_rve_1capa_devang10_densidad_errorbars.pdf} 
		\subcaption{}\label{fig:calibracion_rve_1capa_devang10_densidad_errorbars}
		%\vspace{4ex}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/calibracion_rve_1capa_devang10_densidad_varcoeff.pdf} 
		\subcaption{}\label{fig:calibracion_rve_1capa_devang10_densidad_varcoeff}
	\end{subfigure}%%
	
	\caption[Variabilidad estadística de la densidad superficial de intersecciones en función del tamaño de una malla de fibras enruladas.]{Variabilidad estadística de la densidad superficial de intersecciones en función del tamaño de una malla de fibras enruladas ($\devangmax=\ang{10}$). Comparando con \ref{fig:calibracion_rve_1capa_densidad_vs_L} puede observarse que la tortuosidad de las fibras no afecta la variabilidad estadística de la geometría generada respecto de la densidad superficial de intersecciones.}
	\label{fig:calibracion_rve_1capa_devang10_enrulada_densidad} 
\end{figure}

\subsubsection{Distribución de orientaciones}

También es importante calibrar el tamaño mínimo del RVE para obtener baja variabilidad en la distribución de orientaciones obtenida.
Para ello se generan mallas de fibras rectas tomando una FDO prescripta ($\fdop$) normal truncada:

\begin{equation}
	\fdop(\theta) = 
		\begin{cases}
			\displaystyle 0 \quad &if \quad \theta < 0 \\
			\parent{\frac{1}{C}} \displaystyle e^{\sqbrac{-\frac{\parent{\theta-{\fdopm}}^2}{2 {\parent{\fdops}}^2}}} \quad &if \quad  0 \leq \theta \leq \pi \\
			\displaystyle 0 \quad &if \quad \theta > \pi
		\end{cases}
\end{equation}
tomando $\fdopm=\pi/2$ y $\fdops=\pi/5$.

%\begin{equation}
%	\fdop = \frac{1}{\fdopsigma \sqrt{2\pi}} e^{-\frac{1}{2}\parent{\frac{\theta-\fdopmu}{\fdopsigma}}^2}
%\end{equation}
%tomando $\fdopmu=0.5\pi$ y $\fdopsigma=0.2\pi$.

Para este análisis se toma como variable independiente el número de fibras y se evalúa el coeficiente de variación para los dos parámetros de la distribución obtenida ($\fdoom$ y $\fdoos$) para 10 geometrías por cada valor de $N_f$.
Los resultados muestran cómo disminuye la variabilidad estadística de las geometrías generadas a medida que se aumenta el número de fibras en la malla, siendo mayor la variabilidad para $\fdoos$ que para $\fdoom$.
También se observa que para $N_f=200$ se obtienen coeficientes de variación por debajo de $5\%$ para los dos parámetros (figura \ref{fig:calibracion_rve_orientdistr_varcoeffs_vs_Nf}).

\begin{figure}[p] 
	\centering
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/calibracion_rve_orientdistr_mu_varcoeff_vs_Nf.pdf} 
		\subcaption{}\label{fig:calibracion_rve_orientdistr_mu_varcoeff_vs_Nf}
		%\vspace{4ex}
	\end{subfigure}

	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/calibracion_rve_orientdistr_sigma_varcoeff_vs_Nf.pdf} 
		\subcaption{}\label{fig:calibracion_rve_orientdistr_sigma_varcoeff_vs_Nf}
	\end{subfigure}%%

	\begin{subfigure}[b]{0.45\linewidth}
	\centering
	\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/calibracion_rve_orientdistr_comparation.pdf} 
	\subcaption{}\label{fig:calibracion_rve_orientdistr_comparation}
	\end{subfigure}%%
	
	\caption[Variabilidad estadística de la distribución de orientaciones en función del número de fibras en la malla.]{Variabilidad estadística de la distribución de orientaciones en función del número de fibras en la malla. a) Coeficiente de variación para $\fdoom$ versus $N_f$. b) Coeficiente de variación para $\fdoos$ versus $N_f$. c) Comparación entre la FDO prescripta (negro) y la obtenida (histograma normalizado, gris) para una malla de 200 fibras.}
	\label{fig:calibracion_rve_orientdistr_varcoeffs_vs_Nf} 
\end{figure}

%La figura \ref{fig:calibracion_rve_orientdistr_comparation} muestra una comparación entre la densidad de probabilidad prescripta y la distribución obtenida para una malla de 200 fibras, donde se observa una buena concordancia.

%\begin{figure}[h!] 
%	\centering
%	\includegraphics[width=0.5\linewidth]{../Imagenes/03-Geometria/calibracion_rve_orientdistr_comparation.pdf} 
%	\caption[Distribuciones de orientaciones prescripta y obtenida para la calibración del RVE.]{Distribución de orientaciones prescripta (distribución gaussiana con valor medio $\fdopmu=0.5\pi$ y desviación estándar $\fdopsigma=0.2\pi$, en negro) comparada junto con un ejemplo de una distribución de orientaciones obtenida (histograma normalizado, en gris) para una geometría de 200 fibras.}
%	\label{fig:calibracion_rve_orientdistr_comparation} 
%\end{figure}

En este caso se tomó como variable independiente al número de fibras $N_f$ que a su vez depende del tamaño de recinto $\Lcapa$ y del número de capas tal como indica la figura \ref{fig:N_vs_L_and_tortuosity}.
Para $\fv=0.3$, El número de fibras puede estimarse de forma aproximada como $N_f = 0.4 \adLcapa N_c$, de forma que para una geometría de 200 fibras se puede realizar generando mallas de 10 capas con $\adLcapa=50$. Este tamaño cumple además con una baja variabilidad para la densidad superficial de intersecciones (\ref{fig:calibracion_rve_10capas_densidad_vs_L_varcoeff}).

\begin{figure}[p]
	\centering
	\includegraphics[width=0.5\linewidth]{../Imagenes/03-Geometria/N_vs_L_and_tortuosity}
	\caption[Número de fibras en función del tamaño de la malla.]{Número de fibras en una capa de fibras ($N_f$) en función del largo del recinto cuadrado ($\Lcapa$) y de la tortuosidad de las fibras, tomando constante la fracción de volumen como $\fv=0.3$. Se observa que la cantidad de fibras sigue una relación lineal con cierta variabilidad propia del algoritmo, y que mallas con fibras más tortuosas (c) poseen en general un menor número de fibras que mallas con fibras rectas (a).}
	\label{fig:N_vs_L_and_tortuosity}
\end{figure}

\subsubsection{Distribución de reclutamiento}

La variabilidad estadística de la distribución de reclutamiento obtenida depende en gran medida del valor de $\devangmax$.
Valores pequeños implican mallas con fibras más rectas y baja variabilidad mientras que valores más altos generan mallas con fibras más enruladas, dando lugar también a mayor variabilidad tanto dentro de una malla como entre mallas generadas bajo los mismos parámetros (figura \ref{fig:calibracion_rve_fdr_varcoeff_vs_devangmax}).
Mientras que $\fdrom$ posee muy baja variabilidad para todos los valores de $\devangmax$ estudiados, el coeficiente de variación para $\fdros$ se mantiene por debajo de $5\%$ hasta $\devangmax=\ang{30}$.
Esto indica que si se quieren construir RVEs con distribuciones de tortuosidad más elevadas que las que se obtienen para $\devangmax=\ang{30}$, será necesario incrementar el tamaño de las mismas para mejorar la representatividad estadística.

\begin{figure}[p] 
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/calibracion_rve_fdr_mu_varcoeff_vs_devangmax.pdf} 
		\subcaption{}\label{fig:calibracion_rve_fdr_mu_varcoeff_vs_devangmax}
		%\vspace{4ex}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=1.0\linewidth]{../Imagenes/03-Geometria/calibracion_rve_fdr_sigma_varcoeff_vs_devangmax.pdf} 
		\subcaption{}\label{fig:calibracion_rve_fdr_sigma_varcoeff_vs_devangmax}
	\end{subfigure}%%
	
	\caption[Variabilidad estadística de la distribución de reclutamiento en función del ángulo de desviación máximo.]{Variabilidad estadística de la distribución de reclutamiento en función del ángulo de desviación máximo del algoritmo de deposición virtual. a) Coeficiente de variación para el valor medio de la distribución de reclutamiento. b) Coeficiente de variación para la dispersión estándar de la distribución de reclutamiento.}
	\label{fig:calibracion_rve_fdr_varcoeff_vs_devangmax} 
\end{figure}
