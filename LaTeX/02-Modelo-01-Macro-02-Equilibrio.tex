Se designa mediante $\bRegs$ al contorno del cuerpo en su configuración espacial $\Regs$.
A su vez, se divide este contorno en $\bRegsD$ y $\bRegsN$, denotando respectivamente al borde de Dirichlet y al borde de Neumann, de forma que $\bRegs = \bRegsD \cup \bRegsN$, cumpliéndose también que $\bRegsD \cap \bRegsN = \emptyset$\footnote{$\emptyset$ denota a un conjunto de medida nula.}.
En $\bRegsD$ se prescriben los valores de los desplazamientos $\UD$, mientras que en $\bRegsN$ se prescribe el valor de la tracción $\tN$ (figura \ref{fig:equilibrio_macro_esquema}).

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.6\linewidth]{../Imagenes/02-Modelo/equilibrio_macro_esquema}
	\caption[Esquema del equilibrio macroscópico]{Equilibrio del cuerpo continuo de la escala macroscópica: el borde $\bRegs$ se subdivide en sus partes de Dirichlet $\bRegsD$ y Neumann $\bRegsN$. En el borde de Dirichlet se prescribe el campo de desplazamientos $\UD$ y en el borde de Neumann se prescriben las tracciones $\tN$.}
	\label{fig:equilibrio_macro_esquema}
\end{figure}

Luego, se plantea el equilibrio mecánico en términos del Principio de Potencias Virtuales (PPV), que a su vez se enmarca en el campo de la mecánica variacional \cite{Blanco2016b,deSouza2008,deSouza2011}.
En este contexto resulta fundamental definir los conjuntos de desplazamientos cinemáticamente admisibles:
el campo de desplazamientos espacial $\u$ que caracteriza la deformación del cuerpo, pertenece a un espacio funcional $\setUs$, con funciones suficientemente regulares para que las operaciones matemáticas estén correctamente definidas.
Generalmente se tiene que $\setUs = \mathbf{H}^1(\Regs)$, es decir, el espacio de funciones con gradientes de cuadrado integrable en $\Regs$.

Se define, además, a $\kinUs \in \setUs$ como el conjunto de desplazamientos cinemáticamente admisibles, siendo los desplazamientos que satisfacen las restricciones cinemáticas sobre la frontera de Dirichlet:

\begin{equation}
	\kinUs = \braces{\w \in \setUs ; \evalat{\w}{\bRegsD}=\overline{\w}}
\end{equation}

Este espacio puede considerarse como la traslación de otro subespacio $\varUs$, llamado espacio de desplazamientos variacionalmente admisibles, cuyos elementos son nulos en la frontera de Dirichlet:

\begin{equation}
\varUs = \braces{\w \in \setUs ; \evalat{\w}{\bRegsD}=\vcero}
\end{equation}

Una vez correctamente definidos estos conjuntos, según el PPV, el problema de equilibrio mecánico queda planteado en la configuración espacial como:

\begin{equation} \label{eq:PPV_descr_esp_conocida}
	\int_{\Regs} \T \cdot \gradS \vu \ \dRegs = \int_{\Regs} \vector{g} \cdot \vu \ \dRegs + \int_{\bRegs^N} \tN \cdot \vu \ \dbRegs^N \quad \forall \vu \in \varUs
\end{equation}
donde $\T$ es el tensor de tensiones de Cauchy, $\vu$ una acción de movimiento variacionalmente admisible (también llamada velocidad virtual), $\gradS \vu$ el gradiente simétrico de $\vu$ con respecto a las coordenadas espaciales, $\tN$ la tracción impuesta en la frontera de Neumann y $\vector{g}$ las fuerzas externas de volumen.

Cabe notar que la condición de incompresibilidad ($\det{\F}=1$) no ha sido impuesta ya que se va a tratar con materiales altamente porosos en los que la variación a nivel microscópico del tamaño de los poros resultará en una contracción o dilatación volumétrica a nivel macroscópico.
Además no se han tenido en cuenta efectos inerciales.
Su incorporación al problema podría efectuarse con relativa facilidad en caso de ser necesario, aunque esto iría en perjuicio de la claridad en el desarrollo subsiguiente.

En el apéndice \ref{ch:ap_ppv} se da una descripción completa del PPV, incluyendo su linealización para la implementación en esquemas de Newton-Raphson.

%Dado que se tratará con el caso en que la configuración espacial $\Regs$ es desconocida, conviene expresar el equilibrio respecto de la configuración material $\Regm$, que sí será conocida.
%Es necesario tener en cuenta que en este caso se tiene por variable independiente al campo de desplazamientos material, y se trabaja con los conjuntos $\setUm$, $\kinUm$ y $\varUm$, que son las contrapartes, respectivamente, de $\setUs$, $\kinUs$ y $\varUs$.
%
%\begin{equation} \label{eq:PPV_descr_mat_conocida}
%\int_{\Regm} \P \cdot \Grad \vU \ \dRegm = \int_{\bRegm^N} \norma{\Finv \N} \det{\F} \t \cdot \vU \ \dbRegm^N \quad \forall \vU \in \varUm
%\end{equation}
%donde $\P$ es el primer tensor de tensiones de Piola-Kirchhoff, $\norma{\cdot}$ indica la norma euclidiana de un vector y $\N$ es el versor normal en el borde en la configuración material $\dRegm$\footnote{La equivalencia entre \ref{eq:PPV_descr_esp_conocida} y \ref{eq:PPV_descr_mat_conocida} es un resultado clásico de la mecánica del continuo \cite{Malvern1969ContinuumMechanics}.}.
%
%Por practicidad, es posible reescribir el problema de equilibrio de manera compacta de la siguiente forma: determinar el campo de desplazamientos $\U\in\kinUm$ tal que
%\begin{equation}
%\Resm{\U} = \zeroforall{\vU}{\varUm} 
%\end{equation}
%
%Por último, para poder resolver el problema planteado es necesario establecer alguna relación entre $\P$ y las deformaciones dadas por $\F$.
%En el marco del modelado convencional, esta relación se establece bajo la forma de una ecuación matemática (denominada usualmente como ecuación constitutiva) que se acopla con la ecuación de equilibrio (\ref{eq:PPV_descr_mat_conocida}).
%En el modelado multiescala, en cambio, la relación surgirá de resolver un problema microscópico (figura \ref{fig:Multiescala_esquema_concepto_RVE}).

